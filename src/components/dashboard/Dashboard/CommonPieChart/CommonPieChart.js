import React from 'react';
import {
    ResponsiveContainer,
    PieChart,
    Cell,
    Pie,
    Tooltip
  } from 'recharts';

export default function CommonPieChard ({data}) {
    let colors = [
        'rgb(99, 193, 222)', 
        'rgb(32, 168, 216)',
        'rgb(255, 193, 43)',
        'rgb(248, 108, 107)',
        'rgb(14, 105, 138)',
        'rgb(183, 129, 0)',
      ]
    return (
        <ResponsiveContainer minHeight={360}>
                            <PieChart width={730} height={250}>
                                <Pie dataKey="value" isAnimationActive={false} data={data} cx="50%" cy="50%" outerRadius={80} label>
                                    {
                                    data.map((entry, index) => (
                                        <Cell key={`cell-${index}`} fill={colors[index]}/>
                                    ))
                                    }
                                </Pie>
                                <Tooltip/>
                            </PieChart>
        </ResponsiveContainer>
    );
}