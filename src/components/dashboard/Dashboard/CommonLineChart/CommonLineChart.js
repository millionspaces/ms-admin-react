import React, {PureComponent}from 'react';
import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ResponsiveContainer
} from 'recharts';

class CustomizedLabel extends PureComponent {
    render() {
      const {
        x, y, stroke, value,
      } = this.props;
  
      return <text x={x} y={y} dy={-4} fill={stroke} fontSize={10} textAnchor="middle">{value}</text>;
    }
  }

export default function CommonLineChart({ data, lines, isLabelAllow }) {
    let colors = ['#8884d8', '#82ca9d', '#1ebbd7'];
    return (
        <ResponsiveContainer minHeight={360}>
            <LineChart data={data}
                margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                <CartesianGrid 
                    // vertical={false}
                    stroke={'#e5e5e5'}
                    strokeDasharray="3 3" />
                <XAxis dataKey="date"
                    axisLine={{ stroke: '#e5e5e5', strokeWidth: 1 }}
                    tickLine={false}
                    padding={{ left: 30, right: 30 }}
                />
                <YAxis axisLine={false} tickLine={false} />
                <Tooltip />
                <Legend />
                {lines.map((line, index) => <Line key={index} type="monotone" dataKey={line}
                    stroke={colors[index]}
                    // strokeWidth={3}
                    //  dot={<CustomizedDot />}
                    label={isLabelAllow ? <CustomizedLabel /> : null}
                    activeDot={{ r: 10, strokeWidth: 0 }}
                    animationDuration={300}
                />)}
            </LineChart>
        </ResponsiveContainer>
    );
}  