import React from 'react';
import {
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ResponsiveContainer,
    BarChart,
    Bar
  } from 'recharts';

export default function CommonBarChart({data, bars}){
    const colors = ['#f69899', '#d897eb', '#64ea91'];

    return (
        <ResponsiveContainer minHeight={360}>
                        <BarChart width={730} height={250} data={data}>
                            <CartesianGrid 
                            vertical={false}
                            horizontal={false}
                            stroke={'#e5e5e5'}
                            strokeDasharray="3 3"
                             />
                            <XAxis dataKey="name" />
                            <YAxis />
                            <Tooltip />
                            <Legend />
                            {bars.map((bar, index) =>
                                <Bar key={index} dataKey={bar} fill={colors[index]} />
                            )}
                            </BarChart>
        </ResponsiveContainer>       
    )
}
