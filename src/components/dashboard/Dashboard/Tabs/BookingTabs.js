import React from 'react';
import CountTo from 'react-count-to';
import { Card, Col, Row } from 'antd';


export default function BookingTabs({ chartsDetails }) {
  return (
    <Row gutter={16}>
      <Col md={6} xs={12}>
        <Card bordered={false} style={{ padding: '24px',backgroundColor: '#005073', color : '#ffffff', borderRadius : '8px'}}>
          {/* <Icon type="home" style={{color: 'rgb(100, 234, 145)', fontSize: '54px', float: 'left'}} /> */}
          <Row>
            <Col md={24}>
              <div style={{ fontSize: '18px', fontWeight: 900 }}>
                Today's Bookings
              </div>
            </Col>
          </Row>
          <Col md={8} xs={8}>
            <div>Paid</div>
            <div style={{ fontSize: '18px', fontWeight: 900 }}>
              <CountTo
                to={chartsDetails[0] ? chartsDetails[0].paidBookings : 0}
                speed={3000}
              />
            </div>
          </Col>
          <Col md={8} xs={8}>
            <div>Unpaid</div>
            <div style={{ fontSize: '18px', fontWeight: 900 }}>
              <CountTo
                to={chartsDetails[0] ? chartsDetails[0].unPaidBookings : 0}
                speed={3000}
              />
            </div>
          </Col>
          <Col md={8} xs={8}>
            <div>Total</div>
            <div style={{ fontSize: '18px', fontWeight: 900 }}>
              <CountTo
                to={(chartsDetails[0] ? chartsDetails[0].paidBookings : 0) + (chartsDetails[0] ? chartsDetails[0].unPaidBookings : 0)}
                speed={3000}
              />
            </div>
          </Col>
        </Card>
      </Col>
      <Col md={6} xs={12}>
        <Card bordered={false} style={{ padding: '24px', backgroundColor: '#107dac', color : '#ffffff', borderRadius : '8px' }}>
          {/* <Icon type="home" style={{color: 'rgb(100, 234, 145)', fontSize: '54px', float: 'left'}} /> */}
          <Row>
            <Col md={24}>
              <div style={{ fontSize: '18px', fontWeight: 900 }}>
                Yesterday's Bookings
              </div>
            </Col>
          </Row>
          <Col md={8} xs={8}>
            <div>Paid</div>
            <div style={{ fontSize: '18px', fontWeight: 900 }}>
              <CountTo
                to={chartsDetails[1] ? chartsDetails[1].paidBookings : 0}
                speed={3000}
              />
            </div>
          </Col>
          <Col md={8} xs={8}>
            <div>Unpaid</div>
            <div style={{ fontSize: '18px', fontWeight: 900 }}>
              <CountTo
                to={chartsDetails[1] ? chartsDetails[1].unPaidBookings : 0}
                speed={3000}
              />
            </div>
          </Col>
          <Col md={8} xs={8}>
            <div>Total</div>
            <div style={{ fontSize: '18px', fontWeight: 900 }}>
              <CountTo
                to={(chartsDetails[1] ? chartsDetails[1].paidBookings : 0) + (chartsDetails[1] ? chartsDetails[1].unPaidBookings : 0)}
                speed={3000}
              />
            </div>
          </Col>
        </Card>
      </Col>
      <Col md={6} xs={12}>
        <Card bordered={false} style={{ padding: '24px', backgroundColor: '#189ad3', color : '#ffffff', borderRadius : '8px' }}>
          {/* <Icon type="home" style={{color: 'rgb(100, 234, 145)', fontSize: '54px', float: 'left'}} /> */}
          <Row>
            <Col md={24}>
              <div style={{ fontSize: '18px', fontWeight: 900 }}>
                Monthly Bookings
              </div>
            </Col>
          </Row>
          <Col md={8} xs={8}>
            <div>Paid</div>
            <div style={{ fontSize: '18px', fontWeight: 900 }}>
              <CountTo
                to={
                  chartsDetails[chartsDetails.length - 2]
                    ? chartsDetails[chartsDetails.length - 2].paidBookings
                    : 0
                }
                speed={3000}
              />
            </div>
          </Col>
          <Col md={8} xs={8}>
            <div>Unpaid</div>
            <div style={{ fontSize: '18px', fontWeight: 900 }}>
              <CountTo
                to={
                  chartsDetails[chartsDetails.length - 2]
                    ? chartsDetails[chartsDetails.length - 2].unPaidBookings
                    : 0
                }
                speed={3000}
              />
            </div>
          </Col>
          <Col md={8} xs={8}>
            <div>Total</div>
            <div style={{ fontSize: '18px', fontWeight: 900 }}>
              <CountTo
                to={( chartsDetails[chartsDetails.length - 2]
                  ? chartsDetails[chartsDetails.length - 2].paidBookings
                  : 0) + (chartsDetails[chartsDetails.length - 2]
                    ? chartsDetails[chartsDetails.length - 2].unPaidBookings
                    : 0)}
                speed={3000}
              />
            </div>
          </Col>
        </Card>
      </Col>
      <Col md={6} xs={12}>
        <Card bordered={false} style={{ padding: '24px', backgroundColor: '#1ebbd7', color : '#ffffff', borderRadius : '8px'}}>
          {/* <Icon type="home" style={{color: 'rgb(100, 234, 145)', fontSize: '54px', float: 'left'}} /> */}
          <Row>
            <Col md={24}>
              <div style={{ fontSize: '18px', fontWeight: 900 }}>
                Space Listings
              </div>
            </Col>
          </Row>
          <Col md={12} xs={12}>
            <div>MTD</div>
            <div style={{ fontSize: '18px', fontWeight: 900 }}>
              <CountTo
                to={chartsDetails[chartsDetails.length - 2] ? chartsDetails[chartsDetails.length - 2].totalSpaces : 0}
                speed={3000}
              />
            </div>
          </Col>
          <Col md={12} xs={12}>
            <div>YTD</div>
            <div style={{ fontSize: '18px', fontWeight: 900 }}>
              <CountTo
                to={chartsDetails[chartsDetails.length - 1] ? chartsDetails[chartsDetails.length - 1].totalSpaces : 0}
                speed={3000}
              />
            </div>
          </Col>
        </Card>
      </Col>
    </Row>
  );
}
