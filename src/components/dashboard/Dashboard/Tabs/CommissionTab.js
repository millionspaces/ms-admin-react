import React, { Component } from 'react';
import { Row, Col, Card } from 'antd';

export default function CommissionTab({day, commission}) {
        return (
                <Card bordered={false} style={{backgroundColor: '#42688a', color : '#ffffff'}}>
                    <Row>
                        <Col md={18}>
                            <div style={{ fontSize: '16px', fontWeight: 900 }}>
                                {day} Commission
                            </div>
                        </Col>
                        <Col md={6}>
                            <div style={{ fontSize: '18px', fontWeight: 900 }}>
                                 {commission}
                            </div>
                        </Col>
                    </Row>
                </Card>
        );
}