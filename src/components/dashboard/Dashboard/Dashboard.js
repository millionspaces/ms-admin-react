import React, { Component, fr } from 'react';
import { Row, Col, Card } from 'antd';
import { connect} from 'react-redux';
import {getDashboardDetails, bookingDistributionPieChart, commitionDetailsActionCreator} from '../../../redux/actions/statisticsActions';
import { setCurrentScreen } from '../../../redux/actions/screensActions';
import CommissionTab from './Tabs/CommissionTab';
import BookingTabs from './Tabs/BookingTabs';
import LineChart from './CommonLineChart/CommonLineChart';
import PieChart from './CommonPieChart/CommonPieChart';
import BarChart  from './commonBarChart/CommonBarChart';
import moment from 'moment';

const { Meta } = Card;

class Dashboard extends Component {

    componentDidMount() {
        this.props.getDashboardDetails();
        this.props.bookingDistributionPieChart();
        this.props.commitionDetailsActionCreator();
        this.props.setScreen('Dashboard');
    }

    getvalue = (date) => {
        const {commissions} = this.props;

        switch(date){
            case 'Today':
                return commissions[0].commission;
            case `Yesterday`    :
                return commissions[1].commission;
            case 'Weekly':
                return commissions[7].commission;
            case 'Monthly':
                return commissions[8].commission;
            default :
                return 0;
        }
    }

    render() {
        const {chartsDetails, bookingDistribution, commissions} = this.props;
        const lineChartLines = ['totalBooking', 'paidBookings', 'unPaidBookings'];
        const commitionsLines = ['commission'];
        const tabs           = ['Today', `Yesterday`, 'Weekly', 'Monthly']

        return (
            <div style={{ padding: '30px' }}>
                 <BookingTabs chartsDetails={chartsDetails}/>
                <Row gutter={16}>
                    <Col  md={18} xs={24} >
                        <Card
                            bordered={false}
                            bodyStyle={{
                                padding: '24px 36px 24px 0',
                            }}
                            style={{
                                background: '#fff',
                                marginTop: 16}}
                            >
                            <Meta
                               title="Bookings (Week)"
                               style={{paddingLeft : '53px'}}    
                            />
                            <LineChart 
                                data={chartsDetails.filter(k => k.date !== moment().format('M') && k.date !== chartsDetails[chartsDetails.length-1].date).reverse() }
                                lines={lineChartLines}
                                />
                            
                        </Card>
                    </Col>
                    <Col  md={6} xs={24} >
                        <Card
                            bordered={false}
                            bodyStyle={{
                                padding: '24px 36px 24px 0',
                            }}
                            style={{
                                background: '#fff',
                                marginTop: 16}}
                            >
                            <Meta
                               title="Booking Distribution (Month)"
                               style={{paddingLeft : '53px'}}    
                            />
                            <PieChart data={bookingDistribution.map(k => {return {value : k.noofBookings, name : k.eventType}})}/>
                            </Card>
                        </Col>
                </Row>
                <Row gutter={16} style={{marginTop : 16}}>
                    {tabs.map(item => 
                        <Col  md={6} xs={12}>
                            <CommissionTab day={item} commission={commissions.length > 0 ? this.getvalue(item) : ""}/>
                        </Col>
                    )}
                </Row>
                <Row gutter={16}>
                    <Col  md={24} xs={24} >
                        <Card
                            bordered={false}
                            bodyStyle={{
                                padding: '24px 36px 24px 0',
                            }}
                            style={{
                                background: '#fff',
                                marginTop: 16}}
                            >
                            <Meta
                               title="Commissions (Weekly)"
                               style={{paddingLeft : '53px'}}    
                            />
                            <LineChart 
                                data={commissions.filter(k => k.date !== 'week' && k.date !== 'month').reverse() }
                                lines={commitionsLines}
                                isLabelAllow={true}
                                />
                            
                        </Card>
                    </Col>
                </Row>
                {/* <Row gutter={16}>
                <Col  md={12} xs={24} >
                    <Card
                            bordered={false}
                            bodyStyle={{
                                padding: '24px 36px 24px 0',
                            }}
                            style={{
                                background: '#fff',
                                marginTop: 16}}
                            >
                             <Meta
                               title="Bookings"
                               style={{paddingLeft : '53px'}}    
                            />
                    
                        <PieChart data={bookingDistribution}/>

                        </Card>  
                    </Col>     
                </Row> */}

                {/* <Row gutter={16}>
                <Col  md={24} xs={24} >
                    <Card
                            bordered={false}
                            bodyStyle={{
                                padding: '24px 36px 24px 0',
                            }}
                            style={{
                                background: '#fff',
                                marginTop: 16}}
                            >
                        <BarChart data={data} bars={lines}/>
                        </Card>  
                    </Col>     
                </Row> */}
          </div>
        );
    }
}

const mapStateToProps = ({statistics}) => {
    const chartsDetails = statistics.dashboard && statistics.dashboard ? statistics.dashboard : []
    const bookingDistribution = statistics.bookingDistribution && statistics.bookingDistribution ? statistics.bookingDistribution : []
    const commissions = statistics.commissions && statistics.commissions ? statistics.commissions : []

     return {
        chartsDetails,
        bookingDistribution,
        commissions
    }
};

const mapDispatchToProps = dispatch => ({
    getDashboardDetails: () => dispatch(getDashboardDetails()),
    bookingDistributionPieChart: () => dispatch(bookingDistributionPieChart()),
    commitionDetailsActionCreator: () => dispatch(commitionDetailsActionCreator()),
    setScreen: screen => dispatch(setCurrentScreen(screen)),

});


export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);