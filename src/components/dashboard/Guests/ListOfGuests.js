import React from 'react';

// Ant Design
import { Layout } from 'antd';
import { connect } from 'react-redux';
import { setCurrentScreen } from '../../../redux/actions/screensActions';

class ListOfGuests extends React.Component {

    componentDidMount() {
        this.props.setScreen('Guests')
    }

    render () {
        return (
            <Layout style={{background: '#fff', margin: 10}}>
                <h1>🚧🐒Still monkeying with it.🚧🐒</h1>
                <h3>Please be patient :)</h3>
            </Layout>
        )
    }
}

export default connect(null, dispatch => {
    return {
        setScreen: screen => dispatch(setCurrentScreen(screen))
    }
}) (ListOfGuests)