import React from 'react';
import { Spin } from 'antd';
import FetchSpace from './FetchSpace/FetchSpace';
import ChangeUserForm from './ChangeUserForm';
import { connect } from 'react-redux';

class ChangeUser extends React.Component {

    render () {

        return(
            <Spin spinning={this.props.fetching}><div style={{padding: 16, margin: 16, background: '#fff'}}>
                <FetchSpace />
                <ChangeUserForm />
            </div></Spin>
        )
    }
}

export default connect(({userToBeChanged}) => {

    const { fetching } = userToBeChanged
    return {
        fetching
    }
}, null) (ChangeUser);