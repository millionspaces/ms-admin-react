import React from 'react';
import { Form, Input, Button, notification } from 'antd';
import { connect } from 'react-redux';
import { changeUser } from '../../../redux/actions/tempActions';

const FormItem = Form.Item;

class ChangeUserForm extends React.Component {



    handleSubmit = (e) => {

        e.preventDefault();

        this.props.form.validateFields((err, values) => {
            
            if (!err) {
                this.props.changeHost({
                    space: this.props.user.space,
                    user: {email: values.email}
                });
            }
        });

    }

    componentWillReceiveProps (nextProps) {

        if (nextProps.fetching) {
            this.props.form.resetFields()
        }

        notification.config({
            placement: 'topLeft',
            bottom: 50,
            duration: 10,
        });

        if (nextProps.error) {
            notification['error']({
                message: 'It seems you are using an unregistered email',
                description: 'Check the email address and if the issue still persists contact Priyanthan.'
            });
        }

        if (nextProps.success) {
            notification['success']({
                message: 'Changing user success!'
            });
        }
    }

    render() {

        const { getFieldDecorator } = this.props.form;

        const { user } = this.props;

        return !!this.props.user && <Form
            onSubmit={this.handleSubmit}
        >
            {/* Email */}
            <FormItem label="email" style={{ width: 300, display: 'inline-block', marginRight: 20 }}>
                {getFieldDecorator('email', {
                    initialValue: user.user.email,
                    rules: [{required: true, message: 'This field is required!'}, {type: 'email', message: 'Please enter a valid email'}]
                })(
                    <Input />
                )}
            </FormItem>
            <FormItem style={{ width: 300, display: 'inline-block', marginRight: 20, verticalAlign: 'bottom' }}>
                <Button type="primary" htmlType="submit">Submit</Button>
            </FormItem>

        </Form>
    }
}

ChangeUserForm = Form.create()(ChangeUserForm)

export default connect(({ userToBeChanged }) => {

    const { fetching, user, error, success } = userToBeChanged;

    return {
        fetching,
        user,
        error,
        success
    }

}, dispatch => {
    return {
        changeHost: user => dispatch(changeUser(user))
    }
})(ChangeUserForm)