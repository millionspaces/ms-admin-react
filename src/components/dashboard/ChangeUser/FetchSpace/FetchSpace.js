import React from 'react';
import { Form, Input, Button } from 'antd';
import { connect } from 'react-redux';
import { getSpaceUserDetails } from '../../../../redux/actions/tempActions';

const FormItem = Form.Item;

class FetchSpace extends React.Component {

    handleSubmit = e => {

        e.preventDefault();

        this.props.form.validateFields((err, values) => {
            
            if(!err) {
                this.props.getUser(values.spaceId);
            }

        });

    }

    render () {

        const { getFieldDecorator } = this.props.form;

        return(
            <Form
                onSubmit={this.handleSubmit}
            >
                <FormItem
                    label="Space ID"
                    style={{display: 'inline-block', width: 400}}
                >
                    {getFieldDecorator('spaceId', {
                        rules:[{required: true, message: "This field is required"}]
                    })(
                        <Input />
                    )}
                </FormItem>

                <FormItem
                    style={{display: 'inline-block', verticalAlign: 'bottom', marginLeft: 8}}
                >
                    <Button type="primary" htmlType="submit">Fetch</Button>
                </FormItem>
            </Form>
        )
    }
}

FetchSpace = Form.create()(FetchSpace);

const mapStateToProps = () => {
    return {}
}
const mapDispatchToProps = dispatch => {
    return {
        getUser: spaceId => dispatch(getSpaceUserDetails(spaceId))
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (FetchSpace);