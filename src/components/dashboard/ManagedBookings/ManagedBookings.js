import React from 'react';
import { Layout, Table, Pagination, Icon, Tag, Modal, Button, Spin, Rate, Row, Col, Form, Input} from 'antd';
import { connect } from 'react-redux';
import moment from 'moment';

import { getMangedBookings, getBookingDetailsById, performCancelBooking, getManageBookingsById } from '../../../redux/actions/managedBookings';
import { DATE_FORMAT } from '../../../settings';
import { setCurrentScreen } from '../../../redux/actions/screensActions';
import ManagedBookingsDetails from './ManagedBookingsDetails';

const { Content } = Layout;
const FormItem = Form.Item;


/**
 * Class Representing Managed Bookings Component
 */
class ManagedBookings extends React.Component {

    state = {
        currentPage                 : 1,
        bookingDetailsModal         : false,
        reviewVisibility            : false,
        managedBooking               : {},
        isShowManageBookingDetails  : false
    }

    componentDidMount () {
        this.props.getMangedBookings(this.state.currentPage-1);
        this.props.setScreen('Managed Bookings')
    }

    /**
     * Triggers on pagination link hits.
     * @param {String} pageNumber Page number.
     */
    onPaginationChange = (pageNumber) => {

        this.props.getMangedBookings(pageNumber-1);

        this.setState({
            currentPage: pageNumber
        });
    }

    /**
     * Opens Bookin Details Modal Window.
     */
    showBookingDetails = () => {
        this.setState({
            bookingDetailsModal: true
        });
    }

    /**
     * Hides Booking details modal window.
     */
    hideBookingDetails = () => {
        this.setState({
            bookingDetailsModal: false
        });
    }

    /**
     * Show review screen.
     */
    showHideReview = (state, review = {}) => {

        const { cleanliness, average, service, valueForMoney, description, title, dateCreated } = review;

        this.setState ({
            reviewVisibility: state,
            cleanliness: Number(cleanliness),
            average: average?Number(average):0,
            service: service?Number(service):0,
            valueForMoney: valueForMoney?Number(valueForMoney):0,
            description,
            title,
            dateCreated,
        });
        
    }

    handleSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, values) => {
            if (!err) {
                if(values.bookingId){
                    const { REACT_APP_MS_ENV } = process.env;
                    values.bookingId.includes(REACT_APP_MS_ENV==='qae' ? 'MQ' : 'ML') ? 
                        this.props.getManageBookingsById(values.bookingId.split(REACT_APP_MS_ENV==='qae' ? 'MQ' : 'ML')[1])
                        :   this.props.getManageBookingsById(values.bookingId)
                }else{
                    this.props.getMangedBookings(this.state.currentPage-1);
                }
            }
        });
    }
    onSelectChange = (manageBooking) => {
        this.setState({
                isShowManageBookingDetails : true,
                managedBooking              : manageBooking
        });
    };

    hideManageBookingDetails = () => {
        this.setState({
            isShowManageBookingDetails : false
        });
    }

    render () {

        const { fetching, bookings, pageCount, bookingDetails, bookingDetailsFetching } = this.props;
        const dataSource = [];
        const { getFieldDecorator } = this.props.form;

        const ReviewScreen = ({cleanliness, average, service, valueForMoney, title, description, dateCreated}) => { 
        return <Modal
            visible={this.state.reviewVisibility}
            onOk={() => this.showHideReview(false)}
            onCancel={() => this.showHideReview(false)}
            footer={false}
        >
            <div>
                <div style={{fontWeight: 'bold'}}>Date Created: {dateCreated} </div>
                <div style={{display: 'inline-block', width: 160, marginTop: 20}}>
                    <div>average</div>
                    <Rate disabled value={average} />
                    <div>cleanliness</div>
                    <Rate disabled value={cleanliness} />
                    <div>Service</div>
                    <Rate disabled value={service} />
                    <div>Value for money</div>
                    <Rate disabled value={valueForMoney} />
                </div><div style={{display: 'inline-block', verticalAlign: 'top', marginTop: 20}}>
                    <h1>{title}</h1>
                    <p>{description}</p>
                </div>
            </div>
        </Modal>

        }

        // Booking details column headings.
        const columns = [{
            title: 'Ref',
            dataIndex: 'orderId',
            key: 'orderId',
            width: 100,
            render: (text, manageBookings, index) => { 
                return <a key={index} onClick={(e) => {this.onSelectChange(manageBookings)}}>{text}</a> 
            },
        }, {
            title: 'Guest Name',
            dataIndex: 'guestName',
            key: 'guestName'
        }, {
            title: 'Space Name',
            dataIndex: 'name',
            key: 'name'
        },{
            title: 'Booking Date',
            key: 'bookedDate',
            dataIndex: 'bookedDate'
        }, {
            title: 'Host Name',
            dataIndex: 'hostName',
            key: 'hostName'
        }, {
            title: 'Status',
            dataIndex: 'reservatationStatusName',
            key: 'reservatationStatusName',            
            width: 100,
            render: rec => {
                return (<span>{rec==='Discared'?<Tag color="red">DISCARDED</Tag>:null}
                    {rec==='Confirmed'?<Tag color="blue">CONFIRMED</Tag>:null}
                    {rec==='Cancelled'?<Tag color="orange">CANCELLED</Tag>:null}
                    {rec==='Payment Done'?<Tag color="#428a1f">PAYMENT DONE</Tag>:null}
                    {rec==='Expired'?<Tag color="purple">EXPIRED</Tag>:null}
                    {rec==='Pending Payment'?<Tag color="cyan">PENDING PAYMENT</Tag>:null}
                    {rec==='Initiated'?<Tag color="green">INITIATED</Tag>:null}
                </span>)
            }
        }, {
            title: 'Event Date',
            dataIndex: 'dates',
            key: 'dates',
            width: 100,
            render: (rec=[]) => <div>
                {rec.map (element => <div key={`${element.fromDate}-${element.toDate}`}><span style={{display: 'inline-block', width: 80}}>{moment(element.fromDate).format(DATE_FORMAT)}</span></div>)}
            </div>
        }, {
            title: 'Review',
            dataIndex: 'review',
            key: 'review',
            width: 200,
            render: review => {
                const splittedRatings = review && review.rate?review.rate.split('|'):[]
                return <span><Rate disabled value={splittedRatings[0]} /> {review?<Icon style={{cursor: 'pointer'}} type="info-circle" onClick={() => { this.showHideReview (true, {
                    cleanliness: splittedRatings[2],
                    average: splittedRatings[0],
                    service: splittedRatings[1],
                    valueForMoney: splittedRatings[3],
                    description: review?review.description:'',
                    title: review?review.title:'',
                    dateCreated: review?review.createdAt:undefined,
                }) }} />:null}</span>
            }
        }, {
            title: 'Cancel Booking',
            dataIndex: 'cancel',
            key: 'cancel',

            render: (text, record) => (
                <Button type="danger" onClick={() => this.props.cancelBooking(record.id)}>Cancel</Button>
            )
        }];

        // Booking details table data
        bookings.forEach( element => {
            dataSource.push({
                key: element.id,
                id: element.id,
                orderId: element.orderId,
                name: element.name,
                guestName: element.guestName,
                hostName: element.hostName,
                dates: element.dates,
                reservatationStatusName: element.reservatationStatusName,
                review: element.review
            })
        });

        const { cleanliness, average, service, valueForMoney, title, description, dateCreated, isShowManageBookingDetails, managedBooking } = this.state;
 
        return (
            <Layout style={{margin: 10}}>

                    <Content style={{flex: 'initial', textAlign: 'right', marginTop: 20, marginBottom: 20}}>
                        <Row>
                            <Col span={8} style={{textAlign : 'left'}}>
                                <Form layout="inline" onSubmit={this.handleSubmit}>
                                        <FormItem>
                                        {getFieldDecorator('bookingId')(<Input placeholder="Reference Id" />)}
                                        </FormItem>
                                        <FormItem>
                                        <Button style={{ height: '29px'}}type="primary" htmlType="submit">Search</Button>
                                    </FormItem>
                                </Form>
                            </Col>
                            {pageCount > 10 ? <Col span={16}> Showing 10 of {pageCount} Bookings <Pagination style={{marginLeft: 20, display: 'inline-Block', verticalAlign: 'middle'}} showQuickJumper defaultCurrent={this.state.currentPage} total={pageCount} onChange={this.onPaginationChange} /></Col>:null}
                        </Row>
                    </Content>
                
                <Table style={{background: '#fff'}} bordered pagination={false} loading={fetching} columns={columns} dataSource={bookings}/>
                <ManagedBookingsDetails 
                        isShowManageBookingDetails={isShowManageBookingDetails}
                        manageBookings={managedBooking}
                        hideManageBookingDetails={this.hideManageBookingDetails}
                        />
                <Modal 
                    visible={this.state.bookingDetailsModal}
                    onOk={this.hideBookingDetails}
                    onCancel={this.hideBookingDetails}
                    footer={[
                        <Button type="primary" key="back" onClick={this.hideBookingDetails}>OK</Button>
                    ]}
                >
                    {bookingDetails?<Spin spinning={bookingDetailsFetching}><ul
                        className="manage-bookings-details-list"
                        style={{height: bookingDetailsFetching?100:'auto'}}
                    >
                        <li>
                            <div>Status</div>
                            <div>{bookingDetails.reservationStatus?bookingDetails.reservationStatus.label:null}</div>
                        </li>
                        <li>
                            <div>Guest Detauls</div>
                            <div>
                                <span style={{marginRight: 10}}><Icon style={{marginRight: 5}} type="user" />{bookingDetails.user?bookingDetails.user.name:null}</span>
                                <span style={{marginRight: 10}}><Icon style={{marginRight: 5}} type="mobile" />{bookingDetails.user?bookingDetails.user.mobile:null}</span>
                                <span style={{marginRight: 10}}><Icon style={{marginRight: 5}} type="mail" />{bookingDetails.user?bookingDetails.user.email:null}</span>
                            </div>
                        </li>
                        <li>
                            <div>Reservation time</div>
                            <div>
                                {bookingDetails.dates?bookingDetails.dates.map(date => <div>From: {moment(date.fromDate).format(DATE_FORMAT)} To: {moment(date.toDate).format(DATE_FORMAT)}</div>):null}
                            </div>
                        </li>
                        <li>
                            <div>Date booking was made</div>
                            <div>
                                {bookingDetails.bookedDate}
                            </div>
                        </li>
                        <li>
                            <div>Event type</div>
                            <div>
                                {bookingDetails.eventTypedetail?bookingDetails.eventTypedetail.name:null}
                            </div>
                        </li>
                        <li>
                            <div>Expected Guest Count</div>
                            <div>
                                {bookingDetails.guestCount}
                            </div>
                        </li>
                        <li>
                            <div>Amenities</div>
                            <div className="booking-details-amenitites">
                                {bookingDetails.space?bookingDetails.space.amenities.map(amenity => <span style={{display: 'inline-block', width: '40%', marginBottom: 5}} key={amenity.id}><img alt={amenity.name} style={{width: 20}} src={amenity.mobileIcon} /> {amenity.name}</span>):null}
                            </div>
                        </li>
                        <li>
                            <div>Reservation Free</div>
                            <div>
                                {bookingDetails.bookingCharge}
                            </div>
                        </li>
                    </ul></Spin>:null}
                </Modal>
                <ReviewScreen
                    cleanliness={cleanliness} 
                    average={average}
                    service={service}
                    valueForMoney={valueForMoney}
                    description={description}
                    title={title}
                    dateCreated={dateCreated}
                />
            </Layout>
        )
    }
}

ManagedBookings = Form.create()(ManagedBookings);

const mapStateToProps = ({managedBookings, currentBookingDetails, cancelBookings}) => {
    
    const { data } = managedBookings;
    const { bookings, count } = data;

    return {
        fetching: managedBookings.fetching || currentBookingDetails.fetching || cancelBookings.fetching,
        bookings: bookings?bookings:[],
        pageCount: count,
        bookingDetails: currentBookingDetails?currentBookingDetails.data:null,
        bookingDetailsFetching: currentBookingDetails.fetching?true:false
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getMangedBookings: pageNumber => dispatch (getMangedBookings (pageNumber)),
        getBookingDetailsById: bookingId => dispatch (getBookingDetailsById(bookingId)),
        setScreen: screen => dispatch(setCurrentScreen(screen)),
        cancelBooking: bookingId => dispatch(performCancelBooking(bookingId)),
        getManageBookingsById: id => dispatch(getManageBookingsById(id))
    }
}

export default connect (mapStateToProps, mapDispatchToProps) (ManagedBookings);