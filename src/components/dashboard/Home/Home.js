// TODO: Move Styles
import React from 'react';


// redux
import { setCurrentScreen } from "../../../redux/actions/screensActions";
import { connect } from 'react-redux';

// Ant Design
import { Layout } from 'antd';

/**
 * Class Representing Home component
 */
class Home extends React.Component {

    componentDidMount() {
        this.props.setScreen('Home');
    }

    render() {
        return (
            <Layout style={{display: 'block'}}>
                <h1>🚧🐒Still monkeying with it.🚧🐒</h1>
                <h3>Please be patient :)</h3>
            </Layout>
        )
    }
}

export default connect(null, dispatch => {
    return {
        setScreen: screen => dispatch(setCurrentScreen(screen))
    }
})(Home);