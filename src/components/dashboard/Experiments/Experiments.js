import React, { Component } from 'react';

import { Form, Icon, Input, Button } from 'antd';

const FormItem = Form.Item;

class Experiments extends Component {

    state = {
        perGuestHoursAvailability: [],
        spaceOnlyHoursAvailability: []
    }

    handleSubmit = (e) => { }

    hasErrors = (fieldsError) => {
        return Object.keys(fieldsError).some(field => fieldsError[field]);
    }

    compareToFirstPassword = (rule, value, callback) => {

        const form = this.props.form;

        if (value && value !== form.getFieldValue('password')) {
            callback('Two passwords that you enter is inconsistent!\n');
        } else {
            callback();
        }
        
    }

    validateUserName = (rule, value, callback) => {
        console.log(typeof value)
        if (value.length < 6 ) {
            callback ('Chakabakas')
        } else if (value.length > 6 && typeof value === !'string') {
   
            callback ('second passed')
        } else {
            callback()
        }
    }

    render() {

        const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;

        const userNameError = isFieldTouched('userName') && getFieldError('userName'),
            passwordError = isFieldTouched('password') && getFieldError('password');

        return (
            <Form layout="inline" onSubmit={this.handleSubmit} >
                <FormItem
                    validateStatus={userNameError ? 'error' : ''}
                    help={userNameError}
                >
                    {
                        getFieldDecorator('userName', {
                            rules: [{
                                required: true,
                                message: 'Please input your username!'
                            }, {
                                validator: this.validateUserName
                            }]
                        })(
                            <Input type="number" prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                        )
                    }
                </FormItem>
                <FormItem
                    validateStatus={passwordError ? 'error' : ''}
                    help={passwordError || ''}
                >
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: 'Please input your Password!' }],
                    })(
                        <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                    )}
                </FormItem>
                <FormItem>
                    <Button
                        type="primary"
                        htmlType="submit"
                        disabled={this.hasErrors(getFieldsError())}
                    >Log in</Button>
                </FormItem>
            </Form>
        )
    }
}

Experiments = Form.create()(Experiments);

export default Experiments;
