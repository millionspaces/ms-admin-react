// TODO: Validate Proptypes
// TODO: DOC
// TODO: move styles
import React from 'react';

// Ant Design
import { Layout, notification } from 'antd';

// Sub Components
import MainHeader from './subs/MainHeader';
import MainFooter from './subs/MainFooter';
import MainAside from './subs/MainAside';
import Transactions from './Transactions/Transactions';

// Main Components
import Spaces from './Spaces/Spaces';
import EditSpace from './Spaces/edit/EditSpace';
import ManagedBookings from './ManagedBookings/ManagedBookings';
import ListOfGuests from './Guests/ListOfGuests';
import TentetiveBookings from './TentetiveBookings/TentetiveBookings';
import Releases from '../Releases/Releases';
import SpaceOfTheWeek from './SpaceOfTheWeek/SpaceOfTheWeek';

// React Router
import { BrowserRouter as Router, Route } from 'react-router-dom';

// Redux
import { userDeviceLogoutActionCreator } from '../../redux/actions/authenticationActions';
import { connect } from 'react-redux';

// Experiments
import Experiments from './Experiments/Experiments';
import TempActions from './TempActions/TempActions';
import ChangeUser from './ChangeUser/ChangeUser';
import {SUCCESS, FAILED} from '../../utils'

import Reports from './Reports/Reports'
import Statistics from './Statistics/Statistics';
import FinanceHostPayments from './Finance/FinanceHostPayments/FinanceHostPayments';
import Dashboard from './Dashboard/Dashboard';
import Banner from './Banner/Banner';
/**
 * Class representing Dashboard component
 */
class Main extends React.Component {

    state = {
        collapsed: true
    }   
    // -------------- getDerivedStateFromProps is life cycle method wich will be called in every prop change or state change ---------------
    static getDerivedStateFromProps(nextProps, prevState) {
        let {creationState, deletionState, deletionMessage, updationMessage, message, updationState    } = nextProps;
            if (creationState === SUCCESS) {

                notification.success({ message });

            } else if (creationState === FAILED) {

                notification.error({ message });

            }

            // Generalized deletion notifications
            if (deletionState === SUCCESS) {

                notification.success({ message: deletionMessage });

            } else if (deletionState === FAILED) {

                notification.error({ message: deletionMessage });

            }

            // Generalized updation notifications
            if (updationState === SUCCESS) {

                notification.success({ message: updationMessage });

            } else if (updationState === FAILED) {

                notification.error({ message: updationMessage });
                
            }
    } 
    
    // -------------- Custom methods ---------------

    /**
     * Handles Sider Collapse state
     * @param {Boolean} collapsed 
     */
    onCollapse = collapsed => {
        this.setState({ collapsed });
    }

    /**
     * Toggles Sider collapse state with collapsed button hits
     */
    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }

    /**
     * Handle Logout
     */
    handleLogout = () => {
        this.props.logoutUserDevice();
    }

    render () {
        return (

            <Router>
                <Layout style={{ minHeight: '100vh', minWidth: '97vw', paddingBottom: 45 }} >
                    <MainAside
                        collapsed={this.state.collapsed}
                        onCollapse={this.onCollapse}
                    />
                    <Layout style={{marginTop: 60, zIndex: 8}}>

                        <MainHeader
                            collapsed={this.state.collapsed}
                            toggle={this.toggle}
                            handleLogout={this.handleLogout}
                        />

                        <Route
                            exact
                            path="/"
                            render={() => <Dashboard />}
                        />

                        <Route
                            exact
                            path="/dashboard"
                            render={() => <Dashboard/>}
                        />


                        <Route
                            exact
                            path="/spaces/"
                            render={() => <Spaces />}
                        />

                        <Route
                            exact
                            path="/spaces/:id"
                            render={props => <EditSpace spaceId={props.match.params.id}/>}
                        />

                        <Route
                            exact
                            path="/transactions"
                            render={props => <Transactions />}
                        />

                        <Route
                            exact
                            path="/managed-bookings"
                            render={() => <ManagedBookings />}
                        />

                        <Route
                            exact
                            path="/guests"
                            render={() => <ListOfGuests />}
                        />

                        <Route
                            exact
                            path="/tentetive-bookings"
                            render={() => <TentetiveBookings />}
                        />

                        <Route
                            exact
                            path="/releases"
                            render={() => <Releases />}
                        />

                        <Route
                            exact
                            path="/experiments"
                            render={() => <Experiments />}
                        />

                        <Route
                            exact
                            path="/space-of-the-week"
                            render={() => <SpaceOfTheWeek />}
                        />

                        <Route
                            exact
                            path="/temp-actions"
                            render={() => <TempActions />}
                        />

                        <Route
                            exact
                            path="/change-user"
                            render={() => <ChangeUser />}
                        />

                        <Route
                            exact
                            path="/reports"
                            render={() => <Reports/>}
                        />

                        <Route
                            exact
                            path="/Analytics"
                            render={() => <Statistics/>}
                        />

                        <Route
                            exact
                            path="/hostPayments"
                            render={() => <FinanceHostPayments/>}
                        />

                        <Route
                            exact
                            path="/banner"
                            render={() => <Banner/>}
                        />


                        <MainFooter />

                    </Layout>
                </Layout>
            </Router>

        )
    }
}

const mapStateToProps = ({ creation, deletion, updation }) => {
    const { creationState, message } = creation;
    const { updationState } = updation;

    return {
        creationState :  creationState,
        deletionState: deletion.deletionState, deletionMessage: deletion.message, updationMessage: updation.message,
        updationState :  updationState,
        message
    }
}

const mapDispatchToProps = dispatch => {
    
    return {
        logoutUserDevice: () => dispatch(userDeviceLogoutActionCreator())  
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (Main);
