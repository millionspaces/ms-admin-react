import React from 'react';
import { Form, Input, Button, notification, Spin } from 'antd';
import { hasErrors } from '../../../utils';
import { connect } from 'react-redux';
import { verifyCustomEmail } from '../../../redux/actions/tempActions';

const FormItem = Form.Item;

class ManualEmailVerification extends React.Component {

    /**
     * Handles form submit.
     * @param {SyntheticEvent} e Describes the event triggered.
     */
    handleSubmit = (e) => {

        // Prevent default action.
        e.preventDefault();

        // Validate form fields, If no errors then update space.
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.verifyEmail(values.email)
            }
        });
    }

    componentWillReceiveProps(nextProps) {

        const { verifyCustomEmail, verifEmailError } = nextProps;

        notification.config({
            placement: 'topLeft',
            bottom: 50,
            duration: 10,
        });

        if (verifyCustomEmail) {
            notification['success']({
                message: 'Email Verified Successfully'
            });
        }

        if (verifEmailError) {
            notification['error']({
                message: 'It seems you are trying to verify an unregistered email',
                description: 'Check the email address and if the issue still persists contact Priyanthan.'
            });
        }

    }

    render() {

        const { getFieldDecorator, getFieldsError } = this.props.form;

        return (
            <Spin spinning={this.props.fetching} size="large">
                <Form
                    onSubmit={this.handleSubmit}
                >
                    <h2>Manual email verification</h2>

                    <FormItem
                        label="Email"
                        style={{ width: 400, display: 'inline-block' }}
                    >
                        {getFieldDecorator('email', {
                            rules: [{
                                required: true, message: 'This field is required'
                            }, {
                                type: 'email', message: 'Please enter a valid email address'
                            }]
                        })(
                            <Input type="email" />
                        )}
                    </FormItem>
                    <FormItem
                        style={{ display: 'inline-block', verticalAlign: 'bottom', marginLeft: 20 }}
                    >
                        <Button disabled={hasErrors(getFieldsError())} type="primary" htmlType="submit">Update</Button>
                    </FormItem>

                </Form>
            </Spin>
        )
    }
}

ManualEmailVerification = Form.create()(ManualEmailVerification);

const mapStateToProps = ({ verifyCustomEmail }) => {
    return {
        verifyCustomEmail: verifyCustomEmail.success,
        verifEmailError: !!verifyCustomEmail.error,
        fetching: verifyCustomEmail.fetching
    }
}

const mapDispatchToProps = dispatch => {
    return {
        verifyEmail: email => dispatch(verifyCustomEmail(email))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManualEmailVerification);