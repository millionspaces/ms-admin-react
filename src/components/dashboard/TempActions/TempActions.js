import React from 'react';
import ManualEmailVerification from './manualEmailVerification';
import CancelBooking from './CancelBooking/CancelBooking';
/**
 * Class representing TempActions
 */
class TempActions extends React.Component {

    render () {

        return (
            <div style={{background: '#fff', padding: 16, margin: 16}}>
                <ManualEmailVerification />

                <CancelBooking />
            </div>
        )
    }
}

export default TempActions;