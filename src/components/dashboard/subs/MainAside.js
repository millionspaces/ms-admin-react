import React from 'react';
import PropTypes from 'prop-types';

// Ant Design
import { Layout, Menu, Icon } from 'antd';

// Routing
import { Link } from 'react-router-dom';

import logoFull from '../../../images/logo-full.png';
import logoLetter from '../../../images/logo-letter.png';

const { Sider } = Layout;

/**
 * Class Representing Collapsible main sidebar
 */
class MainAside extends React.Component {

    static propTypes = {
        collapsed: PropTypes.bool.isRequired,
        onCollapse: PropTypes.func.isRequired
    }

    render () {
        return (
            <Sider
                collapsible
                collapsed={this.props.collapsed}
                onCollapse={this.props.onCollapse}
                width={210}
            >
                <div>{this.props.collapsed?<img style={{marginLeft: 15, width: 50, marginTop: 30, marginBottom: 30}} src={logoLetter} />:<img style={{marginLeft: 25, width: 150, marginTop: 40, marginBottom: 40}}  src={logoFull} />}</div>

                <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
                
                    {/* <Menu.Item key='home'>
                        <Link to="/">
                            <Icon type="home" />
                            <span>Home</span>
                        </Link>
                    </Menu.Item> */}
                     <Menu.Item key='main'>
                        <Link to="/dashboard">
                            <Icon type="dashboard" />
                            <span>Dashboard</span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item key='spaces'>
                        <Link to="/spaces">
                            <Icon type="shop" />
                            <span>Spaces</span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="payment">
                        <Link to="/transactions">
                            <Icon type="swap" />
                            <span>Transactions</span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="managedBookings">
                        <Link to="/managed-bookings">
                            <Icon type="wallet" />
                            <span>Manage Bookings</span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="tentetive">
                        <Link to="/tentetive-bookings">
                            <Icon type="credit-card" />
                            <span>Tentetive Bookings</span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="temp-actions">
                        <Link to="/temp-actions">
                            <Icon type="mail" />
                            <span>Custom email verification</span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="change-user">
                        <Link to="/change-user">
                            <Icon type="user" />
                            <span>Change user</span>
                        </Link>
                    </Menu.Item>
                    
                    <Menu.Item key="reports">
                        <Link to="/reports">
                            <Icon type="file-excel" />
                            <span>Reports</span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="Analytics">
                        <Link to="/Analytics">
                            <Icon type="line-chart" />
                            <span>Analytics</span>
                        </Link>
                    </Menu.Item>

                    {/* <Menu.Item key="HostPayments">
                        <Link to="/HostPayments">
                        <Icon type="folder" />
                            <span>Finance Host Payments</span>
                        </Link>
                    </Menu.Item> */}

                    <Menu.Item key="Banner">
                        <Link to="/Banner">
                        <Icon type="picture" />
                            <span>Banner</span>
                        </Link>
                    </Menu.Item>
                </Menu>
            </Sider>
        )
    }
}

export default MainAside;


