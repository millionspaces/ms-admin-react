// TODO: Move Styles
// TODO: DOC
import React from 'react';

// Ant Design
import { Layout } from 'antd';

const { Footer } = Layout;

const MainFooter = () => <Footer style={{ textAlign: 'center', color: '#fff', background: 'rgb(0, 33, 64)', position: 'fixed', bottom: 0, width: '100%', zIndex: 99, padding: '14px 50px' }}>MillionSpaces© {new Date().getFullYear()}</Footer>

export default MainFooter;