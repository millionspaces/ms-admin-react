import React, { Component } from 'react';
import {Modal, Button} from 'antd';

class TransactionDetails extends Component {

    constructor(props) {
        super(props);

        this.state = {
            visible :  false
        }
    }

    componentWillReceiveProps(nextProps) {
       this.setState((state, props) => { return { visible : nextProps.isShowTransactionDetails !== state.visible  && nextProps.isShowTransactionDetails}});
    }
    
    handleOk = (e) => {
        this.setState({
          visible: false,
        });
        this.props.hideTransactionDetails();
    }
    
    handleCancel = (e) => {
        this.setState({
          visible: false,
        });
        this.props.hideTransactionDetails();
    }

    render() {
        let {visible}            = this.state;
        let {transaction}        = this.props;
        return (
            <Modal
                title={`Transaction ${transaction ? transaction.orderId : null}`}
                visible={visible}
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                    <Button key="submit" type="primary" onClick={this.handleOk}>
                      Ok
                    </Button>,
                ]}
                >
                <p>Booking Date: {transaction ? transaction.bookingDate : null}</p>
                <p>Space Name: {transaction ? transaction.spaceName : null}</p>
                <p>Organization: {transaction ? transaction.organizationName : null}</p>
                <p>Event Date: {transaction ? transaction.eventDate : null}</p>
                <p>Cancellation Policy: {transaction ? transaction.cancellationPolicy : null}</p>
                <p>Cut Off Date: {transaction ? transaction.cancellationCutOffDate : null}</p>
                <p>Guest Name: {transaction ? transaction.guestName : null}</p>
                <p>Guest Email: {transaction ? transaction.guestEmail : null}</p>
                <p>Mobile: {transaction ? transaction.guestMobile : null}</p>
                <p>Commission Percentage: {transaction ? transaction.commissionPercentage : null}</p>
                <p>Total: {transaction ? transaction.total : null}</p>
                <p>Host Charge: {transaction ? transaction.hostCharge : null}</p>
                <p>Booking Date: {transaction ? transaction.bookingDate : null}</p>

            </Modal>
        );
    }
}

export default TransactionDetails;