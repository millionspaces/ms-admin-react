import React from 'react';
import { Layout, Table, Pagination, Icon, Button, Popconfirm, Tooltip, Badge, Form, Input, Row, Col } from 'antd';
import { connect } from 'react-redux';
import moment from 'moment';

import { getTransactionDetails, setTransactionProof, getTransactionByBookingId } from '../../../redux/actions/transactionActions';
import { PDF__TRANSACTIONS_CLOUDE_NAME, PDF__TRANSACTIONS_PRESET, DATE_FORMAT } from '../../../settings';
import { setCurrentScreen } from '../../../redux/actions/screensActions';
import TransactionDetails from './TransactionDetails';

const { Content } = Layout;
const FormItem = Form.Item;
/**
 * Class representing Transactions Component.
 *  
 */
class Transactions extends React.Component {

    state = {
        currentPage                 : 1,
        isShowTransactionDetails    : false,
        trasaction                  : {}
    }

    componentDidMount () {
        // Once the component is mounted then fetch first 10 transaction details.
        this.props.getTransactionDetails(this.state.currentPage-1);
        this.props.setScreen('Transactions')
    }
    
    /**
     * Triggers on pagination changes.
     * @param {Number} pageNumber Requested page number.
     */
    onPaginationChange = (pageNumber) => {

        this.props.getTransactionDetails(pageNumber-1);

        this.setState({
            currentPage: pageNumber
        });
    }

    /**
     * Opens Cloudinary widget and with pdf upload wizard.
     * @param {Object} booking Describes a booking
     */
    openWidgetForThumbnail = booking => {
        window.cloudinary.openUploadWidget({
            cloud_name: PDF__TRANSACTIONS_CLOUDE_NAME,
            upload_preset: PDF__TRANSACTIONS_PRESET,
            theme: 'white',
            folder: 'transactions',
            client_allowed_formats: ["pdf", "jpeg", "jpg"],
        }, (error, result) => {
            if (!error) {
                this.props.setTransactionProof({
                    pdf: result[0].url,
                    bookingId: booking.id,
                    verify: 1
                }, this.state.currentPage-1);
            }
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, values) => {
            if (!err) {
                if(values.bookingId){
                    const { REACT_APP_MS_ENV } = process.env;
                    values.bookingId.includes(REACT_APP_MS_ENV==='qae' ? 'MQ' : 'ML') ? 
                        this.props.getTransactionByBookingId(values.bookingId.split(REACT_APP_MS_ENV==='qae' ? 'MQ' : 'ML')[1])
                        :   this.props.getTransactionByBookingId(values.bookingId)
                }else{
                    this.props.getTransactionDetails(this.state.currentPage-1)
                }
            }
        });
    }

    onSelectChange = (transaction) => {
        this.setState({
                isShowTransactionDetails : true,
                transaction              : transaction
        });
    };

    hideTransactionDetails = () => {
        this.setState({
                isShowTransactionDetails : false
        });
    }

    render () {
        const { getFieldDecorator } = this.props.form;

        // Transaction details column table header.
        const columns = [{
            title: 'Ref',
            dataIndex: 'orderId',
            key: 'orderId',
            width: 100,
            render: (text, transaction, index) => { 
                return <a key={index} onClick={(e) => {this.onSelectChange(transaction)}}>{text}</a> 
            },
            
        }, {
            title: 'Space Name',
            dataIndex: 'spaceName',
            key: 'spaceName',
            width: 180
        }, {
            title: 'Organization',
            dataIndex: 'organizationName',
            key: 'organizationName'
        },{
            title: 'Booking Date',
            key: 'bookingDate',
            dataIndex: 'bookingDate'
        }, {
            title: 'Event Date',
            key: 'eventDate',
            dataIndex: 'eventDate'
        }, {
            title: 'Cancellation Policy',
            key: 'cancellationPolicy',
            dataIndex: 'cancellationPolicy'
        }, {
            title: 'Cut Off Date',
            key: 'cancellationCutOffDate',
            dataIndex: 'cancellationCutOffDate'
        }, {
            title: '%',
            key: 'commissionPercentage',
            dataIndex: 'commissionPercentage',
            width: 50
        }, {
            title: 'Total',
            key: 'total',
            dataIndex: 'total'
        }, {
            title: 'Host payment (LKR)',
            key: 'hostCharge',
            dataIndex: 'hostCharge'
        }, {
            title: 'Payment Due Date',
            key: 'paymentDueDate',
            dataIndex: 'paymentDueDate',
            render: paymentDueDate => <Tooltip placement="bottom" title={paymentDueDate.tooltip}><span>{paymentDueDate.text}</span></Tooltip>
        }, {
            title: <div style={{textAlign: 'center'}}><Badge overflowCount={999} offset={[-8, 8]} count={this.props.pendingTransactions}>Verify</Badge></div>,
            dataIndex: 'upload',
            key: 'upload',
            width: 120,
            render: (text, rec) => !rec.paymentVerified
                ?
                    <Popconfirm placement="left" title="Are you sure you want to verify the payment?" onConfirm={() => this.openWidgetForThumbnail(rec)} okText="Yes" cancelText="No">
                        <Tooltip placement="left" title="Click to upload payment proof.">
                            <Button size="small" loading={this.props.fetching} type="primary" icon={rec.paymentVerified?"check":"upload"} >Payment Made</Button>
                        </Tooltip>
                    </Popconfirm>
                :
                    <Tooltip placement="left" title="Click to view payment proof"><a href={rec.pdf} target="_blank"><Icon type="file-pdf" /> View Proof</a></Tooltip>
        }];

        const { transactions, pageCount }   = this.props;
        let {isShowTransactionDetails, transaction}      = this.state;
        
        return (
            <Layout style={{margin: 10}}>
                <Content>
                    <Content style={{textAlign: 'right', marginTop: 20, marginBottom: 20}}>
                        <Row>
                            <Col span={8} style={{textAlign : 'left'}}>
                                <Form layout="inline" onSubmit={this.handleSubmit}>
                                        <FormItem>
                                        {getFieldDecorator('bookingId')(<Input placeholder="Reference Id" />)}
                                        </FormItem>
                                        <FormItem>
                                        <Button style={{ height: '29px'}}type="primary" htmlType="submit">Search</Button>
                                    </FormItem>
                                </Form>
                            </Col>
                            {transactions.length > 0?
                            <Col span={16}>
                                <Pagination showQuickJumper defaultCurrent={this.state.currentPage} total={pageCount?pageCount:1} onChange={this.onPaginationChange} />
                            </Col>
                            :null}
                        </Row>
                    </Content>
                    <Table bordered style={{background: '#fff'}} pagination={false} loading={this.props.fetching} columns={columns} dataSource={transactions}/>
                    <TransactionDetails 
                        isShowTransactionDetails={isShowTransactionDetails}
                        transaction={transaction}
                        hideTransactionDetails={this.hideTransactionDetails}
                        />
                </Content>
            </Layout>
        )
    }
}
Transactions = Form.create()(Transactions);

const mapStateToProps = ({transactions}) => {

    let bookings = transactions.data?transactions.data.bookings:[], arr = [];

    bookings && bookings.forEach(booking => {
        arr.push(Object.assign({}, booking, 
            {eventDate: moment(booking.eventDate).format(DATE_FORMAT)},
            {key: booking.id},
            {cancellationCutOffDate: moment(booking.cancellationCutOffDate).format(DATE_FORMAT)},
            {paymentDueDate: {text: moment(booking.paymentDueDate).format(DATE_FORMAT), tooltip: moment(booking.paymentDueDate).endOf('day').fromNow()}},
            {paymentVerified: booking.paymentVerified},
            {bookingDate: moment(booking.bookingMade).format(DATE_FORMAT)},
            {guestMobile : booking.guestMobile ? booking.guestMobile : null}
        ))
    });

    return {
        transactions: arr,
        fetching: transactions.fetching,
        pageCount:  transactions.data?transactions.data.count:1,        
        pendingTransactions: transactions.data?transactions.data.pending:0,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getTransactionDetails: (pageId) => dispatch(getTransactionDetails(pageId)),
        setTransactionProof: (pdf, pageId) => dispatch(setTransactionProof(pdf, pageId)),        
        setScreen: screen => dispatch(setCurrentScreen(screen)),
        getTransactionByBookingId: bookingId => dispatch(getTransactionByBookingId(bookingId))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Transactions);