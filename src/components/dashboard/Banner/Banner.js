import React, { Component } from 'react';
import { connect } from 'react-redux'
import { IMAGE_CLOUD_NAME, IMAGE_CLOUD_PRESET } from '../../../settings';
import {getBannsers} from '../../../redux/actions/bannersActions'
import { Spin, Icon, Popconfirm } from 'antd';
import {IPAD_LANDSACPE, DESKTOP, MOBILE_PORTRAIT, MOBILE_LANDSACPE, IPAD_PORTRAIT} from './cmmon';
class Banner extends Component {

    state = {
        images: [],
        thumbnailImage: undefined,
        imageDetails: {},
        imageTypes : [
            'ipad_landscape',
            'desktop',
            'mobile_portrait',
            'mobile_landscape',
            'ipad_portrait'
        ] 
    }

    componentDidMount() {
        this.props.getBannsers();
        let {banners, imageDetails} = this.props;

        this.setState({
            images : banners,
            imageDetails : imageDetails 
        });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            images : nextProps.banners ? nextProps.banners[0] : {},
            imageDetails : nextProps.imageDetails ? nextProps : {} 
        });
    }
    
    addSpaceImage = (image) => {
        this.setState ({
            images: [...this.state.images, image]
        })
    }
    
    /**
     * set image data delete token, and url to state
     */
    addSpaceThumbnail = imageData => {
        this.setState (imageData);
    }

    imageWidthSetup = width => {

    }

     /**
     * openWidgetForBanner 
     */

    getMinImageWidth = (type) => {
        if(IPAD_LANDSACPE === type){
            return '2050';
        }else if(DESKTOP === type){
            return '2564';
        }else if(MOBILE_PORTRAIT){
            return '962';
        }else if(MOBILE_LANDSACPE){
            return '1538';
        }else if(IPAD_PORTRAIT){
            return '1202';
        }
    }

    getMinImageHeight = (type) => {
        if(IPAD_LANDSACPE === type){
            return '1202';
        }else if(DESKTOP === type){
            return '1041';
        }else if(MOBILE_PORTRAIT){
            return '1202';
        }else if(MOBILE_LANDSACPE){
            return '1202';
        }else if(IPAD_PORTRAIT){
            return '1202';
        }
    }

    openWidgetForBanner = (minWidth, minHeight, type) => {
        window.cloudinary.openUploadWidget({
            cloud_name: 'dgcojyezg',
            upload_preset: 'hglnuurt',
            theme: 'white',
            public_id : type,
            folder: 'fl_prograssive/millionspaces/banners/space-of-the-week',
            min_image_width: minWidth,
            min_image_height: minHeight,
            client_allowed_formats: ["png", "jpeg"],
        }, (error, result) => {
            if (!error) {
                this.addSpaceImage (result[0].public_id.split('/')[1]);
                this.props.getBannsers();
                
                let tempImageDetails = {
                        type: result[0].public_id.split('/')[1],
                        description : Object.assign(this.state.imageDetails.description ? 
                                                    this.state.imageDetails.description : {},
                                                    { [type + "_deleteToken"]: result[0].delete_token })
                    }
                this.setState(({imageDetails}) => 
                    Object.assign(imageDetails, tempImageDetails), function() {
                        alert(result[0].public_id.split('/')[1] , result[0].delete_token);
                    }
                );
            }            
        });
    }

    removeCoverImage = type => {
        let {images} = this.state;

        var newImageList = Object.keys(images).reduce((image, key) => {
            if (key !== type) {
                image[key] = images[key]
            }
            return image
          }, {})

        // delete images[type]
        this.setState ({
            images : newImageList
        });

    }
    
    render() {
        const { imageTypes, images } = this.state;

        return (    
            <div style={{background: '#fff', padding: 16, margin: 16}}>
                <p style={{marginTop: 20, fontWeight: 'bold'}}>Banners.</p>
                    {imageTypes.map((type, index) => {
                        return images && images[type] ? 
                            <div key={index} style={{position: 'relative', display: 'inline-block', marginRight: 20, verticalAlign: 'middle'}}>
                            <div>{type}</div>
                                <Popconfirm title="Are you sure delete this image?" onConfirm={(e) => this.removeCoverImage(type)} okText="Yes" cancelText="No">
                                    <Icon 
                                        type="delete"
                                        style={{zIndex: 3, position: 'absolute', right: 0, margin: 10, padding: 10, background: '#fff', borderRadius: '50%', cursor: 'pointer', bottom: 0}}
                                    />    
                                </Popconfirm>
                                <div style={{cursor: "pointer", display: 'flex', justifyContent: 'center', alignItems: 'center',minWidth: 200, minHeight: 150, border: '1px dashed #e2e2e2'}}>
                                    <img style={{maxWidth: 300}} src={`${images[type]}`} />
                                </div>
                            </div> :
                            <div onClick={(e) => {this.openWidgetForBanner(this.getMinImageWidth(type), this.getMinImageHeight(type), type)}} style={{display: 'inline-block', padding: '60px', marginTop: 10, border: '1px dashed #e2e2e2', cursor: 'pointer',  verticalAlign: 'middle'}}><Icon style={{fontSize: 22}} type="plus" style={{fontWeight: 'bold', fontSize: 32}} /></div>
                        }) 
                    }
            </div>
        );
    }
}   

const mapStateToProps = ({banners}) => {
    return {
        banners : banners.banners ? banners.banners : [] 
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getBannsers : bannerData => dispatch(getBannsers())
    }
}

export default connect (mapStateToProps, mapDispatchToProps) (Banner);;