export const IPAD_LANDSACPE     = 'ipad_landscape';
export const DESKTOP            = 'desktop';
export const MOBILE_PORTRAIT    = 'mobile_portrait';
export const MOBILE_LANDSACPE   = 'mobile_landscape';
export const IPAD_PORTRAIT      = 'ipad_portrait';
