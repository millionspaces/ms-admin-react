// TODO: proptype validation.
import React from 'react';

//Ant design
import { Form, Input, Button, Icon } from 'antd';

// Custom Components
//import ImageUploadSingle from '../../../../common/ImageUploadSingle';

import { hasErrors } from '../../../../../utils';

const FormItem = Form.Item;

/**
 * Class representing Host Details Section of Edit Space component.
 */
class HostDetails extends React.Component {

    state = {}


    // ------------------------- Custom Methods -------------------------

    /**
     * Handles form submit
     * @param {SyntheticEvent} e Describes Event triggered.
     */
    handleSubmit = (e) => {

        // Prevent default action.
        e.preventDefault();

        let updates = {};

        // Validating values if no errors then update profile.
        this.props.form.validateFields((err, values) => {
            if (!err) {

                // Creates updates.
                updates = Object.assign({}, values, { hostLogo: this.state.hostLogo });

                this.props.updateSpace(updates);

            }
        });
    }

    /**
     * Validates host name.
     * @param {Object} rule Describes rule defined.
     * @param {String} value Value to be validated
     * @param {Function} callback callback to be executed with validations.
     */
    hostNameValidator = (rule, value, callback) => {
        if (value.length > 60) {
            callback('Contact person name can\'t exceed 60 characters.');
        } else if (value.length === 0) {
            callback('This field is required');
        } else if (!/^[A-Za-z\s]+$/.test(value)) {
            callback('Filed can not contain special characters or numbers');
        } else {
            callback();
        }
    }

    /**
     * Validates second host name.
     * @param {Object} rule Describes rule defined.
     * @param {String} value Value to be validated
     * @param {Function} callback callback to be executed with validations.
     */
    secondHostNameValidator = (rule, value, callback) => {
        if (value && value.length > 60) {
            callback('Contact person name can\'t exceed 60 characters.');
        } else if (!/^[A-Za-z\s]+$/.test(value)) {
            callback('Filed can not contain special characters or numbers');
        } else {
            callback();
        }
    }

    render() {

        const { contactPersonName, mobileNumber, companyPhone, contactPersonName2, mobileNumber2, email2 } = this.props;
        const { getFieldDecorator, getFieldsError } = this.props.form;

        return (
            <Form onSubmit={this.handleSubmit} >

                {/* Contact Person Name */}
                <FormItem label="Contact Person Name" style={{ width: 300, display: 'inline-block', marginRight: 20 }}>
                    {getFieldDecorator('contactPersonName', {
                        rules: [{ validator: this.hostNameValidator }],
                        initialValue: contactPersonName
                    })(
                        <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Length 1 - 60 characters" />
                    )}
                </FormItem>
                
                {/* Mobile Number */}
                <FormItem label="Mobile Number" style={{ width: 300, display: 'inline-block', marginRight: 20 }}>
                    {getFieldDecorator('mobileNumber', {
                        rules: [{ required: false, message: 'Please enter a correct mobile phone number!', len: 10 }],
                        initialValue: mobileNumber
                    })(
                        <Input type="tel" pattern="[0-9]*" prefix={<Icon type="mobile" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Mobile number (07********)" />
                    )}
                </FormItem>

                {/* Company Phone Number */}
                <FormItem label="Company Phone Number (Optional)" style={{ width: 300, display: 'inline-block', marginRight: 20 }}>
                    {getFieldDecorator('companyPhone', {
                        rules: [{ required: false, message: 'Please enter a correct phone number.', len: 10 }],
                        initialValue: companyPhone
                    })(
                        <Input type="tel" pattern="[0-9]*" prefix={<Icon type="phone" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Company phone number (011*******)" />
                    )}
                </FormItem>
                
                {/* Secondary contact details */}
                <div>

                    <h3>Secondary contact details</h3>
                    
                    {/* Secondary Contact Person Name */}
                    <FormItem label="Contact Person Name (optional)" style={{ width: 300, display: 'inline-block', marginRight: 20 }}>
                        {getFieldDecorator('contactPersonName2', {
                            rules: [{ validator: this.secondHostNameValidator }],
                            initialValue: contactPersonName2
                        })(
                            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Length 1 - 60 characters" />
                        )}
                    </FormItem>

                    {/* Secondary Mobile number */}
                    <FormItem label="Mobile Number (Optional)" style={{ width: 300, display: 'inline-block', marginRight: 20 }}>
                        {getFieldDecorator('mobileNumber2', {
                            rules: [{ required: false, message: 'Please enter a correct mobile phone number!', len: 10 }],
                            initialValue: mobileNumber2
                        })(
                            <Input type="tel" pattern="[0-9]*" prefix={<Icon type="mobile" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Mobile number (07********)" />
                        )}
                    </FormItem>

                    {/* Secondary Mobile number */}
                    <FormItem label="Email (Optional)" style={{ width: 300, display: 'inline-block', marginRight: 20 }}>
                        {getFieldDecorator('email2', {
                            rules: [{
                                type: 'email', message: 'Please enter a valid email address!',
                            }],
                            initialValue: email2
                        })(
                            <Input type="email" prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="johndoe@domain.com" />
                        )}
                    </FormItem>

                </div>

                {/* Submit button */}
                <div style={{ textAlign: 'right' }}>
                    <Button disabled={hasErrors(getFieldsError())} type="primary" htmlType="submit" className="login-form-button">Update</Button>
                </div>

            </Form>
        );
    }
}

export default Form.create()(HostDetails);

