import React, { Component } from 'react';

// Children
import ImageUploadSingle from '../../../../common/ImageUploadSingle';

// Redux
import { setSpaceImagesDetails } from '../../../../../redux/actions/spaceImagesActions';
import { connect } from 'react-redux';

/**
 * Class representing host logo component.
 */
class HostLogo extends Component {
    
    state = {
        hostLogo: undefined,
        imageData: undefined
    }

    /**
     * Set Image upload data to internal state
     * @param {Object} imageData Contains array of objects explaining image upload data from cloudinary.
     */
    setImageUploadData = imageData => {
        this.props.saveSpaceImages(imageData)
        this.setState ({ imageData });
    }
    
    /**
     * Removes logo from component state
     */
    handleLogoDelete = () => {
        this.setState({ hostLogo: '' });
    }

    /**
     * Handles HostLogo
     * @param {Object} Describes host logo
     */
    onHostLogoChange = (hostLogo) => {
        this.setState({
            hostLogo
        });
    }

    render() { 

        const { hostLogo } = this.props;

        return (
            <ImageUploadSingle
                defaultImg={hostLogo}
                onImageSaveSuccess={this.onHostLogoChange}
                setImageUploadData={this.setImageUploadData}
                removeImage={this.handleLogoDelete}
                updateSpace={this.props.updateSpace}
            />
        )
    }
}


export default connect ( null, dispatch => {
    return {
        saveSpaceImages: imageData => dispatch(setSpaceImagesDetails(imageData)),
    }
})(HostLogo);