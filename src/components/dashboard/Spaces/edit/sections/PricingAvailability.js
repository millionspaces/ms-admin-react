import React from 'react';

import { Form, Input, Button, Checkbox, Select, DatePicker, Icon, InputNumber, notification } from 'antd';
import moment from 'moment';
import { generateHours, compareTimeString, addHours, convert24To12, hasErrors } from '../../../../../utils';
import PerHourTableHeader  from './dumbComponents/PerHourTableHeader';
import { TIME_STRING_FORMAT, IMAGE_CLOUD_NAME } from '../../../../../settings';
import { renderBlockBasedSpaceOnlyHeader, renderBlockBasedPerGuestHeader } from './dumbComponents/BlockBasedTableHeaders';

const FormItem = Form.Item, Option = Select.Option, createFormField = Form.createFormField;

const pricingStyles = {
    flexGrow: 1,
    flexBasis: 0,
    alignItems: 'center',
    marginBottom: 0,
    display: 'flex'
};

class PricingAvailability extends React.Component {

    state ={
        perGuestBookedHours: [{}],
        spaceOnlyBookedHours: [{}],
        perGuestBlocks: undefined,
        repeatedBoxesSpaceOnly: [],
        repeatedBoxesPerGuest: [],
        spaceOnlyId: 0,
        perGuestId: 0,
        blockChargeType: undefined,
        pricingFetching: false,
        isFormUpdate :  false,
        currentSpaceCheckedValue : false,
        currentAvailability : null
    }


    /**
     * Handles form submit.
     */
    onFormSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {

            let updates = undefined,
                avl = [];

            if (!err) {


                if (this.props.availabilityMethod === 'HOUR_BASE') {

                    for (let i=1; i<8; i++) {
                        avl.push({
                            day: i,
                            from: moment(values[`${i}-avlHr-From`], "h:mm A").format("HH:mm:ss"),
                            to:  moment(values[`${i}-avlHr-To`], "h:mm A").format("HH:mm:ss"),
                            charge: values[`${i}-avlHr-ChargePerHour`],
                            isAvailable: values[`${i}-avlHr-Availability`]?1:0
                        });
                    }

                    updates = Object.assign({}, updates, {availability: avl}, {bufferTime: values.bufferTime})
                
                }


                if (this.props.availabilityMethod === 'BLOCK_BASE') {

                    if (this.state.blockChargeType === 1) {
                        let avl = [];

                        for (let i = 0; i <= this.state.spaceOnlyBlocks.length-1; i++) {
                        values[`spaceOnly_${i}_day`] &&    
                            avl.push({
                                day: values[`spaceOnly_${i}_day`],  
                                from: moment(values[`spaceOnly_${i}_from`], "h:mm A").format("HH:mm:ss"),
                                to:  moment(values[`spaceOnly_${i}_to`], "h:mm A").format("HH:mm:ss"),
                                charge: values[`spaceOnly_${i}_charge`],
                                isAvailable: 1
                            });
                        }

                        updates = Object.assign({}, updates, {availability: avl, blockChargeType: this.state.blockChargeType,  reimbursableOption: values.reimbursable?1:0});

                    } else {

                        let avl = [];

                        for (let i = 0; i <= this.state.perGuestBlocks.length-1; i++) {
                            if(values[`perGuest_${i}_day`] !== undefined){
                                avl.push({
                                    day: values[`perGuest_${i}_day`],
                                    from: moment(values[`perGuest_${i}_from`], "h:mm A").format("HH:mm:ss"),
                                    to:  moment(values[`perGuest_${i}_to`], "h:mm A").format("HH:mm:ss"),
                                    charge: Number(values[`perGuest_${i}_charge`]),
                                    isAvailable: 1,
                                    menuFiles:[{url: values[`perGuest_${i}_guestMenu`]}]
                                });
                            };
                        }
                        
                        updates = Object.assign({}, updates, {availability: avl, blockChargeType: this.state.blockChargeType, minParticipantCount: values.minNumberOfGuests})

                    }
                }

                updates = Object.assign({}, updates, {
                    noticePeriod: values.noticePeriod,
                    calendarEnd: values.calendarEnd.format('YYYY-MM-DD')
                });

                this.props.updateSpace(updates);
                this.setState({isFormUpdate : true});
            }

        });
    }

    setRepeatedBoxesSpaceOnly = repeatedBoxesSpaceOnly => {
        this.setState({
            repeatedBoxesSpaceOnly: repeatedBoxesSpaceOnly.avl && repeatedBoxesSpaceOnly.item ? [repeatedBoxesSpaceOnly.avl, repeatedBoxesSpaceOnly.item]: []
        });
    }

    setRepeatedBoxesPerGuest = repeatedBoxesPerGuest => {
        this.setState({
            repeatedBoxesPerGuest: repeatedBoxesPerGuest.avl && repeatedBoxesPerGuest.item ? [repeatedBoxesPerGuest.avl, repeatedBoxesPerGuest.item]: []
        });
    }

    minNumOfGuestsValidator = (rule, value, callback) => {
        if (value > 99999) {
            callback ('Can not exceed 99, 999 ');
        } else if (!/^[0-9]+$/.test(value)) {
            callback ('This field can only contain whole numbers');
        }  else if (value >= this.props.participantCount) {
            callback (`Cannot exceed the maximum  of ${this.props.participantCount} defined`)
        } else if (value === undefined || value === '') {
            callback ('This field is required');
        } else {
            callback ();
        }
    }

    validateMinimumHostNoticePeriod = (rule, value, callback) => this.validateNumbers(value, callback, 999);
    
    validateMinPricePerGuest = (rule, value, callback) => this.validateNumbers(value, callback, 99999);

    validateSpaceOnlyCharge  = (rule, value, callback) => this.validateNumbers(value, callback, 9999999);
    
    bufferTimeValidation = (rule, value, callback) => this.validateNumbers(value, callback, 72);

    chargePerHourValidator = (rule, value, callback) => this.validateNumbers(value, callback, 9999999);
    
    validateNumbers = (value, callback, max) => {
        if (value > max) {
            callback (`Maximum value ${max}`);
        } else if (!/^[0-9]+$/.test(value)) {
            callback ('This field can only contain positive whole numbers');
        } else if (value === undefined || value === '') {
            callback ('This field is required');
        } else {
            callback ();
        }
    }

    validator = (rule, value, callback) => {
        callback ();
    }

    setSpaceOnlyBookedHours = value => {

        let temp = [];

        for (let i=0; i<this.state.spaceOnlyBookedHours.length;i++) {
            
            if (this.state.spaceOnlyBookedHours[i].key !== value.key) {
                temp.push(this.state.spaceOnlyBookedHours[i]);
            }

            if (i === this.state.spaceOnlyBookedHours.length-1) {
                temp.push(value);
                this.setState({
                    spaceOnlyBookedHours: [...temp]
                });
            }
        }
        
    }

    setPerGuestBookedHours = value => {

        let temp = [];

        for (let i=0; i<this.state.perGuestBookedHours.length;i++) {
            
            if (this.state.perGuestBookedHours[i].key !== value.key) {
                temp.push(this.state.perGuestBookedHours[i]);
            }

            if (i === this.state.perGuestBookedHours.length-1) {
                temp.push(value);
                this.setState({
                    perGuestBookedHours: [...temp]
                });
            }
        }
        
    }

    addNewBlock = () => {
        
        if (this.state.blockChargeType === 1) {

            let { spaceOnlyBlocks } = this.state;

            if (!spaceOnlyBlocks) {
                spaceOnlyBlocks = []
            }

            if (this.state.blockChargeType === this.props.blockChargeType) {
                this.setState({
                    spaceOnlyBlocks: [...spaceOnlyBlocks, {
                        key: `spaceOnly_${this.state.spaceOnlyId+1}`,
                        from: '',
                        to: '',
                        charge: '',
                        day: ''
                    }],
                    spaceOnlyId: this.state.spaceOnlyId+1
                });
            } else {
                this.setState({
                    spaceOnlyBlocks: [...spaceOnlyBlocks, {
                        key: `spaceOnly_${this.state.spaceOnlyId}`,
                        from: '',
                        to: '',
                        charge: '',
                        day: ''
                    }],
                    spaceOnlyId: this.state.spaceOnlyId+1
                });
            }
            
        }
        

        if (this.state.blockChargeType === 2) {
            
            
            let { perGuestBlocks } = this.state;

            if (!perGuestBlocks) {
                perGuestBlocks = []
            }

            if (this.state.blockChargeType === this.props.blockChargeType) {
                this.setState({
                    perGuestBlocks: [...perGuestBlocks, {
                        key: `perGuest_${this.state.perGuestId+1}`,
                        from: '',
                        to: '',
                        charge: null,
                        day: '',
                        menuItem: undefined
                    }],
                    perGuestId: this.state.perGuestId+1
                });
            } else {
                this.setState({
                    perGuestBlocks: [...perGuestBlocks, {
                        key: `perGuest_${this.state.perGuestId}`,
                        from: '',
                        to: '',
                        charge: null,
                        day: '',
                        menuItem: undefined
                    }],
                    perGuestId: this.state.perGuestId+1
                });
            }
            
        }
    }

    openNotificationWithIcon = (type) => {
        notification[type]({
            message: 'Cannot Remove All Blocks'
        });
    };

    removePerGuestBlock = key => {
        let {perGuestBlocks, perGuestBookedHours, repeatedBoxesPerGuest} = this.state;

        if(perGuestBlocks.length !== 1){
            let temp = perGuestBlocks
                        .filter(item => {return item.key !== key})
                        .map((item, index) => { return {
                            key: `perGuest_${index}`,
                            from: item.from,
                            to: item.to,
                            charge: item.charge,
                            day: item.day,
                            menuItem: item.menuItem
                        }
            });

            let tempBooked = perGuestBookedHours
                        .filter(item => { return item.key !== key})
                        .map((item, index) => { return {
                            key: `perGuest_${index}`,
                            from: item.from,
                            to: item.to,
                            charge: item.charge,
                            day: item.day,
                            menuItem: item.menuItem
                        };
            });
            
            if (repeatedBoxesPerGuest.includes(key)) {
                this.setRepeatedBoxesPerGuest({avl: undefined, item: undefined});
            }
    
            this.setState({
                perGuestBlocks: [...temp],
                perGuestBookedHours: [...tempBooked],
                perGuestId: this.state.perGuestId-1
            })
        }else{
            this.openNotificationWithIcon("error")
        }
       
    }

    onBlockChargeTypeChange = value => {
        
        this.setState({
            blockChargeType: Number(value)
        });
        this.setRepeatedBoxesSpaceOnly({});
        this.setRepeatedBoxesPerGuest({});

    }
    
    removeSpaceOnlyBlock = key => {
        let {spaceOnlyBlocks, spaceOnlyBookedHours, repeatedBoxesSpaceOnly} = this.state;

            let temp = spaceOnlyBlocks.filter(item => {
                return item.key !== key
            });

            let tempBooked = spaceOnlyBookedHours.filter(item => {
                return item.key !== key
            });

            if (repeatedBoxesSpaceOnly.includes(key)) {
                this.setRepeatedBoxesSpaceOnly({avl: undefined, item: undefined});
            }

            this.setState({
                spaceOnlyBlocks: [...temp],
                spaceOnlyBookedHours: [...tempBooked]
            });
    }

    /************ Life cycle hooks **************/
    
   

    componentDidMount () {
        
        const { availabilityMethod, blockChargeType } = this.props;

        if (!this.state.blockChargeType) {
            this.setState({
                blockChargeType: this.props.blockChargeType
            })
        }

        // Block based logic
        if (availabilityMethod === 'BLOCK_BASE') {

            // If per guest and first time rendering then
            if (blockChargeType === 1) {

                
                if (!this.state.spaceOnlyBlocks) {

                    let temp = [];
                    this.props.availability.forEach(avl => {
                        
                        temp.push ({
                            key: `spaceOnly_${temp.length}`,
                            from: convert24To12(avl.from.split(":")[0]),
                            to: convert24To12(avl.to.split(":")[0]),
                            charge: avl.charge,
                            day: avl.day
                        });

                    });

                    this.setState({
                        spaceOnlyBlocks: [...temp],
                        spaceOnlyBookedHours: [...temp],
                        spaceOnlyId: temp.length-1
                    })

                }

               
            }
            
            if (blockChargeType===2) {
                if (!this.state.perGuestBlocks) {

                    let temp = [];

                    this.props.availability.forEach(avl => {
                        temp.push ({
                            key: `perGuest_${temp.length}`,
                            from: convert24To12(avl.from.split(":")[0]),
                            to: convert24To12(avl.to.split(":")[0]),
                            charge: avl.charge,
                            day: avl.day,
                            menuItem: avl.menuFiles[0]
                        })
                    });

                    this.setState({
                        perGuestBlocks: [...temp],
                        perGuestBookedHours: [...temp],
                        perGuestId: temp.length-1
                    });

                }
            }

        }
    }

    componentWillReceiveProps(nextProps) {
        const { blockChargeType } = this.props;
        let {isFormUpdate} = this.state;
        if(isFormUpdate === true){
            if (blockChargeType===2) {

                let temp = [];

                nextProps.availability.forEach(avl => {
                    temp.push ({
                        key: `perGuest_${temp.length}`,
                        from: convert24To12(avl.from.split(":")[0]),
                        to: convert24To12(avl.to.split(":")[0]),
                        charge: avl.charge,
                        day: avl.day,
                        menuItem: avl.menuFiles[0]
                    })
                });

                this.setState({
                    perGuestBlocks: [...temp],
                    perGuestBookedHours: [...temp],
                    perGuestId: temp.length-1,
                    isFormUpdate : false
                });
            }
        }
    }

    getPerHourElemnts() {
        let perHourElements = [];
        for (let i=1; i<8; i++) {
            perHourElements.push({
                id: i,
                availability: `${i}-avlHr-Availability`,
                day: `${i}-avlHr-Day`,
                from: `${i}-avlHr-From`,
                to: `${i}-avlHr-To`,
                chargePerHour: `${i}-avlHr-ChargePerHour`
            });
        }

        return perHourElements;
    }

    
    handleCheckBoxChange = (e, availability) => {
        let {getFieldValue, setFieldsValue} = this.props.form;
        let checkedUsers = this.getPerHourElemnts().map((item) => {
            return availability === item.availability ? e.target.checked : getFieldValue(item.availability)
        });

        if(checkedUsers.filter(item => item === true || item === 'checked').length === 0){
            this.openNotificationWithIcon("error");
            this.setState({currentSpaceCheckedValue : true,
                currentAvailability: availability
            });
        }else{
            this.setState({currentSpaceCheckedValue : false});
        }
    }

    render () {
        
        const { noticePeriod, calendarEnd } = this.props;
        const { getFieldDecorator, setFieldsValue, getFieldValue, getFieldsError } = this.props.form;
        const { perGuestBlocks, spaceOnlyBlocks } = this.state;

        if(this.state.currentSpaceCheckedValue){
            setFieldsValue({[this.state.currentAvailability] : 'checked' });
            this.setState({currentSpaceCheckedValue : false});
        } 

        let { blockChargeType } = this.state;

        if (!blockChargeType) {
            blockChargeType = this.props.blockChargeType
        }

        const timeSlots = [...generateHours(0, 23)], 
              days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday","Sunday"], 
              hours = generateHours(0, 23);

        const hourlyBlocks = () => {

            let perHourElements = this.getPerHourElemnts();

            const pricingStyles = {
                flexGrow: 1,
                flexBasis: 0,
                alignItems: 'center',
                marginBottom: 0,
                display: 'flex',
            };

            return (
                <div>
                    {
                        <div>
                            <PerHourTableHeader />
                            {
                                perHourElements.map(item => {
                                    return <div key={item.id} style={{display: 'flex'}} className="per-hour-table-body">
                                        <FormItem style={pricingStyles}>
                                            {
                                                getFieldDecorator(item.availability, { 
                                                    valuePropName: 'checked',
                                                })(<Checkbox 
                                                    onChange={(e) => {this.handleCheckBoxChange(e, item.availability)}}
                                                />)
                                            }                   
                                        </FormItem>

                                        <div style={pricingStyles}><div>{days[item.id-1]}</div></div>

                                            <FormItem style={pricingStyles}>
                                                {
                                                    getFieldDecorator(item.from)(
                                                        <Select
                                                            onChange={(value) => {
                                                                !compareTimeString(value, getFieldValue(item.to))?null:setFieldsValue({[item.to]: `${addHours(value, 1)}`})
                                                            }}
                                                            style={{minWidth: 110}}
                                                        >{hours.map(item => <Option
                                                            key={item.value}
                                                            value={item.value}
                                                        >{item.label}</Option>)}</Select>
                                                    )
                                                }
                                            </FormItem>
                                            <FormItem style={pricingStyles}>
                                                {
                                                    getFieldDecorator(item.to)(
                                                        <Select style={{minWidth: 110}}>
                                                            {hours.map(element => <Option disabled={compareTimeString(getFieldValue(item.from), element.value)} key={element.value} value={element.value}>{element.label}</Option>)}
                                                        </Select>
                                                    )
                                                }
                                            </FormItem>
                                            <FormItem style={pricingStyles}>
                                                {
                                                    getFieldDecorator(item.chargePerHour, {
                                                        rules: [{validator: getFieldValue(item.availability) ? this.chargePerHourValidator : this.validator}]
                                                    })(<Input type="number" type="number"  addonBefore="LKR" />)
                                                }
                                            </FormItem>
                                        </div>
                                    }
                                )
                            }

                            <FormItem label="Buffer time (Hrs)" style={{width: 200, textAlign: 'left'}}>
                                {
                                    getFieldDecorator('bufferTime', {
                                        rules: [{ required: true, validator: this.bufferTimeValidation }],
                                        initialValue: this.props.bufferTime
                                    })(<InputNumber addonAfter="HRS"  />)
                                }
                            </FormItem>

                        </div>
                    }
                </div>
            )
        }

        

        const blockBasedBlocks = () => {

            return (
                !this.state.pricingFetching?<div>

                    <FormItem label="Block Charge Type" style={{width: 300, display: 'inline-block', marginRight: 20, verticalAlign: 'middle'}}>
                        {getFieldDecorator('blockChargeType', {
                            initialValue: this.props.blockChargeType
                        })(
                            <Select onChange={this.onBlockChargeTypeChange}>
                                {this.props.blockChargeTypes.map(element => <Option key={element.id} value={element.id}>{element.name}</Option>)}
                            </Select>
                        )}
                    </FormItem>

                    {blockChargeType===1? <FormItem style={{width: 300, display: 'inline-block', verticalAlign: 'bottom'}}>
                        {
                            getFieldDecorator('reimbursable', { valuePropName: 'checked', initialValue:Boolean(this.props.reimbursableOption) })(
                                <Checkbox>Reimbursable against bill</Checkbox>
                            )
                        }                   
                    </FormItem>:<FormItem label="Minimum number of guests" style={{width: 300, display: 'inline-block', marginRight: 20, verticalAlign: 'middle'}}>
                        {
                            getFieldDecorator('minNumberOfGuests', {
                                initialValue: this.props.minParticipantCount,
                                rules: [{validator: this.minNumOfGuestsValidator}]
                            })(
                                <Input type="number"/>
                            )
                        }
                    </FormItem>}
                    
                    <div style={{marginBottom: 20}}>
                        <h3>Menu files</h3>
                        {this.props.menuFiles.map(item => {
                            return <span key={item.id} style={{verticalAlign: 'middle', display: 'inline-block', width: 100, marginRight: 20}}><img style={{width: '100%'}} src={`http://res.cloudinary.com/${IMAGE_CLOUD_NAME}/image/upload/millionspaces/${item.url}.jpg`} />Menu {item.menuId}</span>
                        })}
                    </div>
                    {blockChargeType===1?<h3>Space only charge</h3>:<h3>Per guest based charge</h3>}
                    {blockChargeType===1?renderBlockBasedSpaceOnlyHeader():renderBlockBasedPerGuestHeader()}
                    
                    {
                        blockChargeType===1
                            ?
                                spaceOnlyBlocks?spaceOnlyBlocks.map(avl => {
                                    return <div key={avl.key} style={{border:  this.state.repeatedBoxesSpaceOnly.includes(avl.key) ?'1px solid red': null, display: 'flex'}}>
                                        <FormItem style={{width: 350}} className="per-hour-table-body-elm">
                                            {
                                                getFieldDecorator( avl.key + '_day', {
                                                    initialValue: avl.day,
                                                    rules: [{required: true, message: 'This field is required'}]
                                                })(
                                                    <Select
                                                        onChange={
                                                            value => {

                                                                if (this.state.repeatedBoxesSpaceOnly.includes(avl.key)) {
                                                                    this.setRepeatedBoxesSpaceOnly({});
                                                                }

                                                                this.state.spaceOnlyBookedHours?this.state.spaceOnlyBookedHours.forEach(item => {
                                                                    
                                                                    if (item.key !== avl.key) {
                                                                        if (
                                                                            item.from === getFieldValue(avl.key+'_from') && 
                                                                            item.to === getFieldValue(avl.key+'_to') &&
                                                                            item.day === Number(value)
                                                                        ) {
                                                                            this.setRepeatedBoxesSpaceOnly({avl: avl.key, item: item.key});
                                                                        }
                                                                    }

                                                                    this.setSpaceOnlyBookedHours({
                                                                        from: getFieldValue(avl.key+'_from'),
                                                                        to: getFieldValue(avl.key+'_to'),
                                                                        day: Number(value),
                                                                        charge: getFieldValue(avl.key+'_charge'),
                                                                        key: avl.key
                                                                    });
                                                                }):null;
                                                            }
                                                        }
                                                    >
                                                        <Option value={8}>Weekday</Option>
                                                        <Option value={6}>Saturday</Option>
                                                        <Option value={7}>Sunday</Option>
                                                    </Select>
                                                )
                                            }
                                        </FormItem>
                                        <FormItem style={{width: 350}} className="per-hour-table-body-elm">
                                            {
                                                getFieldDecorator(avl.key+'_from', {
                                                    initialValue: avl.from,
                                                    rules: [{required: true, message: 'This field is required'}]
                                                })(
                                                    <Select
                                                        onChange={(value) => {

                                                            if (this.state.repeatedBoxesSpaceOnly.includes(avl.key)) {
                                                                this.setRepeatedBoxesSpaceOnly({});
                                                            }

                                                            this.state.spaceOnlyBookedHours?this.state.spaceOnlyBookedHours.forEach(item => {
                                                                
                                                                if (item.key !== avl.key) {
                                                                    if (
                                                                        item.from === value && 
                                                                        item.to === getFieldValue(avl.key+'_to') &&
                                                                        item.day === Number(getFieldValue(avl.key+'_day'))
                                                                    ) {
                                                                        this.setRepeatedBoxesSpaceOnly({avl: avl.key, item: item.key});
                                                                    }
                                                                    this.setSpaceOnlyBookedHours({
                                                                        from: value,
                                                                        to: getFieldValue(avl.key+'_to'),
                                                                        day: Number(Number(getFieldValue(avl.key+'_day'))),
                                                                        charge: getFieldValue(avl.key+'_charge'),
                                                                        key: avl.key
                                                                    });
                                                                }
                                                            }):null;

                                                            !compareTimeString(value, getFieldValue(avl.key+'_to'))?null:setFieldsValue({[avl.key+'_to']: `${addHours(value, 1)}`})
                                                        }}
                                                    >
                                                        {timeSlots.map(item => <Option key={item.value} value={item.value}>{item.label}</Option>)}
                                                    </Select>
                                                )
                                            }
                                        </FormItem>
                                        <FormItem style={{width: 350}} className="per-hour-table-body-elm">
                                            {
                                                getFieldDecorator(avl.key+'_to', {
                                                    initialValue: avl.to,
                                                    rules: [{required: true, message: 'This field is required'}]
                                                })(
                                                    <Select
                                                        onChange={value=>{
                                                            
                                                            if (this.state.repeatedBoxesSpaceOnly.includes(avl.key)) {
                                                                this.setRepeatedBoxesSpaceOnly({});
                                                            }

                                                            this.state.spaceOnlyBookedHours?this.state.spaceOnlyBookedHours.forEach(item => {
                                                                console.log(item.key, avl.key)
                                                                if (item.key !== avl.key) {
                                                                    if (
                                                                        item.from === getFieldValue(avl.key+'_from') && 
                                                                        item.to === value &&
                                                                        item.day === Number(getFieldValue(avl.key+'_day'))
                                                                    ) {
                                                                        this.setRepeatedBoxesSpaceOnly({avl: avl.key, item: item.key});
                                                                    }
                                                                }
                                                                this.setSpaceOnlyBookedHours({
                                                                    from: getFieldValue(avl.key+'_from'),
                                                                    to: value,
                                                                    day: Number(Number(getFieldValue(avl.key+'_day'))),
                                                                    charge: getFieldValue(avl.key+'_charge'),
                                                                    key: avl.key
                                                                });
                                                            }):null;
                                                        }}
                                                    >
                                                        {hours.map(element => <Option disabled={compareTimeString(getFieldValue(avl.key+'_from'), element.value)} key={element.value} value={element.value}>{element.label}</Option>)}
                                                    </Select>
                                                )
                                            }
                                        </FormItem>
                                        <FormItem style={{width: 350}} className="per-hour-table-body-elm">
                                            {
                                                getFieldDecorator(avl.key+'_charge', {
                                                    initialValue: avl.charge,
                                                    rules: [{required: true, validator: this.validateSpaceOnlyCharge}]
                                                })(
                                                    <Input type="number"
                                                        style={{width: '100%'}}
                                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                        addonBefore="LKR"
                                                    />
                                                )
                                            }
                                        </FormItem>
                                        <div style={{width: 350}}><Icon onClick={()=>{this.removeSpaceOnlyBlock(avl.key)}} style={{border: '1px solid #e2e2e2', borderRadius: '50%', cursor: 'pointer', padding: 3}} type="close" /></div> 
                                    </div>
                            }):null
                            
                            :<div>
                                {perGuestBlocks?perGuestBlocks.map(avl => {
                                    return (
                                        <div key={avl.key} style={{border:  this.state.repeatedBoxesPerGuest.includes(avl.key) ?'1px solid red': null, display: 'flex'}}  className="per-hour-table-body">
                                            <FormItem style={pricingStyles} className="per-hour-table-body-elm">
                                                {
                                                    getFieldDecorator( avl.key + '_day', {
                                                        initialValue: avl.day,
                                                        rules: [{required: true, message: 'This field is required'}]
                                                    })(
                                                        <Select
                                                            onChange={
                                                                value => {

                                                                    if (this.state.repeatedBoxesPerGuest.includes(avl.key)) {
                                                                        this.setRepeatedBoxesPerGuest({});
                                                                    }

                                                                    this.state.perGuestBookedHours?this.state.perGuestBookedHours.forEach(item => {
                                                                        
                                                                        if (item.key !== avl.key) {
                                                                            if (
                                                                                item.from === getFieldValue(avl.key+'_from') && 
                                                                                item.to === getFieldValue(avl.key+'_to') &&
                                                                                item.day === Number(value)
                                                                            ) {
                                                                                this.setRepeatedBoxesPerGuest({avl: avl.key, item: item.key});
                                                                            }
                                                                        }

                                                                        this.setPerGuestBookedHours({
                                                                            from: getFieldValue(avl.key+'_from'),
                                                                            to: getFieldValue(avl.key+'_to'),
                                                                            day: Number(value),
                                                                            charge: getFieldValue(avl.key+'_charge'),
                                                                            key: avl.key
                                                                        });

                                                                    }):null;

                                                                }
                                                            }
                                                        >
                                                            <Option value={8}>Weekday</Option>
                                                            <Option value={6}>Saturday</Option>
                                                            <Option value={7}>Sunday</Option>
                                                        </Select>
                                                    )
                                                }
                                            </FormItem>
                                            <FormItem style={pricingStyles} className="per-hour-table-body-elm">
                                                {
                                                    getFieldDecorator(avl.key+'_from', {
                                                        initialValue: avl.from,
                                                        rules: [{required: true, message: 'This field is required'}]
                                                    })(
                                                        <Select
                                                            onChange={(value) => {

                                                                if (this.state.repeatedBoxesPerGuest.includes(avl.key)) {
                                                                    this.setRepeatedBoxesPerGuest({});
                                                                }

                                                                this.state.perGuestBookedHours?this.state.perGuestBookedHours.forEach(item => {
                                                                    if (item.key !== avl.key) {
                                                                        if (
                                                                            item.from === value && 
                                                                            item.to === getFieldValue(avl.key+'_to') &&
                                                                            item.day === getFieldValue(avl.key+'_from')
                                                                        ) {
                                                                            this.setRepeatedBoxesPerGuest({avl: avl.key, item: item.key});
                                                                        }
                                                                        this.setPerGuestBookedHours({
                                                                            from: value,
                                                                            to: getFieldValue(avl.key+'_to'),
                                                                            day: Number(Number(getFieldValue(avl.key+'_day'))),
                                                                            charge: getFieldValue(avl.key+'_charge'),
                                                                            key: avl.key
                                                                        });
                                                                    }
                                                                }):null;

                                                                !compareTimeString(value, getFieldValue(avl.key+'_to'))?null:setFieldsValue({[avl.key+'_to']: `${addHours(value, 1)}`})
                                                            }}
                                                        >
                                                            {timeSlots.map(item => <Option key={item.value} value={item.value}>{item.label}</Option>)}
                                                        </Select>
                                                    )
                                                }
                                            </FormItem>
                                            <FormItem style={pricingStyles} className="per-hour-table-body-elm">
                                                {
                                                    getFieldDecorator(avl.key+'_to', {
                                                        initialValue: avl.to,
                                                        rules: [{required: true, message: 'This field is required'}]
                                                    })(
                                                        <Select
                                                            onChange={(value) => {

                                                                if (this.state.repeatedBoxesPerGuest.includes(avl.key)) {
                                                                    this.setRepeatedBoxesPerGuest({});
                                                                }

                                                                this.state.perGuestBookedHours?this.state.perGuestBookedHours.forEach(item => {
                                                                    if (item.key !== avl.key) {
                                                                        if (
                                                                            item.from === getFieldValue(avl.key+'_from') && 
                                                                            item.to === value &&
                                                                            item.day === Number(getFieldValue(avl.key+'_day'))
                                                                        ) {
                                                                            this.setRepeatedBoxesPerGuest({avl: avl.key, item: item.key});
                                                                        }
                                                                        this.setPerGuestBookedHours({
                                                                            from: getFieldValue(avl.key+'_from'),
                                                                            to: value,
                                                                            day: Number(Number(getFieldValue(avl.key+'_day'))),
                                                                            charge: getFieldValue(avl.key+'_charge'),
                                                                            key: avl.key
                                                                        });
                                                                    }
                                                                }):null;

                                                                !compareTimeString(value, getFieldValue(avl.key+'_to'))?null:setFieldsValue({[avl.key+'_to']: `${addHours(value, 1)}`})
                                                            }}
                                                        >
                                                            {hours.map(element => <Option disabled={compareTimeString(getFieldValue(avl.key+'_from'), element.value)} key={element.value} value={element.value}>{element.label}</Option>)}
                                                        </Select>
                                                    )
                                                }
                                            </FormItem>
                                            <FormItem style={pricingStyles} className="per-hour-table-body-elm">
                                                {
                                                    getFieldDecorator(avl.key+'_charge', {
                                                        initialValue: avl.charge,
                                                        rules: [{validator: this.validateMinPricePerGuest}]
                                                    })(
                                                        <Input  type="number" addonBefore="LKR"/>
                                                    )
                                                }
                                            </FormItem>
                                           
                                            <FormItem style={pricingStyles} className="per-hour-table-body-elm">
                                                {
                                                    getFieldDecorator(avl.key+'_guestMenu', {
                                                        initialValue: avl.menuItem?avl.menuItem.url:''
                                                    })(
                                                        <Select>
                                                            {this.props.menuFiles.map(item => <Option key={item.url} value={item.url}>{item.menuId}</Option>)}
                                                        </Select>
                                                    )
                                                }
                                            </FormItem>
                                            <FormItem style={pricingStyles} className="per-hour-table-body-elm">
                                                {
                                                    getFieldDecorator(avl.key+'_minCharge', {
                                                        initialValue: (Number(getFieldValue(avl.key+'_charge'))? (Number(getFieldValue(avl.key+'_charge')) * Number(getFieldValue('minNumberOfGuests'))) : (Number(getFieldValue(avl.key+'_charge')) * Number(getFieldValue('minNumberOfGuests')))).toString()
                                                    })(
                                                        <Input type="number" addonBefore="LKR" disabled/>
                                                    )
                                                }
                                            </FormItem>
                                            <div style={{width: 250}} onClick={()=> {
                                                
                                                
                                            }}><Icon onClick={()=>{this.removePerGuestBlock(avl.key)}} style={{border: '1px solid #e2e2e2', borderRadius: '50%', cursor: 'pointer', padding: 3}} type="close" /></div> 
                                        </div>
                                    )
                                }):null}
                            </div>
                    }
                    <div className="add-new-block-btn">
                        <Button disabled={this.state.repeatedBoxesPerGuest.length ===0 && this.state.repeatedBoxesSpaceOnly.length ===0?false:true} type="dashed" onClick={this.addNewBlock} style={{ width: '100%', fontWeight: 'bold' }}>
                            <Icon type="plus" />Add a block
                        </Button>
                    </div>
                </div>:null
            )
        }

        return (
            <Form onSubmit={this.onFormSubmit}>
                <div>
                    <FormItem
                        label="Minimum host notice period" 
                        style={{width: 300, display: 'inline-block', marginRight: 20}}
                    >
                        {getFieldDecorator('noticePeriod', {
                            rules: [{ 
                                required: true, 
                                validator: this.validateMinimumHostNoticePeriod 
                            }],
                            initialValue: noticePeriod,

                        })(
                            <InputNumber style={{width: '100%'}} addonAfter="Hrs" />
                        )}

                    </FormItem>

                    <FormItem
                        label="Valid till"
                        style={{width: 300, display: 'inline-block', marginRight: 20}}
                    >
                        {getFieldDecorator('calendarEnd', {
                            initialValue: moment(calendarEnd)
                        })(
                            <DatePicker allowClear={false} />
                        )}
                    </FormItem>
                </div>
    
                <div>
                   {this.props.availabilityMethod === 'BLOCK_BASE'?blockBasedBlocks():hourlyBlocks()}
                </div>

               

                <div style={{ textAlign: 'right' }}>
                    <Button type="primary" htmlType="submit" className="login-form-button" disabled={ !hasErrors(getFieldsError()) && this.state.repeatedBoxesSpaceOnly.length===0 && this.state.repeatedBoxesPerGuest.length===0?false:true}>Update</Button>
                </div>
            </Form>
        )
    }
}


export default Form.create({

    mapPropsToFields (props) {

        let obj = {};

        const { availability, availabilityMethod } = props;

        if (availability && availabilityMethod === 'HOUR_BASE') {

            availability.forEach(item => {
    
                obj = Object.assign({}, obj, {
                    [`${item.day}-avlHr-Availability`]: createFormField({ value: Boolean(item.isAvailable) }),
                    [`${item.day}-avlHr-From`]: createFormField({value: moment().utcOffset(0).set({hour:item.from.split(':')[0],minute:0,second:0,millisecond:0}).format(TIME_STRING_FORMAT).toString()}),
                    [`${item.day}-avlHr-To`]: createFormField({value: moment().utcOffset(0).set({hour:item.to.split(':')[0],minute:0,second:0,millisecond:0}).format(TIME_STRING_FORMAT).toString()}),
                    [`${item.day}-avlHr-ChargePerHour`]: createFormField({value: item.charge})
                });
            });
        }

        return obj;
    }
})(PricingAvailability);
