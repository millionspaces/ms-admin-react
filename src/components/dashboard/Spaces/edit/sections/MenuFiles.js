import React from 'react';
import { IMAGE_CLOUD_NAME, IMAGE_CLOUD_PRESET } from '../../../../../settings';
import { Icon, Popconfirm } from 'antd';

/**
 * Class containing Menu files upload component.
 */
class MenuFiles extends React.Component {

    state = {
        menuFiles: undefined
    }

    // ************************* Custom methods *************************

    /**
     * Removes an image from cloud and ms dbs.
     * @param {String} menu menu file
     * @param {SyntheticEvent} event Describes event occured
     */
    removeCoverImage = menu => event => {

        // Remove image from cloud.
        this.props.removeImageFromCloud (menu)

        // Remove space from space.
        
        let menuFiles = this.state.menuFiles.filter (img => {
            return img.url !== menu
        });

        this.setState ({
            menuFiles: [...menuFiles]
        });

        this.props.updateSpace({menuFiles});
        
    }

    /**
     * Add menu files to state
     * @param {Object} menuFile Object describing uploaded menu file. 
     */
    addMenuFiles = menuFile => {
        this.setState ({
            menuFiles: [...this.state.menuFiles, menuFile]
        });
    }
    
    /**
     * Opens Cloudinary widget.
     */
    openWidget = () => {
        
        // Configuring and openinig upload widget.
        window.cloudinary.openUploadWidget({
            cloud_name: IMAGE_CLOUD_NAME,
            upload_preset: IMAGE_CLOUD_PRESET,
            theme: 'white',
            folder: 'millionspaces',
            client_allowed_formats: ["png", "jpeg", "pdf"],
        }, (error, result) => {
            
            // If no errors then save image data in ms dbs.
            if (!error) {  

                const menuIds = [];

                this.state.menuFiles.forEach(item => {
                    item.menuId?menuIds.push(Number(item.menuId)):null;
                });

                // Menu file name is generated here.
                this.addMenuFiles({
                    url: result[0].public_id.split('/')[1],
                    menuId: result[0] && menuIds.length === 0 ? 1 : Math.max(...menuIds) + 1
                });

                this.props.updateSpace({ menuFiles: this.state.menuFiles })

                this.props.saveSpaceImages({
                    imageDetails: [{  
                        url: result[0].public_id.split('/')[1],
                        publicId: result[0].public_id,
                        deleteToken: result[0].delete_token,
                        etag: result[0].etag,
                        signature: result[0].signature,
                        secureUrl: result[0].secure_url
                    }]
                });
               
            }            
        });
    }

    // ************************* Life cycle methods *************************

    componentDidMount () {
        
        if (!this.state.menuFiles && this.props.menuFiles) {
            this.setState ({
                menuFiles: [...this.props.menuFiles]
            });
        }
    }

    render () {

        const { menuFiles } = this.state;

        return (
            <div>

                {/* If there are menues then render them */}
                {menuFiles?menuFiles.map(file => {
                    return <div key={file.id} style={{verticalAlign: 'middle', display: 'inline-block', marginRight: 20, maxWidth: 400}}>
                        <img style={{width: '100%'}} src={`http://res.cloudinary.com/${IMAGE_CLOUD_NAME}/image/upload/millionspaces/${file.url}.jpg`} />
                        <span style={{display: 'inline-block', marginTop: 10, width: '50%'}}>Menu {file.menuId}</span>
                        <Popconfirm title="Are you sure delete this menu?" onClick={this.removeCoverImage(file.url)} okText="Yes" cancelText="No"><Icon type="delete" style={{textAlign: 'right', color: 'red', width: '50%'}}/></Popconfirm>
                    </div>
                }):null}
                
                {/* Open widget button */}
                <div onClick={this.openWidget} style={{cursor: 'pointer', display: 'inline-block', verticalAlign: 'middle', padding: 50, border: '1px dashed #e2e2e2'}}><Icon style={{ fontSize: 32 }} type="plus" /></div>
            
            </div>
        )
    }
}

export default MenuFiles;