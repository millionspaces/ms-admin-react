import React from 'react';

import { Form, Input, Button } from 'antd';
import { hasErrors } from '../../../../../utils';

const FormItem = Form.Item;

/**
 * Class representing bank details component.
 */
class BankDetails extends React.Component {


    /**
     * Handles form submit.
     * @param {SyntheticEvent} e Describes the event triggered.
     */   
    handleSubmit = (e) => {

        // Prevent default action.
        e.preventDefault();

        // Validate form fields, If no errors then update space.
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.updateSpace(values);
            }
        });

    }

    // ------------------ Validation methods ------------------
    

    /**
     * Validates acount holder name.
     * @param {Object} rule Describes rule defined.
     * @param {String} value Value to be validated
     * @param {Function} callback callback to be executed with validations.
     */
    validateAccountHolderName = (rule, value, callback) => {
        if (value.length > 60) {
            callback ('Field character length exceeded. Max 60 characters.');
        } else if (value.length === 0) {
            callback ('This field is required');
        } else if (!/^[A-Za-z\s]+$/.test(value)) {
            callback ('Account holder name can not contain special characters or numbers');
        } else {
            callback ()
        }
    }

    /**
     * Validates account number.
     * @param {Object} rule Describes rule defined.
     * @param {String} value Value to be validated
     * @param {Function} callback callback to be executed with validations.
     */
    accountNumberValidator = (rule, value, callback) => {
        if (value.length===0) {
            callback('This field is required.');
        } else if (!/^[0-9]*$/.test(value)) {
            callback('Account number can not contain letters or special characters.');
        } else if (value.length > 34) {
            callback('Field character length exceeded. Max 34 characters.');
        } else {
            callback();
        }
    }

    /**
     * Validates bank.
     * @param {Object} rule Describes rule defined.
     * @param {String} value Value to be validated
     * @param {Function} callback callback to be executed with validations.
     */    
    validateBank = (rule, value, callback) => {
        if (value.length===0) {
            callback('This field is required.');
        } else if (value.length > 50) {
            callback('Field character length exceeded. Max 50 characters.');
        } else {
            callback();
        }
    }

    /**
     * Validates bank.
     * @param {Object} rule Describes rule defined.
     * @param {String} value Value to be validated
     * @param {Function} callback callback to be executed with validations.
     */  
    validateBankBranch = (rule, value, callback) => {
        if (value.length===0) {
            callback('This field is required.');
        } else if (value.length > 60) {
            callback('Field character length exceeded. Max 60 characters.');
        } else {
            callback();
        }
    }

    render () {

        const { getFieldDecorator, getFieldsError } = this.props.form;
        const { accountHolderName, accountNumber, bank, bankBranch } = this.props;

        return (
            <Form onSubmit={this.handleSubmit}>

                {/* Account holder name */}
                <FormItem label="Account Holder Name" style={{width: 300, display: 'inline-block', marginRight: 20}}>
                    {getFieldDecorator('accountHolderName', {
                        rules: [{ required: true, validator: this.validateAccountHolderName }],                        
                        initialValue: accountHolderName,
                        
                    })(
                        <Input placeholder="Ex: Adam" />
                    )}
                </FormItem>

                {/* Account Number */}
                <FormItem label="Account Number" style={{width: 300, display: 'inline-block', marginRight: 20}}>
                    {getFieldDecorator('accountNumber', {
                        rules: [{ required: true, validator: this.accountNumberValidator}],
                        initialValue: accountNumber
                    })(
                        <Input placeholder='Ex: 9123456789'  />
                    )}
                </FormItem>

                {/* Bank */}
                <FormItem label="Bank" style={{width: 300, display: 'inline-block', marginRight: 20}}>
                    {getFieldDecorator('bank', {
                        rules: [{ required: true, validator: this.validateBank }],
                        initialValue: bank
                    })(
                        <Input placeholder='Ex: Standard Chartered'  />
                    )}
                </FormItem>

                {/* Bank Branch */}
                <FormItem label="Bank Branch" style={{width: 300, display: 'inline-block', marginRight: 20}}>
                    {getFieldDecorator('bankBranch', {
                        rules: [{ required: true, validator: this.validateBankBranch}],
                        initialValue: bankBranch
                    })(
                        <Input placeholder="Ex: Colombo 01" />
                    )}
                </FormItem>

                {/* Update button */}
                <div style={{ textAlign: 'right' }}>
                    <Button disabled={hasErrors(getFieldsError())} type="primary" htmlType="submit" className="login-form-button">Update</Button>
                </div>

            </Form>
        );
    }
}

BankDetails = Form.create()(BankDetails);

export default BankDetails;