import React from 'react';

import { Spin, Icon, Popconfirm } from 'antd';

import { connect } from 'react-redux'
import { IMAGE_CLOUD_NAME, IMAGE_CLOUD_PRESET } from '../../../../../settings';
import { setSpaceImagesDetails, getImgDetailsAndRemoveImageFromCloudinery } from '../../../../../redux/actions/spaceImagesActions';

class SpaceImages extends React.Component {

    state = {
        images: [],
        thumbnailImage: undefined
    }

    addSpaceImage = (image) => {
        this.setState ({
            images: [...this.state.images, image]
        })
    }
    
    /**
     * set image data delete token, and url to state
     */
    addSpaceThumbnail = imageData => {
        this.setState (imageData);
    }

    /**
     * Opens Cloudinary widget.
     */
    uploadAndConfirmThumbnail = () => {
        window.cloudinary.openUploadWidget({
            cloud_name: IMAGE_CLOUD_NAME,
            upload_preset: IMAGE_CLOUD_PRESET,
            theme: 'white',
            folder: 'millionspaces',
            min_image_width: 1586,
            min_image_height: 814,
            client_allowed_formats: ["png", "jpeg"],
        }, (error, result) => {
            if (!error) {  

                this.addSpaceThumbnail({
                    thumbnailImage: result[0].public_id.split('/')[1],
                    thumbnailDeleteToken: result[0].delete_token
                });

                this.props.updateSpace({ thumbnailImage: result[0].public_id.split('/')[1] })

                this.props.saveSpaceImages({
                    imageDetails: [{  
                        url: result[0].public_id.split('/')[1],
                        publicId: result[0].public_id,
                        deleteToken: result[0].delete_token,
                        etag: result[0].etag,
                        signature: result[0].signature,
                        secureUrl: result[0].secure_url
                    }]
                });
               
            }            
        });
    }

    /**
     * openWidgetForCoverImages 
     */
    openWidgetForCoverImages = () => {
        window.cloudinary.openUploadWidget({
            cloud_name: IMAGE_CLOUD_NAME,
            upload_preset: IMAGE_CLOUD_PRESET,
            theme: 'white',
            folder: 'millionspaces',
            min_image_width: 1586,
            min_image_height: 814,
            client_allowed_formats: ["png", "jpeg"],
        }, (error, result) => {
            if (!error) {

                this.props.updateSpace({ images: [...this.state.images, result[0].public_id.split('/')[1]] });

                this.addSpaceImage (result[0].public_id.split('/')[1]);

                this.props.saveSpaceImages({
                    imageDetails: [{  
                        url: result[0].public_id.split('/')[1],
                        publicId: result[0].public_id,
                        deleteToken: result[0].delete_token,
                        etag: result[0].etag,
                        signature: result[0].signature,
                        secureUrl: result[0].secure_url
                    }]
                });
            }            
        });
    }


    /**
     * Removes thumbnail Image.
     */
    removeThumbnail = (event) => {

        this.props.removeImageFromCloud (this.state.thumbnailImage);

        this.setState ({
            thumbnailImage: ''
        });

        this.props.updateSpace({ thumbnailImage: '' });

        
    }

    removeCoverImage = image => event => {
        this.props.removeImageFromCloud (image)

        let images = this.state.images.filter (img => {
            return img !== image
        });

        this.setState ({
            images: [...images]
        });

        this.props.updateSpace({images});
    }


    // ---------------------------- Life cycle methods ----------------------------


    componentDidMount () {
        // Initial images passed from container component.
        this.setState({
            thumbnailImage: this.props.thumbnailImage,
            images: this.props.images
        });
    }

    render () {

        const { thumbnailImage, images } = this.state;

        // TODO: move styles to a seperate file
        return (
            <div>
                <div style={{display: 'inline-block', position: 'relative'}}>
                    <p style={{fontWeight: 'bold'}}>Thumbnail Image.</p><p style={{width: '100%', display: 'block'}}><span style={{color: 'tomato'}}>* </span>Please note once an image is uploaded it is immediately updated on www.millionspaces.com</p>
                    
                    <Popconfirm title="Are you sure delete this image?" onConfirm={this.removeThumbnail} okText="Yes" cancelText="No">
                        {thumbnailImage?<Icon 
                            type="delete"
                            style={{zIndex: 3, position: 'absolute', left: 145, margin: 10, padding: 10, background: '#fff', borderRadius: '50%', cursor: 'pointer', bottom: 0}}
                        />:null}    
                    </Popconfirm>
                    <div onClick={this.uploadAndConfirmThumbnail} style={{cursor: "pointer", display: 'flex', justifyContent: 'center', alignItems: 'center',width: 200, border: '1px dashed #e2e2e2', minHeight: 104}}>
                        <Spin spinning={false} >{thumbnailImage?<img style={{maxWidth: 278, width: '100%'}} alt="User thumbnail" src={`http://res.cloudinary.com/${IMAGE_CLOUD_NAME}/image/upload/millionspaces/${thumbnailImage}.jpg`} />:<Icon type="picture" style={{fontSize: 32, fontWeight: 'bold'}}/>}</Spin>
                    </div>
                </div>
                
                {<div>
                    
                    <p style={{marginTop: 20, fontWeight: 'bold'}}>Space Images.</p>
                    
                    {images.map(image => <div key={image} style={{position: 'relative', display: 'inline-block', marginRight: 20, verticalAlign: 'middle'}}>
                        <Popconfirm title="Are you sure delete this image?" onConfirm={this.removeCoverImage(image)} okText="Yes" cancelText="No">
                            <Icon 
                                type="delete"
                                style={{zIndex: 3, position: 'absolute', right: 0, margin: 10, padding: 10, background: '#fff', borderRadius: '50%', cursor: 'pointer', bottom: 0}}
                            />    
                        </Popconfirm>
                        <div style={{cursor: "pointer", display: 'flex', justifyContent: 'center', alignItems: 'center',minWidth: 200, minHeight: 150, border: '1px dashed #e2e2e2'}}>
                            <img style={{maxWidth: 300}} src={`http://res.cloudinary.com/${IMAGE_CLOUD_NAME}/image/upload/millionspaces/${image}.jpg`} />
                        </div>
                    </div>)}

                    <div onClick={this.openWidgetForCoverImages} style={{display: 'inline-block', padding: '60px', marginTop: 10, border: '1px dashed #e2e2e2', cursor: 'pointer',  verticalAlign: 'middle'}}><Icon style={{fontSize: 22}} type="plus" style={{fontWeight: 'bold', fontSize: 32}} /></div>

                </div>}
               
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        saveSpaceImages: imageData => dispatch(setSpaceImagesDetails(imageData)),
        removeImageFromCloud: url => dispatch(getImgDetailsAndRemoveImageFromCloudinery(url))
    }
}

export default connect (null, mapDispatchToProps) (SpaceImages);