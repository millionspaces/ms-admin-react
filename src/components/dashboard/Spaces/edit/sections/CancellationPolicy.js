import React from 'react';
import { Form, Radio, Button } from 'antd';

const FormItem = Form.Item;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

/**
 * Class representing cancellation policy component.
 */
class CancellationPolicy extends React.Component {

    /**
     * Handle form submit.
     * @param {SyntheticEvent} e Event triggered.
     */
    handleSubmit = e => {

        // Prevent default action.
        e.preventDefault();

        // Validating values if no errors then update profile.
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.updateSpace(values);
            }
        });

    }

    render () {

        const { getFieldDecorator } = this.props.form;
        const { cancellationPolicies } = this.props;

        return (
            <Form onSubmit={this.handleSubmit}>

                {/* Cancellation policies */}
                <FormItem>
                    {getFieldDecorator('cancellationPolicy', {
                        rules: [{ required: true, message: 'Please select cancellation policy' }],
                        initialValue: this.props.cancellationPolicy
                    })(
                        <RadioGroup style={{marginRight: 20}}>
                            {cancellationPolicies.map(item => <RadioButton key={item.id} value={item.id}>{item.name}</RadioButton>)}
                        </RadioGroup>
                    )}                
                </FormItem>
                
                {/* Cancellation policy rules descriptions. */}
                <div><span style={{fontWeight: 'bold'}}>Flexible:</span> Full refund up to two days prior to event</div>
                <div><span style={{fontWeight: 'bold'}}>Moderate:</span> 50% refund up to one week prior to event</div>
                <div><span style={{fontWeight: 'bold'}}>Strict:</span> No refund</div>

                {/*  Update Button */}
                <div style={{ textAlign: 'right' }}>
                    <Button type="primary" htmlType="submit" className="login-form-button">Update</Button>
                </div>

            </Form>
        )
    }
    
}

CancellationPolicy = Form.create()(CancellationPolicy);
export default CancellationPolicy;