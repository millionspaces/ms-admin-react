import React from 'react';

// Redux
import { getSpacesSetActionCreator, toggleSpaceActiveStateActionCreator, clearSpaceDetailsActionCreator, getSpacesForIdActionCreator } from '../../../redux/actions/spacesActions';
import { connect } from 'react-redux';

// Ant Design
import { Layout, Table, Pagination, Switch, Icon, Row, Col, Form, Input, Button} from 'antd';

// React Router
import { Link } from 'react-router-dom';
import { setCurrentScreen } from '../../../redux/actions/screensActions';

const { Content } = Layout;
const FormItem = Form.Item;
/**
 * Class representing Spaces Component
 */
class Spaces extends React.Component {

    state = {
        fetching: false,
        spaces: [],
        currentPage: 1,
        totalPages: 0,
        spaceToggleFetching: false,
        prevToggledId: undefined
    }

    /**
     * Triggers on pagination changes.
     * @param {Number} pageNumber Requested page number.
     */
    onPaginationChange = (pageNumber) => {

        this.props.getSpacesSet(pageNumber - 1);

        this.setState({
            currentPage: pageNumber
        });
    }

    /**
     * Triggers on pagination status switch changes.
     * @param {Number} id Id of space.
     * @param {Boolean} checked Switch state.
     */
    onSpaceStatusChange = id => checked => {
        this.props.toggleSpaceStatus(id);
        this.setState({
            prevToggledId: id
        })

    }
    // --------------------- Life cycle methods ----------- -----------
    componentWillReceiveProps(nextProps) {

        let { fetching, spaces, totalPages, spaceToggleFetching, spaceToggledId } = nextProps;

        this.setState({
            fetching
        });

        if (spaces) {

            let temp = [];

            spaces.forEach(item => {
                temp.push({
                    key: item.id,
                    id: item.id,
                    spaceName: item.name,
                    hostName: item.hostName,
                    organization: item.organizationName,
                    status: item.approved
                });
            });

            this.setState({
                spaces: [...temp]
            });
        }

        if (totalPages) {
            this.setState({
                totalPages
            });
        }

        // If a space state is changed then refetch the api
        // TODO: Tuneup performance
        if (spaceToggledId) {
            this.setState({
                spaceToggleFetching
            });
            if (this.state.prevToggledId === spaceToggledId) {
                this.props.getSpacesSet(this.state.currentPage-1);
                this.setState({ prevToggledId: 0 })
            }
        }

    }

    componentDidMount() {
        this.props.getSpacesSet(this.state.currentPage-1);
        this.props.setScreen('Spaces')
        this.props.clearSpaceDetails();
    }

    handleSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, values) => {
            if (!err) {
                if(values.bookingId){
                    const { REACT_APP_MS_ENV } = process.env;
                    values.bookingId.includes(REACT_APP_MS_ENV==='qae' ? 'MQ' : 'ML') ? 
                        this.props.getSpacesForIdActionCreator(values.bookingId.split(REACT_APP_MS_ENV==='qae' ? 'MQ' : 'ML')[1])
                        :   this.props.getSpacesForIdActionCreator(values.bookingId)
                }else{
                    this.props.getSpacesSet(this.state.currentPage-1);
                }
            }
        });
    }

    render() {

        let { fetching, spaces } = this.state;
        const { totalPages } = this.props;
        const { getFieldDecorator } = this.props.form;

        const columns = [{
            title: 'Id',
            dataIndex: 'id',
            key: 'id',
            width: 100
        }, {
            title: 'Space Name',
            dataIndex: 'spaceName',
            key: 'spaceName'
        }, {
            title: 'Host Name',
            dataIndex: 'hostName',
            key: 'hostName'
        }, {
            title: 'Organization',
            dataIndex: 'organization',
            key: 'organization'
        }, {
            title: 'Status',
            key: 'status',
            width: 80,
            render: (text, record) => (
                <Switch checked={Boolean(record.status)} onChange={this.onSpaceStatusChange(record.id)} />
            )
        }, {
            title: 'Edit',
            dataIndex: 'edit',
            width: 60,
            render: (text, record) => (
                <Link to={`/spaces/${record.id}`} style={{textAlign : "center"}}><Icon type="edit" /></Link>
            )
        }]

        return (
            <Layout style={{ margin: 10 }}>
                
                <Content style={{ flex: 'initial', textAlign: 'right', marginTop: 20, marginBottom: 20 }}>
                    <Row>
                        <Col span={8} style={{textAlign : 'left'}}>
                                <Form layout="inline" onSubmit={this.handleSubmit}>
                                        <FormItem>
                                        {getFieldDecorator('bookingId')(<Input placeholder="Id" />)}
                                        </FormItem>
                                        <FormItem>
                                        <Button style={{ height: '29px'}}type="primary" htmlType="submit">Search</Button>
                                    </FormItem>
                                </Form>
                        </Col> 
                        {spaces.length > 0?
                            <Col span={16}> 
                        Showing {10} of {totalPages} spaces <Pagination style={{marginLeft: 20, display: 'inline-block', verticalAlign: 'middle'}} showQuickJumper defaultCurrent={this.state.currentPage} total={totalPages} onChange={this.onPaginationChange} />
                        </Col>:null}
                    </Row> 
                </Content>
                <Content style={{ background: '#fff' }}>
                    <Table  bordered pagination={false} loading={fetching} columns={columns} dataSource={spaces} />
                </Content>
            
            </Layout>
        );
    }
}

Spaces =  Form.create()(Spaces);

const mapStateToProps = ({ spaces, spaceToggle }) => {

    const { data, fetching } = spaces;

    return {
        spaces: data ? data.spaces : undefined,
        fetching,
        totalPages: data ? data.count : 1,
        spaceToggleFetching: spaceToggle.fetching,
        spaceToggledId: spaceToggle.id
    }
};

const mapDispatchToProps = dispatch => ({
    getSpacesSet: page => dispatch(getSpacesSetActionCreator(page)),
    toggleSpaceStatus: id => dispatch(toggleSpaceActiveStateActionCreator(id)),
    clearSpaceDetails: () => dispatch(clearSpaceDetailsActionCreator()),
    setScreen: screen => dispatch(setCurrentScreen(screen)),
    getSpacesForIdActionCreator: id => dispatch(getSpacesForIdActionCreator(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Spaces);