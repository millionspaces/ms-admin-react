import React, { Component } from 'react';
import {XYPlot, LineSeries, XAxis, YAxis, VerticalGridLines, HorizontalGridLines, Crosshair} from 'react-vis';
import {Row, Col, Button, Icon} from 'antd'
import '../../../../../node_modules/react-vis/dist/style.css';
import DateRange from '../../../common/DateRange';
import { connect} from 'react-redux';
import {getUserStatistics} from '../../../../redux/actions/statisticsActions';
import analyticsHelper from '../../../../helper/AnalyticHelper';
import moment from 'moment';
import './User.css'

class Users extends Component {
    state = {
        startDate       :   "",
        endDate         :   "",
        crosshairValues :   [],
        data            :   [],
        dates           :   []
    }

    componentDidMount() {
        this.props.getUserStatistics(moment().add(-1,'days').format('YYYY-MM-DD').toString(), moment().format('YYYY-MM-DD').toString());    
    }

    handleStartDate = (startDate) => {
        this.setState({startDate : moment(startDate).format('YYYY-MM-DD')});
    }

    handleEndDate = (endDate)  => {
        this.setState({endDate : moment(endDate).format('YYYY-MM-DD')});        
    }

    handlDatesSubmit = () => {
        const {startDate, endDate} = this.state;
        this.props.getUserStatistics(startDate, endDate)
    }

    componentWillReceiveProps(nextProps) {
        let {startDate, endDate} = this.state;
        let formatterdDates = []
        if (!startDate || !endDate) {
            formatterdDates = analyticsHelper.setStartAndEndDates(moment().add(-1,'days').format('YYYY-MM-DD').toString(), moment().format('YYYY-MM-DD').toString(), nextProps.users);
        }else{
            formatterdDates = analyticsHelper.setStartAndEndDates(startDate, endDate, nextProps.users);
        }
       
        let formatedTotalUsers  = formatterdDates.map(user => { return {x : new Date(moment(user.date, 'YYYY-MM-DD').format('MM DD YYYY').toString()), y : user.total} } )
        let formatedUsers       = formatterdDates.map(user => { return {x : new Date(moment(user.date, 'YYYY-MM-DD').format('MM DD YYYY').toString()), y : user.user} } )
        let formatedGuests      = formatterdDates.map(user => { return {x : new Date(moment(user.date, 'YYYY-MM-DD').format('MM DD YYYY').toString()), y : user.guest} } )
        let dates               = formatedTotalUsers.map(date => date.x)
        this.setState({
            data            : [formatedTotalUsers, formatedUsers, formatedGuests],
            dates           : dates
        });
    }

    _onMouseLeave = () => {
        this.setState({crosshairValues: []});
    };

    handlOnxyChange = (value, {index}) => {
        let {data} = this.state;
        var formatedValues =  data.map(k => 
            { return {x : new Date(moment(k[index].x).format('MM DD YYYY').toString()).toString(), y : k[index].y} 
        });
        this.setState({crosshairValues: formatedValues });
      };

    render() {
        const {crosshairValues, data, dates} = this.state;
        return (
            <div>
                <Row style={{marginBottom : '15px'}}>
                    <Col md={{ span: 20, offset: 4 }}>
                        <Col md={{ span: 9}} style= {{textAlign:'center'}}>
                            <DateRange 
                                handleStartDate={this.handleStartDate} 
                                handleEndDate={this.handleEndDate}
                                />
                        </Col>
                        <Col md={{ span: 5}} style= {{textAlign:'left'}}>
                            <Button type="primary" onClick={this.handlDatesSubmit}><Icon type='search'></Icon> Search</Button>
                        </Col>
                    </Col>
                </Row>
                <Row>
                    <Col md={{ span: 20, offset: 4 }}>
                        <XYPlot onMouseLeave={this._onMouseLeave}
                            xType="time"
                            width={1000}
                            height={300}>
                            <VerticalGridLines />
                            <HorizontalGridLines />
                            <XAxis
                                title="Dates"
                                tickValues={dates}
                            />
                            <YAxis hideTicks />

                            <YAxis />
                            <LineSeries
                                onNearestXY={this.handlOnxyChange}
                                data={data[0]}
                            />
                            <LineSeries
                                data={data[1]}
                                color={'red'} />
                             <LineSeries
                                data={data[2]}
                                color={'green'} /> 
                            {crosshairValues ? 
                                        <Crosshair values={crosshairValues} className={'test-class-name'} > 
                                            <div style={{background: 'black', width : '200px', padding : '15px', opacity : 0.7}}>
                                                <h3 style={{color: 'white'}}>{crosshairValues[0] && moment(crosshairValues[0].x).format('YYYY-MM-DD')}</h3>
                                                <p>Total Signups: {crosshairValues[0] && crosshairValues[0].y}</p>
                                                <p>Traditional Signups: {crosshairValues[1] && crosshairValues[1].y}</p>
                                                <p>Guests Signups: {crosshairValues[2] && crosshairValues[2].y}</p>
                                            </div>
                                        </Crosshair> : null }
                        </XYPlot>
                    </Col>
                </Row>
                <Row>
                    <Col md={{ span: 15, offset: 9 }}>
                        <div style={{display : 'inline-flex', position : 'relative', marginRight : '8px'}}>
                            <div style={{height : '4px', width : '25px', backgroundColor : 'rgb(18, 147, 154)', marginTop: '8px', marginRight : '4px'}}></div>
                            <div>Total Signups</div>
                        </div>
                        <div style={{display : 'inline-flex', position : 'relative', marginRight : '8px'}}>
                            <div style={{height : '4px', width : '25px', backgroundColor : '#fc3c0c', marginTop: '8px', marginRight : '4px'}}></div>
                            <div>Traditional Signups</div>
                        </div>
                        <div style={{display : 'inline-flex', position : 'relative', marginRight : '8px'}}>
                            <div style={{height : '4px', width : '25px', backgroundColor : '#338118', marginTop: '8px', marginRight : '4px'}}></div>
                            <div>Guests Signups</div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = ({statistics}) => {
    let users = statistics.data && statistics.data.user ? statistics.data.user : []
     return {
       users
    }
};

const mapDispatchToProps = dispatch => ({
    getUserStatistics: (startDate, endDate) => dispatch(getUserStatistics(startDate, endDate))
});


export default connect(mapStateToProps, mapDispatchToProps)(Users);