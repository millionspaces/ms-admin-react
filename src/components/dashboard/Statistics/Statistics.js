import React, { Component } from 'react';
import Users from './StatisticsList/Users';

class Statistics extends Component {
    render() {
        return (
            <div style={{padding: 16, margin: 16, background: '#fff'}}>
                <Users/>
            </div>
        );
    }
}

export default Statistics;