import React, { Component } from 'react';
import { connect} from 'react-redux';
import { Layout, Spin, Collapse} from 'antd';
import BookingSummaryReport from './ReportsList/BookingSummaryReport';
import { setCurrentScreen } from '../../../redux/actions/screensActions';
import SpaceReport from './ReportsList/SpaceReport';
import BookingSummarySpaceReport from './ReportsList/BookingSummarySpaceReport';
import BookingSummaryUserReport from './ReportsList/BookingSummaryUserReport';
import BookingReport from './ReportsList/BookingReport';
import UserReport from './ReportsList/UserReport';

const Panel = Collapse.Panel;

const panelStyles = {
    background: '#fff',
    borderRadius: 3,
    marginBottom: 21,
    border: 0,
    overflow: 'hidden'
}

class Reports extends Component {

    componentDidMount() {
        this.props.setScreen('Reports')
    }
    
    render() {
        const {fetching} = this.props;

        return (
            <Spin spinning={fetching}>
                <Layout style={{background: '#fff', margin: 20}}>
                    <Collapse bordered={false}  style={{background: '#f0f2f5'}} destroyInactivePanel={true} accordion>
                    
                        <Panel key={0} header={<h3 style={{marginBottom: 0}}>Booking</h3>} style={panelStyles}>
                            <BookingReport/>
                        </Panel>

                        <Panel key={1} header={<h3 style={{marginBottom: 0}}>Users</h3>} style={panelStyles}>
                            <UserReport/>
                        </Panel>

                        {/* <Panel key={2} header={<h3 style={{marginBottom: 0, display:'none'}}>Booking Summary</h3>} style={panelStyles} display="none">
                            <BookingSummaryReport/>
                        </Panel>

                        <Panel key={3} header={<h3 style={{marginBottom: 0}}>Space Report</h3>} style={panelStyles}>
                           <SpaceReport/>
                        </Panel>

                        <Panel key={4} header={<h3 style={{marginBottom: 0}}>Booking Summary By Space</h3>} style={panelStyles}>
                           <BookingSummarySpaceReport/>
                        </Panel>

                        <Panel key={5} header={<h3 style={{marginBottom: 0}}>Booking Summary By User</h3>} style={panelStyles}>
                           <BookingSummaryUserReport/>
                        </Panel> */}

                    </Collapse>
                </Layout>
            </Spin>
        );
    }
}

const mapStateToProps = ({report}) => {

    return {
        fetching :  report.fetching
    }
};

const mapDispatchToProps = dispatch => ({
   setScreen: screen => dispatch(setCurrentScreen(screen)),
});


export default connect(mapStateToProps, mapDispatchToProps)(Reports);