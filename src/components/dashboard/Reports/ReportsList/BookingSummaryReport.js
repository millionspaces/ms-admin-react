import React, { Component, Fragment } from 'react';
import { Table, Button, Row, Col, Icon } from 'antd';
import { connect} from 'react-redux';
import {getBookingSummaryReportActionCreator} from '../../../../redux/actions/reportsAction';
import XLSX from 'xlsx';

class BookingSummaryReport extends Component {

    componentDidMount() {
        this.props.getBookingSummaryReportActionCreator('bookingsummary', 'booking_summary_report');
    }
    
    handleOnDownloadOnClick = () => {
        let wscols = [
            {wch:10},
            {wch:10},
            {wch:12},
            {wch:12},
            {wch:12}
        ];

        let table = document.getElementById('detailTable')
        let worksheet = XLSX.utils.table_to_sheet(table);
        worksheet['!cols'] = wscols;
        let new_workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(new_workbook, worksheet, "SheetJS");
        XLSX.writeFile(new_workbook, "Booking_SummaryReport.xlsx");
    }

    render() {
        let {reportDetails} = this.props
        const columns = [{
            title: 'Date',
            dataIndex: 'date',
            key: 'date',
        },{
            title: 'Total Charge',
            dataIndex: 'totalCharge',
            key: 'totalCharge'
        }, {
            title: 'Total Booking',
            dataIndex: 'totalBooking',
            key: 'totalBooking',
        }, {
            title: 'Paid Bookings',
            key: 'paidBookings',
            dataIndex: 'paidBookings'
        }, {
            title: 'Unpid Bookings',
            key: 'unPaidBookings',
            dataIndex: 'unPaidBookings'
        }];

        return (
            <Fragment>
                <Row>
                    <Col md={{ span: 4, offset: 20 }} style={{textAlign : "right", marginBottom : '5px'}}>
                        <Button type="primary" onClick={this.handleOnDownloadOnClick}><Icon type='file-excel'></Icon> Download</Button>
                    </Col>
                </Row>
                <Table id={'detailTable'} rowKey='refId' bordered style={{background: '#fff', height: '300px', overflowY: 'scroll'}} pagination={false} loading={this.props.fetching} columns={columns} dataSource={reportDetails}/>
            </Fragment>
        );
    }
}

const mapStateToProps = ({report}) => {
    const bookingsummary = report && report.bookingSummary ? report.bookingSummary : []
     
    return {
        fetching :  report.fetching,
        reportDetails : bookingsummary
    }
};

const mapDispatchToProps = dispatch => ({
    getBookingSummaryReportActionCreator : (report, path) => dispatch(getBookingSummaryReportActionCreator(report, path))
});

export default connect(mapStateToProps, mapDispatchToProps)(BookingSummaryReport);