import React, { Component, Fragment } from 'react';
import { Table, Button, Row, Col, Icon } from 'antd';
import { connect} from 'react-redux';
import {bookingSummaryBySpaceActionCreator} from '../../../../redux/actions/reportsAction';
import XLSX from 'xlsx';

class BookingSummarySpaceReport extends Component {

    componentDidMount() {
        this.props.bookingSummaryBySpaceActionCreator('booking_summary_by_space');
    }
    
    handleOnDownloadOnClick = () => {
        let wscols = [
            {wch:25},
            {wch:7},
            {wch:25},
            {wch:12}
        ];

        let table = document.getElementById('bookingsummarybyspacetable')
        let worksheet = XLSX.utils.table_to_sheet(table);
        worksheet['!cols'] = wscols;
        let new_workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(new_workbook, worksheet, "SheetJS");
        XLSX.writeFile(new_workbook, "Booking_Summary_by_Space.xlsx");
    }

    render() {
        let {reportDetails} = this.props
        const columns = [{
            title: 'Space Name',
            dataIndex: 'spaceName',
            key: 'spaceName',
        },{
            title: 'Space Id',
            dataIndex: 'spaceId',
            key: 'spaceId'
        }, {
            title: 'Organization Name',
            dataIndex: 'oganizationName',
            key: 'oganizationName',
        }, {
            title: 'Booking Count',
            key: 'bookingCount',
            dataIndex: 'bookingCount'
        }];

        return (
            <Fragment>
                <Row>
                    <Col md={{ span: 4, offset: 20 }} style={{textAlign : "right", marginBottom : '5px'}}>
                        <Button type="primary" onClick={this.handleOnDownloadOnClick}><Icon type='file-excel'></Icon> Download</Button>
                    </Col>
                </Row>
                <Table id={'bookingsummarybyspacetable'} rowKey='spaceId' bordered style={{background: '#fff', height: '300px', overflowY: 'scroll'}} pagination={false} loading={this.props.fetching} columns={columns} dataSource={reportDetails}/>
            </Fragment>
        );
    }
}

const mapStateToProps = ({report}) => {
    const bookingSummarySpace = report && report.bookingSummaryStatus ? report.bookingSummaryStatus : [] 
     
    return {
        fetching :  report.fetching,
        reportDetails : bookingSummarySpace
    }
};

const mapDispatchToProps = dispatch => ({
    bookingSummaryBySpaceActionCreator : (path) => dispatch(bookingSummaryBySpaceActionCreator(path))
});

export default connect(mapStateToProps, mapDispatchToProps)(BookingSummarySpaceReport);