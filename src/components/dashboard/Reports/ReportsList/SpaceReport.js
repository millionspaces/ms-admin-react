import React, { Component, Fragment } from 'react';
import { Table, Button, Row, Col, Icon } from 'antd';
import { connect} from 'react-redux';
import {getStatusActionCreator} from '../../../../redux/actions/reportsAction';
import XLSX from 'xlsx';
import ReportHelper from '../../../../helper/ReportHelper';

class SpaceReport extends Component {

    componentDidMount() {
        this.props.getStatusActionCreator('spaces', 'space_report');
    }
    
    handleOnDownloadOnClick = () => {
        const {reportDetails} = this.props
        const spaceCharactoList = ReportHelper.getCharatorsCount(reportDetails);
        let wscols = [
            {wch:spaceCharactoList.Space_Name},
            {wch:spaceCharactoList.Pricing_Category},
            {wch:spaceCharactoList.Event_Types},
            {wch:spaceCharactoList.Organization_Name},
            {wch:spaceCharactoList.Space_Id}
        ];

        let table = document.getElementById('spaceTable')
        let worksheet = XLSX.utils.table_to_sheet(table);
        worksheet['!cols'] = wscols;
        let new_workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(new_workbook, worksheet, "SheetJS");
        XLSX.writeFile(new_workbook, "Spaces.xlsx");
    }

    render() {
        let {reportDetails} = this.props
        const columns = [{
            title: 'Space Name',
            dataIndex: 'Space_Name',
            key: 'Space_Name',
        },{
            title: 'Pricing Category',
            dataIndex: 'Pricing_Category',
            key: 'Pricing_Category'
        }, {
            title: 'Event Types',
            dataIndex: 'Event_Types',
            key: 'Event_Types',
        }, {
            title: 'Organization Name',
            key: 'Organization_Name',
            dataIndex: 'Organization_Name'
        }, {
            title: 'Space Id',
            key: 'Space_Id',
            dataIndex: 'Space_Id'
        }];

        return (
            <Fragment>
                <Row>
                    <Col md={{ span: 4, offset: 20 }} style={{textAlign : "right", marginBottom : '5px'}}>
                        <Button type="primary" onClick={this.handleOnDownloadOnClick}><Icon type='file-excel'></Icon> Download</Button>
                    </Col>
                </Row>
                <Table id={'spaceTable'} rowKey='Space_Id' bordered style={{background: '#fff', height: '300px', overflowY: 'scroll'}} pagination={false} loading={this.props.fetching} columns={columns} dataSource={reportDetails}/>
            </Fragment>
        );
    }
}

const mapStateToProps = ({report}) => {
    const spaces = report && report.spaces ? report.spaces : [] 
     
    return {
        fetching :  report.fetching,
        reportDetails : spaces
    }
};

const mapDispatchToProps = dispatch => ({
    getStatusActionCreator : (report, path) => dispatch(getStatusActionCreator(report, path))
});

export default connect(mapStateToProps, mapDispatchToProps)(SpaceReport);