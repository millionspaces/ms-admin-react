import React, { Component, Fragment } from 'react';
import { Table, Button, Row, Col, Icon, DatePicker} from 'antd';
import { connect} from 'react-redux';
import {getBookingReportActionCreator} from '../../../../redux/actions/reportsAction';
import XLSX from 'xlsx';
import ReportHelper from '../../../../helper/ReportHelper';
import moment from 'moment';

class BookingReport extends Component {

    state = {
        reportDetails : []
    }
    componentDidMount() {
        var startOfWeek = moment(moment().format('YYYY-MM-DD')).startOf('isoWeek').format('YYYY-MM-DD');
        var endOfWeek = moment(moment().format('YYYY-MM-DD')).endOf('isoWeek').format('YYYY-MM-DD');

        this.props.getBookingReportActionCreator('booking_report', startOfWeek, endOfWeek);
    }

    async componentWillReceiveProps(nextProps) {
        let {reportDetails} = nextProps
        
        let cumulativeCashInflow = ReportHelper.getCumulativeCashInflow(reportDetails);
        let cumulativeMSContributedDiscount = ReportHelper.getCumulativeMSContributedDiscount(cumulativeCashInflow);
        let cumulativeCommissionRevenue = ReportHelper.getCumulativeCommissionRevenue(cumulativeMSContributedDiscount)
        await this.setState({
            reportDetails : cumulativeCommissionRevenue
        });
    }
    

    handleDateOnChange = (date, dateString) => {
        var startOfWeek = moment(moment(dateString).format('YYYY-MM-DD')).startOf('isoWeek').format('YYYY-MM-DD');
        var endOfWeek = moment(moment(dateString).format('YYYY-MM-DD')).endOf('isoWeek').format('YYYY-MM-DD');
        
        this.props.getBookingReportActionCreator('booking_report', startOfWeek, endOfWeek);
    }
        
    handleOnDownloadOnClick = () => {
        const {reportDetails} = this.state;
        const wscols = ReportHelper.getCharactorsCountOfTableColumns(reportDetails)
       
        let table = document.getElementById('bookingReportDetails')
        let worksheet = XLSX.utils.table_to_sheet(table);
        worksheet['!cols'] = wscols;
        let new_workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(new_workbook, worksheet, "SheetJS");
        XLSX.writeFile(new_workbook, "Booking.xlsx");
    }

    disabledDate = (current) => {
        return current && current > moment().endOf('day');
    }

    render() {
        let {reportDetails} = this.state;
        const columns = [
        {
            title: 'REF ID',
            key: 'REF_ID',
            dataIndex: 'REF_ID',
            width: 200
        },{
            title: 'Event Date',
            key: 'Event_Date',
            dataIndex: 'Event_Date',
            width: 200
        },{
            title: 'Space Name',
            key: 'Space_Name',
            dataIndex: 'Space_Name',
            width: 200
        },{
            title: 'Organization Name',
            dataIndex: 'Organization_Name',
            key: 'Organization_Name',
            width: 200
        },{
            title: 'Booking Value  before discount(LKR)',
            dataIndex: 'Booking_Value_Before_Discount',
            key: 'Booking_Value_Before_Discount',
            width: 200
        },{
            title: 'MS-Contributed Discount (LKR)',
            key: 'MS_Contributed_Discount',
            dataIndex: 'MS_Contributed_Discount',
            width: 200
        },{
            title: 'Host-Contributed Discount (LKR)',
            key: 'HOST_Contributed_Discount',
            dataIndex: 'HOST_Contributed_Discount',
            width: 200
        },{
            title: 'Cash Inflow after Discount (LKR)',
            key: 'Cash_Flow_After_Discount',
            dataIndex: 'Cash_Flow_After_Discount',
            width: 200
            
        },{
            title: 'Commission %',
            dataIndex: 'Commision',
            key: 'Commision',
            width: 200
        },{
            title: 'Host Payment (LKR)',
            dataIndex: 'Host_Payment',
            key: 'Host_Payment',
            width: 200
        },{
            title: 'Commission Revenue (LKR)',
            dataIndex: 'Commission_Revenue',
            key: 'Commission_Revenue',
            width: 200
        },{
            title: 'Promo Code Applied',
            key: 'Promo_Code_Applied',
            dataIndex: 'Promo_Code_Applied',
            width: 200
        },{
            title: 'Booking Received Date',
            key: 'Booking_Received_Date',
            dataIndex: 'Booking_Received_Date',
            width: 200
        },{
            title: 'Review Received?',
            key: 'Review_Received',
            dataIndex: 'Review_Received',
            width: 200
        },{
            title: 'Cumulative Cash Inflow (LKR)',
            key: 'CumulativeCashInflow',
            dataIndex: 'CumulativeCashInflow',
            width: 200
        },{
            title: 'Cumulative Commission Revenue (LKR)',
            key: 'cumulativeCommissionRevenue',
            dataIndex: 'cumulativeCommissionRevenue',
            width: 200
        },{
            title: 'Cumulative MS-Contributed Discount (LKR)',
            key: 'cumulativeMSContributedDiscount',
            dataIndex: 'cumulativeMSContributedDiscount',
            width: 200
        },{
            title: 'Nett Commission Revenue (LKR)',
            key: 'Net_Commission_Revenue',
            dataIndex: 'Net_Commission_Revenue',
            width: 200
        },{
            title: 'Payment Mode',
            key: 'Payment_Method',
            dataIndex: 'Payment_Method',
            width: 200
        },{
            title: 'Guest Name',
            key: 'Guest_Name',
            dataIndex: 'Guest_Name',
            width: 200
        },{
            title: 'EventType',
            key: 'Event_Type',
            dataIndex: 'Event_Type',
            width: 200
        },{
            title: 'Guest Contact Number',
            key: 'Guest_Moble',
            dataIndex: 'Guest_Moble',
            width: 200
        },{
            title: 'Guest Email',
            dataIndex: 'Guest_Email',
            key: 'Guest_Email',
            width: 200
        }, {
            title: 'Is Advance',
            key: 'Is_Advance',
            dataIndex: 'Is_Advance',
            width: 200
        },{
            title: 'Host Payment Deadline',
            key: 'Host_Payment_Deadline',
            dataIndex: 'Host_Payment_Deadline',
            width: 200
        }];

        return (
            <Fragment>
                <Row>
                    <Col md={{ span: 24 }}>
                        <Col md={{ span: 8}} style= {{textAlign:'left'}}>
                            <DatePicker 
                                onChange={this.handleDateOnChange}
                                disabledDate={this.disabledDate}
                             />
                        </Col>
                        <Col md={{ span: 4, offset: 12}} style={{textAlign : "right", marginBottom : '5px'}}>
                            <Button type="primary" onClick={this.handleOnDownloadOnClick}><Icon type='file-excel'></Icon> Download</Button>
                         </Col>
                    </Col>
                </Row>
                <Table id={'bookingReportDetails'} rowKey='REF_ID' bordered style={{background: '#fff'}} pagination={false} loading={this.props.fetching} columns={columns} dataSource={reportDetails} scroll={{ x: 5000, y: 300 }}/>
            </Fragment>
        );
    }
}

const mapStateToProps = ({report}) => {
    const bookings= report && report.bookings ? report.bookings : []
    return {
        fetching :  report.fetching,
        reportDetails : bookings
    }
};

const mapDispatchToProps = dispatch => ({
    getBookingReportActionCreator : (reportName, startOfWeek, endOfWeek) => dispatch(getBookingReportActionCreator(reportName, startOfWeek, endOfWeek))
});

export default connect(mapStateToProps, mapDispatchToProps)(BookingReport);