import React, { Component, Fragment } from 'react';
import { Table, Button, Row, Col, Icon } from 'antd';
import { connect} from 'react-redux';
import {bookingSummaryByUserActionCreator} from '../../../../redux/actions/reportsAction';
import XLSX from 'xlsx';

class BookingSummaryUserReport extends Component {

    componentDidMount() {
        this.props.bookingSummaryByUserActionCreator('booking_summary_by_user');
    }
    
    handleOnDownloadOnClick = () => {
        let wscols = [
            {wch:25},
            {wch:13},
            {wch:25},
            {wch:12},
            {wch:12}
        ];

        let table = document.getElementById('bookingsummaryByUser');
        let worksheet = XLSX.utils.table_to_sheet(table);
        worksheet['!cols'] = wscols;
        let new_workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(new_workbook, worksheet, "SheetJS");
        XLSX.writeFile(new_workbook, "BookingSummaryByUser.xlsx");
    }

    render() {
        let {reportDetails} = this.props
        const columns = [{
            title: 'User Email',
            dataIndex: 'userEmail',
            key: 'userEmail',
        },{
            title: 'Booking Count',
            dataIndex: 'bookingCount',
            key: 'bookingCount'
        }, {
            title: 'User Name',
            dataIndex: 'userName',
            key: 'userName',
        }, {
            title: 'User Id',
            key: 'userId',
            dataIndex: 'userId'
        }, {
            title: 'User Mobile',
            key: 'userMobile',
            dataIndex: 'userMobile'
        }];

        return (
            <Fragment>
                <Row>
                    <Col md={{ span: 4, offset: 20 }} style={{textAlign : "right", marginBottom : '5px'}}>
                        <Button type="primary" onClick={this.handleOnDownloadOnClick}><Icon type='file-excel'></Icon> Download</Button>
                    </Col>
                </Row>
                <Table id={'bookingsummaryByUser'} rowKey='userId' bordered style={{background: '#fff', height: '300px', overflowY: 'scroll'}} pagination={false} loading={this.props.fetching} columns={columns} dataSource={reportDetails}/>
            </Fragment>
        );
    }
}

const mapStateToProps = ({report}) => {
    const bookingSummaryUser = report && report.bookingSummaryUser ? report.bookingSummaryUser : [] 
     
    return {
        fetching :  report.fetching,
        reportDetails : bookingSummaryUser
    }
};

const mapDispatchToProps = dispatch => ({
    bookingSummaryByUserActionCreator : (path) => dispatch(bookingSummaryByUserActionCreator(path))
});

export default connect(mapStateToProps, mapDispatchToProps)(BookingSummaryUserReport);