import React, { Component, Fragment } from 'react';
import { Table, Button, Row, Col, Icon, DatePicker} from 'antd';
import { connect} from 'react-redux';
import {getUserReportActionCreator} from '../../../../redux/actions/reportsAction';
import XLSX from 'xlsx';
import ReportHelper from '../../../../helper/ReportHelper';
import moment from 'moment';

class UserReport extends Component {

    state = {
        startOfWeek  : null,
        endOfWeek    : null,
        reportDetails: []
    }

    componentDidMount() {
        var startOfWeek = moment(moment().format('YYYY-MM-DD')).startOf('isoWeek').format('YYYY-MM-DD');
        var endOfWeek = moment(moment().format('YYYY-MM-DD')).endOf('isoWeek').format('YYYY-MM-DD');

        this.props.getUserReportActionCreator('user_summary_report', startOfWeek, endOfWeek);

        this.setState({
            startOfWeek,
            endOfWeek
        });
    }

    componentWillReceiveProps(nextProps) {
        let {startOfWeek, endOfWeek} = this.state;
        let {reportDetails} = nextProps;

        let getUserDetailsForWeek = ReportHelper.getFromToDateList(startOfWeek, endOfWeek, reportDetails);
        this.setState({reportDetails : getUserDetailsForWeek});
    }
    
    
    handleDateOnChange = async (date, dateString) => {
        var startOfWeek = moment(moment(dateString).format('YYYY-MM-DD')).startOf('isoWeek').format('YYYY-MM-DD');
        var endOfWeek = moment(moment(dateString).format('YYYY-MM-DD')).endOf('isoWeek').format('YYYY-MM-DD');
        
        await this.setState({
            startOfWeek,
            endOfWeek
        });

        this.props.getUserReportActionCreator('user_summary_report', startOfWeek, endOfWeek);
    }
    handleOnDownloadOnClick = () => {
        const {reportDetails} = this.state
        const spaceCharactoList = ReportHelper.getCharatorsCount(reportDetails);
        let wscols = [
            {wch:spaceCharactoList.date},
            {wch:'Visitor Count'.length > spaceCharactoList.total ? 'Visitor Count'.length : spaceCharactoList.total},
            {wch:'Traditional Login Count'.length > spaceCharactoList.user ? 'Traditional Login Count'.length : spaceCharactoList.user},
            {wch:'Guest Login Count'.length > spaceCharactoList.guest ? 'Guest Login Count'.length : spaceCharactoList.guest}
        ];

        let table = document.getElementById('spaceTable')
        let worksheet = XLSX.utils.table_to_sheet(table);
        worksheet['!cols'] = wscols;
        let new_workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(new_workbook, worksheet, "SheetJS");
        XLSX.writeFile(new_workbook, "Users.xlsx");
    }

    disabledDate = (current) => {
        return current && current > moment().endOf('day');
    }

    render() {
        let {reportDetails} = this.state
        const columns = [{
            title: 'Date',
            dataIndex: 'date',
            key: 'date',
        }, {
            title: 'Visitor Count',
            dataIndex: 'total',
            key: 'total',
        }, {
            title: 'Traditional Login Count',
            key: 'user',
            dataIndex: 'user'
        },{
            title: 'Guest Login Count',
            dataIndex: 'guest',
            key: 'guest'
        }];

        return (
            <Fragment>
                <Row>
                    <Col md={{ span: 24 }}>
                        <Col md={{ span: 8}} style= {{textAlign:'left'}}>
                            <DatePicker 
                                onChange={this.handleDateOnChange}
                                disabledDate={this.disabledDate}
                             />
                        </Col>
                        <Col md={{ span: 4, offset: 12}} style={{textAlign : "right", marginBottom : '5px'}}>
                            <Button type="primary" onClick={this.handleOnDownloadOnClick}><Icon type='file-excel'></Icon> Download</Button>
                        </Col>

                    </Col>
                    
                </Row>
                <Table id={'spaceTable'} rowKey='date' bordered style={{background: '#fff', height: '300px', overflowY: 'scroll'}} pagination={false} loading={this.props.fetching} columns={columns} dataSource={reportDetails}/>
            </Fragment>
        );
    }
}

const mapStateToProps = ({report}) => {
    const user = report && report.user ? report.user : [] 
     
    return {
        fetching :  report.fetching,
        reportDetails : user
    }
};

const mapDispatchToProps = dispatch => ({
    getUserReportActionCreator : (path, startOfWeek, endOfWeek) => dispatch(getUserReportActionCreator(path, startOfWeek, endOfWeek))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserReport);