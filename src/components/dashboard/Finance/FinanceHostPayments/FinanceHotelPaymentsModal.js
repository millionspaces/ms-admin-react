import React, { Component } from 'react';

class FinanceHotelPaymentsModal extends Component {
    render() {
        return (
            <div>
                
            </div>
        );
    }
}

export default FinanceHotelPaymentsModal;

import React, { Component } from 'react';
import {Modal, Button} from 'antd';
import { DATE_FORMAT } from '../../../settings';
import moment from 'moment';

class ManagedBookingsDetails extends Component {

    constructor(props) {
        super(props);

        this.state = {
            visible :  false
        }
    }

    componentWillReceiveProps(nextProps) {
       this.setState((state, props) => { return { visible : nextProps.isShowManageBookingDetails !== state.visible  && nextProps.isShowManageBookingDetails}});
    }
    
    handleOk = (e) => {
        this.setState({
          visible: false,
        });
        this.props.hideManageBookingDetails();
    }
    
    handleCancel = (e) => {
        this.setState({
          visible: false,
        });
        this.props.hideManageBookingDetails();
    }

    render() {
        let {visible}            = this.state;
        let {manageBookings}      = this.props;
        return (
            <Modal
                title={`Managed Boooking ${manageBookings ? manageBookings.orderId : null}`}
                visible={visible}
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                    <Button key="submit" type="primary" onClick={this.handleOk}>
                      Ok
                    </Button>,
                ]}
                >
                <p>Booking Date: {manageBookings ? moment(manageBookings.bookedDate).format(DATE_FORMAT) : null}</p>
                <p>Organization: {manageBookings ? manageBookings.organizationName : null}</p>
                <p>Event Date: {manageBookings && manageBookings.eventDate ? moment(manageBookings.eventDate).format(DATE_FORMAT) : null}</p>
                <p>Guest Name: {manageBookings ? manageBookings.guestName : null}</p>
                <p>Guest Email: {manageBookings ? manageBookings.guestEmail : null}</p>
                <p>Mobile: {manageBookings ? manageBookings.guestMobile : null}</p>
                <p>Total: {manageBookings ? manageBookings.total : null}</p>
                <p>Host Name: {manageBookings ? manageBookings.hostName : null}</p>
                <p>Host Email: {manageBookings ? manageBookings.hostEmail : null}</p>
                <p>Host Charge: {manageBookings ? manageBookings.hostCharge : null}</p>
                <p>Reservatation Status: {manageBookings ? manageBookings.reservatationStatus : null}</p>
                <p>Reservatation Status Name: {manageBookings ? manageBookings.reservatationStatusName : null}</p>
                <p>Space Id: {manageBookings ? manageBookings.spaceId : null}</p>
            </Modal>
        );
    }
}

export default ManagedBookingsDetails;