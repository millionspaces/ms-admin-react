import React, { Component } from 'react';
import { Layout, Table, Icon, Tag } from 'antd';
const { Content } = Layout;
class FinanceHostPaymentsList extends Component {

    state = {
        isShowModal : false
    }
    onSelectChange = (hostPayment) => {
        
    }

    render() {
        
        const columns = [{
            title: 'Booking ID',
            dataIndex: 'id',
            key: 'id',
        }, {
            title: 'Organization Name',
            dataIndex: 'organizationName',
            key: 'organizationName'
        }, {
            title: 'Bank',
            dataIndex: 'bank',
            key: 'bank'
        }, {
            title: 'Branch',
            dataIndex: 'branch',
            key: 'branch'
        },{
            title: 'Account Number',
            dataIndex: 'accountNumber',
            key: 'accountNumber'
        }, {
            title: 'Account HolderName',
            dataIndex: 'accountHolderName',
            key: 'accountHolderName'
        }, {
            title: 'Commission %',
            dataIndex: 'commissionPercentage',
            key: 'commissionPercentage'
        }, {
            title: 'Confirm payment to host',
            dataIndex: 'pdf',
            key: 'pdf',
            render: (text, hostPayment, index) => { 
                return <Tag key={index} color="volcano" onClick={(e) => {this.onSelectChange(hostPayment)}}><Icon type="file-pdf" /> PDF</Tag> 
            }
        }, {
            title: 'Notes',
            dataIndex: 'notes',
            key: 'notes'
        }];
        return (
            <Content style={{ background: '#fff' }}>
                    <Table  bordered pagination={false} columns={columns} dataSource={this.props.hostPayments} rowKey='id'/>
            </Content>
        );
    }
}

export default FinanceHostPaymentsList;