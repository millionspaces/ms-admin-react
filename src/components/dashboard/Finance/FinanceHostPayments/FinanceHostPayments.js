import React, { Component } from 'react';
import FinanceHostPaymentsList from './FinanceHostPaymentsList';
import { Layout, Col, Form,  Row, Input, Button, Pagination } from 'antd';
import { setCurrentScreen } from '../../../../redux/actions/screensActions';
import { connect } from 'react-redux';
import {getFinanceHostPayments} from '../../../../redux/actions/financeActions';

const { Content } = Layout;
const FormItem = Form.Item;

class FinanceHostPayments extends Component {

    state = {
        currentPage : 1
    }

    componentDidMount() {
        this.props.getFinanceHostPayments(this.state.currentPage-1);
        this.props.setScreen('Finance Host Payment');
    }
    
    onPaginationChange = (pageNumber) => {
        this.props.getFinanceHostPayments(pageNumber-1);

        this.setState({
            currentPage: pageNumber
        });
    }

    render() {
        let {totalPages, financeHostPayments} = this.props;;
        const { getFieldDecorator } = this.props.form;

        return (
            <Layout style={{ margin: 10 }}>
                   <Content style={{ flex: 'initial', textAlign: 'right', marginTop: 20, marginBottom: 20 }}>
                        <Row>
                            <Col span={8} style={{textAlign : 'left'}}>
                                    <Form layout="inline" onSubmit={this.handleSubmit}>
                                            <FormItem>
                                                {getFieldDecorator('bookingId')(<Input placeholder="Booking id" />)}
                                            </FormItem>
                                            <FormItem>
                                            <Button style={{ height: '29px'}}type="primary" htmlType="submit">Search</Button>
                                        </FormItem>
                                    </Form>
                            </Col> 
                            {financeHostPayments && financeHostPayments.length > 0?
                            <Col span={16}> 
                                Showing {10} of {totalPages} host payments <Pagination style={{marginLeft: 20, display: 'inline-block', verticalAlign: 'middle'}} showQuickJumper defaultCurrent={this.state.currentPage} total={totalPages} onChange={this.onPaginationChange}/>
                            </Col>:null}
                        </Row> 
                    </Content>

                <FinanceHostPaymentsList 
                    hostPayments={this.props.financeHostPayments}/>
            </Layout>
        );
    }
}

FinanceHostPayments =  Form.create()(FinanceHostPayments);


const mapStateToProps = ({finance}) => {

    return {
      financeHostPayments       : finance.financeHostPayment ? finance.financeHostPayment.bookings : [],
      totalPages                : finance.financeHostPayment ? finance.financeHostPayment.count : 1,
    }
};

const mapDispatchToProps = dispatch => ({
    setScreen                   : screen => dispatch(setCurrentScreen(screen)),
    getFinanceHostPayments      : page => dispatch(getFinanceHostPayments(page))
});

export default connect(mapStateToProps, mapDispatchToProps)(FinanceHostPayments)
