import React, { Component } from 'react';
import PropTypes, { instanceOf } from 'prop-types';
import './App.css';

// Custom components
import LoginModal from './login/LoginModal';
import {userDeviceLogoutActionCreator} from '../redux/actions/authenticationActions';

// Other
import {Cookies, withCookies } from 'react-cookie';
import { compose } from 'recompose';
import IdleTimer from 'react-idle-timer'

// Redux
import { connect } from 'react-redux';
import Main from './dashboard/Main';

/**
 * Class Representing App
 */
class App extends Component {

	static propTypes = {
		cookies: instanceOf(Cookies).isRequired,
		id: PropTypes.number,
		role: PropTypes.string
	}

	state = {
		loggedIn: false,
		fetching: false
	};

	componentDidMount () {
		
		const { cookies } = this.props;

		cookies.get('MS_ADMIN_USER')?this.setState({loggedIn: true}):this.setState({loggedIn: false})

	}

	componentWillReceiveProps (nextProps) {

		const { id, fetching, role, cookies, logged } = nextProps;
	
		if (fetching) {
			this.setState({
				fetching
			});
		}
		if (logged && id && role === 'ADMIN') {
			cookies.set('MS_ADMIN_USER', id, {path:"/"});
			this.setState({loggedIn: true});
		}

		if (!logged) {
			cookies.remove('MS_ADMIN_USER', {path: '/'});
			this.setState({ loggedIn: false, fetching });
		}

	}

	onIdle = (e) => {
		const { fetching, cookies } = this.props;

		this.props.logout();
		cookies.remove('MS_ADMIN_USER', {path: '/'});
		this.setState({ loggedIn: false, fetching });
	}

	render () {

		let { loggedIn, fetching } = this.state;

		return (
			<div className="main-wrapper">
			  	<IdleTimer
					ref={ref => { this.idleTimer = ref }}
					element={document}
					onIdle={this.onIdle}
					debounce={250}
					timeout={1000 * 60 * 10} 
				/>
				{process.env=="development"?<div>Development</div>:null}
				{loggedIn?<Main />:<LoginModal fetching={fetching} loggedIn={loggedIn} />}
			</div>
		);
	}
}

/**
 * Maps Redux store to component props
 */
const mapStateToProps = ({auth}) => {

	const {id, role, fetching, name, logged} = auth;

	return {
		id,
		role,
		fetching,
		logged,
		name
	}
}

const mapDispatchToProps = dispatch => {
	return {
		logout : () => dispatch(userDeviceLogoutActionCreator())
	}
}

App = compose(
	connect(mapStateToProps, mapDispatchToProps)
) (App);

export default withCookies (App);
