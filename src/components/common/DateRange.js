import React from 'react';
import { DatePicker } from 'antd';
import moment from 'moment';

class DateRange extends React.Component {
  state = {
    startValue: moment().add(-1,'days'),
    endValue: moment(),
    endOpen: false,
  };

  disabledStartDate = (startValue) => {
    const endValue = this.state.endValue;
    if (!startValue || !endValue) {
      return false;
    }
    if(startValue.valueOf() > endValue.valueOf()){
       return true;
    }else if(startValue.valueOf() <  moment(endValue, "DD-MM-YYYY").add(-25, 'days').valueOf()){
      return true;
    }else{
      return false
    }
  }

  disabledEndDate = (endValue) => {
    const startValue = this.state.startValue;
    if (!endValue || !startValue) {
      return false;
    }
    if(endValue.valueOf() <= startValue.valueOf()){
      return true;
    }else if(endValue.valueOf() >= moment(startValue, "DD-MM-YYYY").add(25, 'days').valueOf()){
      return true;
    }else {
      return false;
    }
  }

  onChange = (field, value) => {
    this.setState({
      [field]: value,
    });
  }

  onStartChange = (value) => {
    this.onChange('startValue', value);
    this.props.handleStartDate(value)
  }

  onEndChange = (value) => {
    this.onChange('endValue', value);
    this.props.handleEndDate(value);
  }

  handleStartOpenChange = (open) => {
    if (!open) {
      this.setState({ endOpen: true });
    }
  }

  handleEndOpenChange = (open) => {
    this.setState({ endOpen: open });
  }

  render() {
    const dateFormat = 'YYYY-MM-DD';
    const { startValue, endValue, endOpen } = this.state;
    return (
      <div>
        <DatePicker
          disabledDate={this.disabledStartDate}
          value={startValue}
          format="YYYY-MM-DD"
          placeholder="Start"
          onChange={this.onStartChange}
          onOpenChange={this.handleStartOpenChange}
          style={{marginRight : '10px'}}
        />
        <DatePicker
          disabledDate={this.disabledEndDate}
          format="YYYY-MM-DD"
          value={endValue}
          placeholder="End"
          onChange={this.onEndChange}
          open={endOpen}
          onOpenChange={this.handleEndOpenChange}
        />
      </div>
    );
  }
}

export default DateRange;