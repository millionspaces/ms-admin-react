import React from 'react';
import PropTypes from 'prop-types';
import { Map, Marker } from 'google-maps-react';
import PlacesAutoComplete, { geocodeByPlaceId, getLatLng } from 'react-places-autocomplete';

/**
 * Class Containing Google Maps.
 */
class GMapContainer extends React.Component {

    static propTypes = {
        latitude: PropTypes.string.isRequired,
        longitude: PropTypes.string.isRequired
    }

    state = {
        addressLine1: ''
    }


    /*************************** Life cycle methods /************************/

    componentWillReceiveProps (nextProps) {
        this.setState({
            latitude: nextProps.latitude,
            longitude: nextProps.longitude
        });
    }

    componentDidMount () {
        this.setState({
            latitude: this.props.latitude,
            longitude: this.props.longitude
        });
    }


    /*************************** Custom Method ***************************/

    /**
     * Triggeres with auto complete onChange
     * @param {String} address Returned address string.
     */
    onChange = (address) => {
        this.setState({ addressLine1: address });
    }

    /**
     * Triggers with auto complete onSelect
     * @param {String} addressLine1
     * @param {String} placeId
     */
    handleSelect = (addressLine1, placeId) => {

        this.setState({ addressLine1, placeId });

        // get the geo code by google place id and set lat and lng to state.
        geocodeByPlaceId(placeId)

            .then(results => getLatLng(results[0]))
            .then(({ lat, lng }) => {
                this.props.getLocation({latitude: lat, longitude: lng})
                this.setState({
                    latitude: lat,
                    longitude: lng
                });
            });
        
    }



    render () {
        
        let { latitude, longitude, addressLine1 } = this.state;

        const inputProps = {
            value: this.state.addressLine1,
            onChange: this.onChange,
            placeholder: 'Select Location on Map'

        }

        return (
            <div style={{height: 300, marginTop: 30}}>

                {/* Field description */}
                <p style={{ display: 'block', position: 'relative', top: '-35px'}}><span style={{ display: 'inline-block', marginRight: '4px', content: "*", fontFamily: 'SimSun', lineHeight: 1, fontSize: 14, color: '#f5222d'}}>* </span>Search on map, and select the location</p>
                
                {/* Google map integration */}
                <Map google={window.google} zoom={14} center={{
                    lat: latitude,
                    lng: longitude
                }} style={{background: '#fff'}} onReady={this.fetchPlaces}>

                    {/* Map marker */}
                    <Marker
                        position={{lat: latitude?latitude:this.props.latitude, lng: longitude?longitude:this.props.longitude}} />

                        {/* Google place search autocomplete. */}
                        <PlacesAutoComplete
                            value={addressLine1}
                            inputProps={inputProps}
                            onSelect={this.handleSelect}
                            placeholder="Serch"
                            styles={{
                                input: {
                                    border: '1px solid #e2e2e2',
                                    outline: 'none',
                                    height: 35,
                                    borderRadius: 4,
                                    margin: 10,
                                    width: '45.5%',
                                    position: 'absolute',
                                    top: '-60px',
                                    left: '-10px'
                                }
                            }}
                        />
                        
                </Map>
            </div>
        )
    }
}

export default GMapContainer;