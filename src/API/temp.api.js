import { API_SETTINGS } from './CONSTS';

const { API } = API_SETTINGS;

export const fetchSpaceUser = spaceId => {
    return fetch(`${API}admin/space/host/${spaceId}`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        }
    });
};

export const manuallyVerifyUserEmail = email => {

    return fetch(`${API}admin/user/verify`, {
        method: 'POST',
        credentials: 'include',
        headers: { "content-type": "application/json" },
        body: JSON.stringify({ email })
    });
}

export const changeHost = user => {

    return fetch(`${API}admin/space/changeHost`, {
        method: 'POST',
        credentials: 'include',
        headers: { "content-type": "application/json" },
        body: JSON.stringify(user)
    });

}


