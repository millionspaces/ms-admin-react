import { API_SETTINGS } from './CONSTS';
const { API } = API_SETTINGS;

/**
 * API Call: Get getBannerDetails
 */

export const getBannerDetails = (page) => {
    return fetch(`${API}common/procedure/banner_details`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        }
    }).then(res=>res.json());
};

export const postBannerDetails = (banners) => {
    return fetch (`${API}admin/space/banners`, {
        method: 'POST',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify({
            banners
        })
    });
}