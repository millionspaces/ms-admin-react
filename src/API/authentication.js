import CryptoJS from 'crypto-js';
import { API_SETTINGS } from './CONSTS';

const { API } = API_SETTINGS;

/**
 * Authentication service call
 * @param {Object} Describes the user
 * @returns {Promise}
 */
export const loginUserDevice = user => {
    return fetch(
        `${API}grant_eventspace_security`, {
            method: 'POST',
            credentials: 'include',
            headers: {
                "content-type": "application/x-www-form-urlencoded"
            },
            body: new URLSearchParams(`username=${user.userName}&password=${CryptoJS.SHA256(user.password).toString()}`)  
        }
    )
};

/**
 * Loggot user device
 * @returns {Promise}
 */
export const userDeviceLogout = () => {
    return fetch (
        `${API}device_logout`, {
            method: 'GET',
            credentials: 'include',
            headers: {
                "content-type": "application/x-www-form-urlencoded"
            }
        }
    );
}