import { API_SETTINGS } from './CONSTS';
const axios = require('axios');

const { API } = API_SETTINGS;

export const getUsersStatistics = (startDate, endDate) => {
    return axios.get(`${API}common/reports?procedureName=${'user_summary_report'}('${startDate}','${endDate}')`).then((res) =>{
        return res;
    }).catch(error => 
        error);
};

export const getMsDashboard = () => {
    return axios.get(`https://api.millionspaces.com/api/common/reports?procedureName=getMsDashboard`).then((res) =>{
        return res;
    }).catch(error => 
        error);
};


export const getBookingDsitributionDetails = () => {
    return axios.get(`https://api.millionspaces.com/api/common/reports?procedureName=getMsSpacedistribution`).then((res) => {
        return res;
    }).catch(error => error)
}

export const getCommissionDetails = () => {
    return axios.get(`https://api.millionspaces.com/api/common/reports?procedureName=getMsCommission`).then((res) => {
        return res;
    }).catch(error => error)
}