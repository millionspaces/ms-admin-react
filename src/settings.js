const { REACT_APP_MS_ENV }          = process.env;
const imageCloudNameLive            = 'dgcojyezg';
const imageCloudNameQa              = 'dz4u73trs';
const imageCloudPresetQa            = 'xgnvzwig';
const imageCloudPresetLive          = 'hglnuurt';
const pdfTransactionsCloudNameLive  = 'dgcojyezg';
const pdfTransactionsCloudNameQa    = 'dz4u73trs';
const PDF__TRANSACTIONS_PRESET      = 'pdf_transaction_preset';

const constructApi                  = env => `https://${env}api.millionspaces.com/api/`;

let IMAGE_CLOUD_NAME                = imageCloudNameQa;
let IMAGE_CLOUD_PRESET              = imageCloudPresetQa;
let PDF__TRANSACTIONS_CLOUDE_NAME   = pdfTransactionsCloudNameQa;   

let api                             = constructApi(REACT_APP_MS_ENV==='qae'?'q':'');

if (REACT_APP_MS_ENV === "qae"){  

    console.log(constructApi(REACT_APP_MS_ENV==='qae'?'q':''))

    IMAGE_CLOUD_NAME                = imageCloudNameQa;
    IMAGE_CLOUD_PRESET              = imageCloudPresetQa;
    PDF__TRANSACTIONS_CLOUDE_NAME   = pdfTransactionsCloudNameQa;

} else if (REACT_APP_MS_ENV === "live"){ 
    
    IMAGE_CLOUD_NAME                = imageCloudNameLive;
    IMAGE_CLOUD_PRESET              = imageCloudPresetLive
    PDF__TRANSACTIONS_CLOUDE_NAME   = pdfTransactionsCloudNameLive

}

export {api, IMAGE_CLOUD_NAME, IMAGE_CLOUD_PRESET, PDF__TRANSACTIONS_PRESET, PDF__TRANSACTIONS_CLOUDE_NAME};

// Dates
export const TIME_STRING_FORMAT = 'hh:mm A';
export const DATE_FORMAT = 'DD MMM YY';

// Messages
export const COPY_PASTE_WARNING_BODY = 'Please think twice before copy pasting. You are going to make an innocent dev soul suffer for being a dev soul. The string you paste may contain hidden characters which could cause bugs. Dont be lazy enter them manually. Its still faster than you could manually do it'
export const COPY_PASTE_WARNING_HEADING = 'COPY PASTE DETECTED! 😠😠😠😠'