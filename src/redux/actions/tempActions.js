import { TEMP_VERIFYING_CUSTOM_EMAIL, TEMP_GET_USER_DETAILS, TEMP_GET_USER_DETAILS_SUCCESS, TEMP_GET_USER_DETAILS_FAILED, TEMP_VERIFYING_CUSTOM_EMAIL_SUCCESS, TEMP_VERIFYING_CUSTOM_EMAIL_FAILED, TEMP_RESET_MANUAL_EMAIL_VEFIF_STATE } from "../actionTypes/tempActionTypes";
import { fetchSpaceUser, manuallyVerifyUserEmail, changeHost } from "../../API/temp.api";


export const verifyCustomEmail = email => {
    return dispatch => {

        dispatch({
            type: TEMP_VERIFYING_CUSTOM_EMAIL
        });

        manuallyVerifyUserEmail(email).then(res => {

            if (res.status === 200) {
                dispatch({
                    type: TEMP_VERIFYING_CUSTOM_EMAIL_SUCCESS
                });
                
                dispatch(resetManualEmailVerif());

            } else {
                dispatch({
                    type: TEMP_VERIFYING_CUSTOM_EMAIL_FAILED
                });
                
                dispatch(resetManualEmailVerif());
            }
        }).catch(err => {
            dispatch({
                type: TEMP_VERIFYING_CUSTOM_EMAIL_FAILED
            });
            
            dispatch(resetManualEmailVerif());
        });
        
    }
}

export const getSpaceUserDetails = (spaceId) => {
    
    return dispatch => {

        dispatch({
            type: TEMP_GET_USER_DETAILS
        });

        fetchSpaceUser(spaceId)            
            .then(res => {

                if (res.status == 200) {
                    return res.json()
                } else {
                    dispatch({
                        type: TEMP_GET_USER_DETAILS_FAILED,
                        payload: 'An error has occured'
                    });
                }
                
            }).then(data => {
                dispatch({
                    type: TEMP_GET_USER_DETAILS_SUCCESS,
                    payload: data
                });
            })
            .catch(err => {
                dispatch({
                    type: TEMP_GET_USER_DETAILS_FAILED,
                    payload: err
                });
            });
    }
}

export const changeUser = userData => {
    return dispatch => {

        dispatch({
            type: 'CHANGING_USER'
        });

        changeHost(userData)
            .then(res => {
                if (res.status === 200) {
                    dispatch({
                        type: 'CHANGING_USER_SUCCESS'
                    });
                } else {
                    dispatch({
                        type: 'CHANGING_USER_FAILED'
                    });
                }
                dispatch(getSpaceUserDetails(userData.space));
                dispatch(resetChangeUserDetails())
            })
            .catch(err => {
                dispatch({
                    type: 'CHANGING_USER_FAILED'
                });
                dispatch(getSpaceUserDetails(userData.space));
                dispatch(resetChangeUserDetails())
            })
            
    }
}

export const resetManualEmailVerif = () => ({
    type: TEMP_RESET_MANUAL_EMAIL_VEFIF_STATE
});

export const resetChangeUserDetails = () => ({
    type: 'RESET_USER_CHANGE'
})