
import { 
    SPACE_STATISTICS_FETCHING, 
    SPACE_STATISTICS_FETCH_SUCCESS, 
    SPACE_STATISTICS_FETCH_FAIL, 
    GET_SPACE_DASHBOARD_FETCHING, 
    GET_SPACE_DASHBOARD_SUCCESS, 
    GET_SPACE_DASHBOARD_FAIL,

    BOOKING_DISTRIBUTION_PIE_CHART_FETCHING,
    BOOKING_DISTRIBUTION_PIE_CHART_FETCH_SUCCESS,
    BOOKING_DISTRIBUTION_PIE_CHART_FETCH_FAIL,
    COMMISION_DETAILS_FETCHING,
    COMMISION_DETAILS_FETCHING_SUCCESS,
    COMMISION_DETAILS_FETCHING_FAIL

} from '../actionTypes/statisticsActionTypes';
import {getUsersStatistics, getMsDashboard, getBookingDsitributionDetails, getCommissionDetails} from '../../API/statisticsApi';

export const getUserStatistics = (startDate, endDate) => {
    return dispatch => {
        dispatch({
            type : SPACE_STATISTICS_FETCHING
        });

        getUsersStatistics(startDate, endDate)
        .then(res => {
            dispatch({
                type    : SPACE_STATISTICS_FETCH_SUCCESS,
                payload : {user : res.data}
            })
        })
        .catch(error => {
            dispatch({
                type    : SPACE_STATISTICS_FETCH_FAIL,
                payload : error
            })

        });
    }
}

export const getDashboardDetails = () => {
    return dispatch => {
        dispatch({
            type : GET_SPACE_DASHBOARD_FETCHING
        });

        getMsDashboard()
        .then(res => {
            dispatch({
                type    : GET_SPACE_DASHBOARD_SUCCESS,
                payload : res.data
            })
        })
        .catch(error => {
            dispatch({
                type    : GET_SPACE_DASHBOARD_FAIL,
                payload : error
            })

        });
    }
}

export const bookingDistributionPieChart = () => {
    return dispatch => {
        dispatch({
            type : BOOKING_DISTRIBUTION_PIE_CHART_FETCHING
        });
        getBookingDsitributionDetails()
        .then(res => {
            dispatch({
                type    : BOOKING_DISTRIBUTION_PIE_CHART_FETCH_SUCCESS,
                payload : res.data
            }) 
        })
        .catch(error => {
            dispatch({
                type    : BOOKING_DISTRIBUTION_PIE_CHART_FETCH_FAIL,
                payload : error
                })
    
            });
       
    }
}

export const commitionDetailsActionCreator = () => {
    return dispatch => {
        dispatch({
            type : COMMISION_DETAILS_FETCHING
        });
        getCommissionDetails()
        .then(res => {
            dispatch({
                type    : COMMISION_DETAILS_FETCHING_SUCCESS,
                payload : res.data
            }) 
        })
        .catch(error => {
            dispatch({
                type    : COMMISION_DETAILS_FETCHING_FAIL,
                payload : error
                })
            });      
    }
}
