import {getBannerDetails} from '../../API/banner';
import {BANNERS_FETCHING,
        BANNERS_FETCHING_SUCESS, 
        BANNERS_FETCHING_FAILS,
        BANNERS_POST,
        BANNERS_POST_SUCCESS} from '../actionTypes/bannerActionTypes'

export const getBannsers = () => {
    return dispatch => {

        dispatch({
            type : BANNERS_FETCHING,
        });

        getBannerDetails()
            .then(res => {
                dispatch({
                    type    : BANNERS_FETCHING_SUCESS,
                    payload : res
                });
            })
            .catch(error => {
                dispatch({
                    type    : BANNERS_FETCHING_FAILS,
                    payload : error
                });
            });
    }
};

export const postBannsers = () => {
    return dispatch => {

        dispatch({
            type : BANNERS_POST,
        });

        getBannerDetails()
            .then(res => {
                dispatch({
                    type    : BANNERS_POST_SUCCESS,
                });
            })
            .catch(error => {
                dispatch({
                    type    : BANNERS_FETCHING_FAILS,
                    payload : error
                });
            });
    }
};