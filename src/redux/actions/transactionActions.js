import { 
    TRANSACTIONS__FETCHING_TRANSACTIONS,
    TRANSACTIONS__FETCHING_TRANSACTIONS_SUCCESS,
    TRANSACTIONS__FETCHING_TRANSACTIONS_FAILED,
    TRANSACTIONS__FLAGGING_TRANSACTION_AS_RECEIVED,
    TRANSACTIONS__FLAGGING_TRANSACTION_AS_RECEIVED_SUCCESS,
    TRANSACTIONS__FLAGGING_TRANSACTION_AS_RECEIVED_FAILED
} from '../actionTypes/transactionActionTypes';

import { fetchTransactionDetails, markTransactionReceived, fetchBybookingId } from '../../API/transactions';

export const getTransactionDetails = (pageId) => {

    return dispatch => {

        dispatch ({
            type: TRANSACTIONS__FETCHING_TRANSACTIONS
        });

        fetchTransactionDetails (pageId)
            .then( res =>  res.json())
            .then (data => {
                dispatch ({
                    type: TRANSACTIONS__FETCHING_TRANSACTIONS_SUCCESS,
                    payload: data
                })
            })
            .catch (err => {
                dispatch({
                    type: TRANSACTIONS__FETCHING_TRANSACTIONS_FAILED,
                    payload: err
                })
            });
    }
}

export const getTransactionByBookingId = (bookingId) => {
    return dispatch => {
        dispatch ({
            type: TRANSACTIONS__FETCHING_TRANSACTIONS
        });

        fetchBybookingId(bookingId)
        .then( res => {
            if(res.code && res.code === 500){
                dispatch ({
                    type: TRANSACTIONS__FETCHING_TRANSACTIONS_SUCCESS,
                    payload: {bookings : [] }
                }) 
            }else{
                dispatch ({
                    type: TRANSACTIONS__FETCHING_TRANSACTIONS_SUCCESS,
                    payload: {bookings : [res] }
                })
            }
        })
        .catch (err => {
            dispatch({
                type: TRANSACTIONS__FETCHING_TRANSACTIONS_FAILED,
                payload: err
            });
        });
    }
}

export const setTransactionProof = (pdf, pageId) => {

    return dispatch => {

        dispatch ({
            type: TRANSACTIONS__FLAGGING_TRANSACTION_AS_RECEIVED
        });

        markTransactionReceived (pdf)
            .then(res => res.json())
            .then(data => {
                dispatch ({
                    type: TRANSACTIONS__FLAGGING_TRANSACTION_AS_RECEIVED_SUCCESS,
                    payload: data
                });

                dispatch ({
                    type: TRANSACTIONS__FETCHING_TRANSACTIONS
                });
        
                fetchTransactionDetails (pageId)
                    .then( res =>  res.json())
                    .then (data => {
                        dispatch ({
                            type: TRANSACTIONS__FETCHING_TRANSACTIONS_SUCCESS,
                            payload: data
                        })
                    })
                    .catch (err => {
                        dispatch({
                            type: TRANSACTIONS__FETCHING_TRANSACTIONS_FAILED,
                            payload: err
                        })
                    });
            })
            .catch (err => {
                dispatch ({
                    type: TRANSACTIONS__FLAGGING_TRANSACTION_AS_RECEIVED_FAILED,
                    payload: err
                });
            });
    }
}
