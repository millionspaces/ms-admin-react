import {
        FETCHING_REPORTS, 
        FETCHING_BOOKINGSUMMARY_REPORT_SUCCESS,
        FETCHING_STATUS_REPORT_SUCCESS,
        FETCHING_BOOKINGSUMMARY_BY_SPACE_REPORT_SUCCESS, 
        FETCHING_REPORTS_FAILED, 
        FETCHING_BOOKINGSUMMARY_BY_USER_REPORT_SUCCESS,
        FETCHING_BOOKING_REPORT_SUCCESS,
        FETCHING_USER_REPORT_SUCCESS
    } from '../actionTypes/reportsActionTypes';

export const reportReducer = (state={fetching: false, error: {status: false, message:""}}, action) => {

    switch (action.type) {

        case FETCHING_REPORTS:
            return Object.assign({}, state, {fetching: true});

        case FETCHING_BOOKINGSUMMARY_REPORT_SUCCESS:
            return Object.assign({}, {
                ...state, bookingSummary: action.payload, bookingSummaryStatus:[], bookingSummaryUser: [], spaces: [], bookings : [], user : [],
                fetching : false });

        case FETCHING_STATUS_REPORT_SUCCESS:
            return Object.assign({}, {
                ...state, spaces: action.payload, bookingSummaryStatus: [],  bookingSummary: [], bookingSummaryUser: [], bookings : [], user : [],
                fetching : false 
            });

        case FETCHING_BOOKINGSUMMARY_BY_SPACE_REPORT_SUCCESS:
            return Object.assign({}, {
                ...state, bookingSummaryStatus: action.payload, bookingSummary: [], bookingSummaryUser: [], spaces: [], bookings : [], user : [],
                fetching : false
            });

        case FETCHING_BOOKINGSUMMARY_BY_USER_REPORT_SUCCESS:
            return Object.assign({}, {
                ...state, bookingSummaryUser: action.payload, bookingSummaryStatus: [],  bookingSummary: [], spaces: [], bookings : [], user : [],
                fetching : false 
            });

        case FETCHING_BOOKING_REPORT_SUCCESS:
            return Object.assign({}, {
                ...state, bookings : action.payload, bookingSummaryUser : [], bookingSummaryStatus: [],  bookingSummary: [], spaces: [], user : [],
                fetching : false 
            });
        
        case FETCHING_USER_REPORT_SUCCESS:
            return Object.assign({}, {
                ...state, user : action.payload, bookings : [], bookingSummaryUser : [], bookingSummaryStatus: [],  bookingSummary: [], spaces: [],
                fetching : false 
            });
            
        case FETCHING_REPORTS_FAILED:
            return Object.assign({}, state, {fetching: false}, {error: {status: true, message: "Unable to receive spaces"}});

        default:
            return state;
    }
}

