/**
 * This file imports all the reducers and exports single reducer object
 */
import { combineReducers } from 'redux';

import { userDeviceLoginReducer } from './authentication';
import { spacesReducer, spaceToggleReducer, spaceDetailsReducer } from './spacesReducer';
import { spacesRulesReducer, seatingArrangementsReducer, activitiesReducer, amenitiesReducer, amenityUnitsReducer, spaceTypesReducer, blockChargeTypesReducer, cancellationPoliciesReducer } from './commonReducers';
import { transactionsReducer } from './transactionsReducers';
import { managedBookingsReducer, currentBookingDetailsReducer, cancellBookingsReducer } from './managedBookingsReducer';
import { hostLogoDataReducer } from './spaceImagesReducer';
import { tentetiveBookingsReducer } from './tentetiveBookingsReducers';
import { screensReducer } from './screensReducer';
import { manualEmailVerificationReducer } from './manualEmailVerificationReducer';
import { ChangeUserReducer } from './changeUserReducer';
import {creationReducer, deletionReducer, updationReducer} from './common/dbOperationReducer'
import {reportReducer} from './reportsReducer';
import {statistics} from './statisticsReducer';
import {finance} from './financeReducer';
import {banners} from './bannersReducer'

const reducers = combineReducers({
    auth: userDeviceLoginReducer,
    spaces: spacesReducer,
    spaceToggle: spaceToggleReducer,
    space: spaceDetailsReducer,
    seatingArrangements: seatingArrangementsReducer,
    activities: activitiesReducer,
    amenities: amenitiesReducer,
    amenityUnits: amenityUnitsReducer,
    spaceTypes: spaceTypesReducer,
    blockChargeTypes: blockChargeTypesReducer,
    spaceRules: spacesRulesReducer,
    cancellationPolicies: cancellationPoliciesReducer,
    transactions: transactionsReducer,
    managedBookings: managedBookingsReducer,
    currentBookingDetails: currentBookingDetailsReducer,
    hostLogoData: hostLogoDataReducer,
    tentetiveBookings: tentetiveBookingsReducer,
    screens: screensReducer,
    cancelBookings: cancellBookingsReducer,
    verifyCustomEmail: manualEmailVerificationReducer,
    userToBeChanged: ChangeUserReducer,
    creation : creationReducer,
    deletion : deletionReducer,
    updation : updationReducer,
    report : reportReducer,
    statistics: statistics,
    finance : finance,
    banners : banners
});

export default reducers;