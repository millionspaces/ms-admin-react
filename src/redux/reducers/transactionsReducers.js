import { 
    TRANSACTIONS__FETCHING_TRANSACTIONS,
    TRANSACTIONS__FETCHING_TRANSACTIONS_SUCCESS,
    TRANSACTIONS__FETCHING_TRANSACTIONS_FAILED,
    TRANSACTIONS__FLAGGING_TRANSACTION_AS_RECEIVED,
    TRANSACTIONS__FLAGGING_TRANSACTION_AS_RECEIVED_SUCCESS,
    TRANSACTIONS__FLAGGING_TRANSACTION_AS_RECEIVED_FAILED,
} from '../actionTypes/transactionActionTypes';

export const transactionsReducer = (
    state={
        fetching: false,
        error: {status: false, message: ""},
        data: []
    }, action) => {

    switch (action.type) {
        case TRANSACTIONS__FETCHING_TRANSACTIONS:
            return Object.assign({}, state, {fetching: true});

        case TRANSACTIONS__FETCHING_TRANSACTIONS_SUCCESS:
            return Object.assign({}, {fetching: false}, {data: action.payload});

        case TRANSACTIONS__FETCHING_TRANSACTIONS_FAILED:
            return Object.assign({}, state, {data: []},  {fetching: false}, {error: {status: true, message:"Failed to Fetch transaction details."}});

        case TRANSACTIONS__FLAGGING_TRANSACTION_AS_RECEIVED:
            return Object.assign({}, state, {fetching: true});

        case TRANSACTIONS__FLAGGING_TRANSACTION_AS_RECEIVED_SUCCESS:
            return Object.assign({}, state, {fetching: false});

        case TRANSACTIONS__FLAGGING_TRANSACTION_AS_RECEIVED_FAILED:
            return Object.assign({}, state, {fetching: false}, {error: {status: true, message:"Failed to flag transaction status."}});    

        default:
            return state;            
    }
}