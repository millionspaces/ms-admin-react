import { 
    SPACE_STATISTICS_FETCHING, 
    SPACE_STATISTICS_FETCH_SUCCESS, 
    SPACE_STATISTICS_FETCH_FAIL, 
    GET_SPACE_DASHBOARD_FETCHING, 
    GET_SPACE_DASHBOARD_SUCCESS, 
    GET_SPACE_DASHBOARD_FAIL,

    BOOKING_DISTRIBUTION_PIE_CHART_FETCHING,
    BOOKING_DISTRIBUTION_PIE_CHART_FETCH_SUCCESS,
    BOOKING_DISTRIBUTION_PIE_CHART_FETCH_FAIL,
    COMMISION_DETAILS_FETCHING,
    COMMISION_DETAILS_FETCHING_SUCCESS,
    COMMISION_DETAILS_FETCHING_FAIL
} from '../actionTypes/statisticsActionTypes';

export const statistics = (state={fetching: false, error: {status: false, message:""}, data: [], dashboard: []}, actions) => {
    switch(actions.type){
        case SPACE_STATISTICS_FETCHING:
            return Object.assign({}, state, {fetching: true});
        case SPACE_STATISTICS_FETCH_SUCCESS:
            return Object.assign({}, state , {fetching : false, data : actions.payload});
        case SPACE_STATISTICS_FETCH_FAIL:
            return Object.assign({}, state, {fetching: false, error:  {status : true, message : "Fail to load user statistic details"}});

        case GET_SPACE_DASHBOARD_FETCHING:
            return Object.assign({}, state, {fetching: true});
        case GET_SPACE_DASHBOARD_SUCCESS:
            return Object.assign({}, state, {fetching: false, dashboard : actions.payload});
        case GET_SPACE_DASHBOARD_FAIL:
            return Object.assign({}, state, {fetching: false, error:  {status : true, message : "Fail to load dahsboard details"}});

        case BOOKING_DISTRIBUTION_PIE_CHART_FETCHING:
            return Object.assign({}, state, {fetching: true});
        case BOOKING_DISTRIBUTION_PIE_CHART_FETCH_SUCCESS:
            return Object.assign({}, state, {fetching: false, bookingDistribution : actions.payload});
        case BOOKING_DISTRIBUTION_PIE_CHART_FETCH_FAIL:
            return Object.assign({}, state, {fetching: false, error:  {status : true, message : "Fail to load booking distribution details"}});
        
        case COMMISION_DETAILS_FETCHING:
            return Object.assign({}, state, {fetching: true});
        case COMMISION_DETAILS_FETCHING_SUCCESS:
            return Object.assign({}, state, {fetching: false, commissions : actions.payload});
        case COMMISION_DETAILS_FETCHING_FAIL:
            return Object.assign({}, state, {fetching: false, error:  {status : true, message : "Fail to load booking commissions details"}});

        default :
            return state
    }
}

