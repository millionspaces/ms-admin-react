import {FINANCE__FETCHING_HOSTPAYMENTS, FINANCE__FETCHING_HOSTPAYMENTS_FAIL, FINANCE__FETCHING_HOSTPAYMENTS_SUCCESS} from '../actionTypes/financeActionTypes';

export const finance  = (state={fetching: false, error: {status: false, message:""}}, action) => {
    switch (action.type) {

        case FINANCE__FETCHING_HOSTPAYMENTS:
            return Object.assign({}, state, {fetching: true});
        case FINANCE__FETCHING_HOSTPAYMENTS_SUCCESS:
            return Object.assign({},{ ...state, financeHostPayment: action.payload});
        case FINANCE__FETCHING_HOSTPAYMENTS_FAIL:
            return Object.assign({}, state, {fetching: false}, {error: {status: true, message: "Unable to receive spaces"}});
        default:
            return state;
    }
}