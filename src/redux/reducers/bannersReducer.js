import {BANNERS_FETCHING, 
        BANNERS_FETCHING_SUCESS, 
        BANNERS_FETCHING_FAILS,
        BANNERS_POST,
        BANNERS_POST_SUCCESS
        } from '../actionTypes/bannerActionTypes';

export const banners  = (state={fetching: false, error: {status: false, message:""}}, action) => {
    switch (action.type) {
        case BANNERS_FETCHING:
            return Object.assign({}, state, {fetching: true});
        case BANNERS_FETCHING_SUCESS:
            return Object.assign({},{ ...state, banners: action.payload});
        case BANNERS_FETCHING_FAILS:
            return Object.assign({}, state, {fetching: false}, {error: {status: true, message: "Unable to receive bannsers"}});

        case BANNERS_POST:
            return Object.assign({}, state, {fetching: true})
        case BANNERS_POST_SUCCESS:
            return Object.assign({}, state, {fetching: false})
        default:
            return state;
    }
}