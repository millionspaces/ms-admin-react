// TODO: DOC
import {
    COMMON__FETCHING_SEATING_ARRANGEMENTS,
    COMMON__FETCHING_SEATING_ARRANGEMENTS_SUCCESS,
    COMMON__FETCHING_SEATING_ARRANGEMENTS_FAILED,

    COMMON__FETCHING_ACTIVITIES,
    COMMON__FETCHING_ACTIVITIES_SUCCESS,
    COMMON__FETCHING_ACTIVITIES_FAILED,

    COMMON__FETCHING_AMENITIES,
    COMMON__FETCHING_AMENITIES_SUCCESS,
    COMMON__FETCHING_AMENITIES_FAILED,

    COMMON__FETCHING_AMENITY_UNITS,
    COMMON__FETCHING_AMENITY_UNITS_SUCCESS,
    COMMON__FETCHING_AMENITY_UNITS_FAILED,

    COMMON__FETCHING_SPACE_TYPES,
    COMMON__FETCHING_SPACE_TYPES_SUCCESS,
    COMMON__FETCHING_SPACE_TYPES_FAILED,

    COMMON__FETCHING_BLOCK_CHARGE_TYPES,
    COMMON__FETCHING_BLOCK_CHARGE_TYPES_SUCCESS,
    COMMON__FETCHING_BLOCK_CHARGE_TYPES_FAILED,

    COMMON__FETCHING_SPACE_RULES,
    COMMON__FETCHING_SPACE_RULES_SUCCESS,
    COMMON__FETCHING_SPACE_RULES_FAILED,

    COMMON__FETCHING_CANCELLATION_POLICIES,
    COMMON__FETCHING_CANCELLATION_POLICIES_SUCCESS,
    COMMON__FETCHING_CANCELLATION_POLICIES_FAILED

} from '../actionTypes/spaceCommon';

export const seatingArrangementsReducer = (state={
    fetching: false,
    error: {status: false, message:""},
    data: []
}, action) => {
    switch (action.type) {
        case COMMON__FETCHING_SEATING_ARRANGEMENTS:
            return Object.assign({}, state, {fetching: true});

        case COMMON__FETCHING_SEATING_ARRANGEMENTS_SUCCESS:
            return Object.assign({}, state, {fetching: false}, {data: action.payload});

        case COMMON__FETCHING_SEATING_ARRANGEMENTS_FAILED:
            return Object.assign({}, state, {fetching: false}, {data: []}, {error: {status: true, message:"Failed to Fetch Seating Arrangements"}});
        
        
        default:
            return state;
    }
}

export const activitiesReducer = (
    state={
        fetching: false,
        error: {status: false, message:""},
        data: []
    }, action) => {

    switch (action.type) {
        case COMMON__FETCHING_ACTIVITIES:
            return Object.assign({}, state, {fetching: true});

        case COMMON__FETCHING_ACTIVITIES_SUCCESS:
            return Object.assign({}, {fetching: false}, {data: action.payload});

        case COMMON__FETCHING_ACTIVITIES_FAILED:
            return Object.assign({}, state, {data: []},  {fetching: false}, {error: {status: true, message:"Failed to Fetch Seating Arrangements"}});
    
        default:
            return state;
    }
}

export const amenitiesReducer = (
    state={
        fetching: false,
        error: {status: false, message: ""},
        data: []
    }, action) => {

    switch (action.type) {
        case COMMON__FETCHING_AMENITIES:
            return Object.assign({}, state, {fetching: true});

        case COMMON__FETCHING_AMENITIES_SUCCESS:
            return Object.assign({}, {fetching: false}, {data: action.payload});

        case COMMON__FETCHING_AMENITIES_FAILED:
            return Object.assign({}, state, {data: []},  {fetching: false}, {error: {status: true, message:"Failed to Fetch amenities"}});

        default:
            return state;
    }
}

export const amenityUnitsReducer = (
    state={
        fetching: false,
        error: {status: false, message: ""},
        data: []
    }, action) => {

    switch (action.type) {
        case COMMON__FETCHING_AMENITY_UNITS:
            return Object.assign({}, state, {fetching: true});

        case COMMON__FETCHING_AMENITY_UNITS_SUCCESS:
            return Object.assign({}, {fetching: false}, {data: action.payload});

        case COMMON__FETCHING_AMENITY_UNITS_FAILED:
            return Object.assign({}, state, {data: []},  {fetching: false}, {error: {status: true, message:"Failed to Fetch amenitiy units"}});

        default:
            return state;
    }
}

export const spaceTypesReducer = (
    state={
        fetching: false,
        error: {status: false, message: ""},
        data: []
    }, action) => {

    switch (action.type) {
        case COMMON__FETCHING_SPACE_TYPES:
            return Object.assign({}, state, {fetching: true});

        case COMMON__FETCHING_SPACE_TYPES_SUCCESS:
            return Object.assign({}, {fetching: false}, {data: action.payload});

        case COMMON__FETCHING_SPACE_TYPES_FAILED:
            return Object.assign({}, state, {data: []},  {fetching: false}, {error: {status: true, message:"Failed to Fetch space types"}});

        default:
            return state;
            
    }
}

export const blockChargeTypesReducer = (
    state={
        fetching: false,
        error: {status: false, message: ""},
        data: []
    }, action) => {
    
    switch (action.type) {
        case COMMON__FETCHING_BLOCK_CHARGE_TYPES:
            return Object.assign({}, state, {fetching: true});

        case COMMON__FETCHING_BLOCK_CHARGE_TYPES_SUCCESS:
            return Object.assign({}, state, {fetching: false}, {data: action.payload});

        case COMMON__FETCHING_BLOCK_CHARGE_TYPES_FAILED:
            return Object.assign({},state, {data:[]}, {fetching: false}, {error: {status: true, message:"Failed to Fetch block charge types"}});

        default:
            return state;
    }
}

export const spacesRulesReducer = (
    state={
        fetching: false,
        error: {status: false, message: ""},
        data: []
    }, action) => {
    switch (action.type) {
        case COMMON__FETCHING_SPACE_RULES:
            return Object.assign({}, state, {fetching: true});

        case COMMON__FETCHING_SPACE_RULES_SUCCESS:
            return Object.assign({}, state, {fetching: false}, {data: action.payload});

        case COMMON__FETCHING_SPACE_RULES_FAILED:
            return Object.assign({},state, {data:[]}, {fetching: false}, {error: {status: true, message:"Failed to Fetch space rules"}});

        default:
            return state;
    }
}

export const cancellationPoliciesReducer = (
    state={
        fetching: false,
        error: {status: false, message: ""},
        data: []
    }, action) => {
    switch (action.type) {
        case COMMON__FETCHING_CANCELLATION_POLICIES:
            return Object.assign({}, state, {fetching: true});

        case COMMON__FETCHING_CANCELLATION_POLICIES_SUCCESS:
            return Object.assign({}, state, {fetching: false}, {data: action.payload});

        case COMMON__FETCHING_CANCELLATION_POLICIES_FAILED:
            return Object.assign({},state, {data:[]}, {fetching: false}, {error: {status: true, message:"Failed to Fetch cancellation policies."}});

        default:
            return state;
    }
}