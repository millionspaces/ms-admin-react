import ReportHelper from './ReportHelper';
import AnalyticHelper from './AnalyticHelper';

test('should send charactors length correctly to the sending values', () => {
    var charactorsCount = ReportHelper.getCharatorsCount([{name: "test"}])    
    expect(charactorsCount).toEqual({name: 4});

    var idCount = ReportHelper.getCharatorsCount([{id: 12346}])    
    expect(idCount).toEqual({id: 5});
});

test('should send charactors length correctly to array object', () => {
    var charactorsCount = ReportHelper.getCharatorsCount([{"Space_Name":"NYC Co-working Space","Pricing_Category":"BLOCK_BASE - Per guest based charge","Event_Types":"5-Interview,8-Hot Desk","Organization_Name":"Electro","Space_Id":823},{"Space_Name":"dgffhiijlj","Pricing_Category":"HOUR_BASE","Event_Types":"8-Hot Desk","Organization_Name":null,"Space_Id":822},{"Space_Name":"TestingBug","Pricing_Category":"HOUR_BASE","Event_Types":"9-Sports","Organization_Name":"TestingBug","Space_Id":817},{"Space_Name":"Test_01","Pricing_Category":"HOUR_BASE","Event_Types":"1-Party","Organization_Name":"Test Company","Space_Id":815},{"Space_Name":"Manchester Futsal Club - PerHour","Pricing_Category":"BLOCK_BASE - Space only charge","Event_Types":"6-Training,9-Sports,14-Entertainment","Organization_Name":"Wencedor International","Space_Id":811},{"Space_Name":"CafeHue[SpaceOnly][Non-Reimburse][Menu]","Pricing_Category":"BLOCK_BASE - Space only charge","Event_Types":"9-Sports","Organization_Name":"Wencedor International","Space_Id":810},{"Space_Name":"Restaurant SpaceOnly Reimburse NoMenu","Pricing_Category":"BLOCK_BASE - Per guest based charge - Reimbursable","Event_Types":"1-Party,9-Sports,12-Day Outing","Organization_Name":"Wencedor International 2","Space_Id":809},{"Space_Name":"Spizirna 1902 Per guest based","Pricing_Category":"BLOCK_BASE - Per guest based charge","Event_Types":"4-Meeting,8-Hot Desk","Organization_Name":"Pragues Cafes","Space_Id":808},{"Space_Name":"Space One","Pricing_Category":"BLOCK_BASE - Per guest based charge","Event_Types":"1-Party,2-Wedding,4-Meeting,5-Interview,7-Photoshoot,10-Concert","Organization_Name":"test","Space_Id":806},{"Space_Name":"Greenwich Room &  PerHour","Pricing_Category":"HOUR_BASE","Event_Types":"9-Sports,13-Home for flood victims","Organization_Name":"Amjad Co","Space_Id":805},{"Space_Name":"Alto's Room - Per guest base charge","Pricing_Category":"BLOCK_BASE - Space only charge","Event_Types":"3-Press Conference","Organization_Name":"Amjad","Space_Id":804},{"Space_Name":"Coffee InDoor NoBuffer Menu-PDF","Pricing_Category":"HOUR_BASE","Event_Types":"4-Meeting,5-Interview,8-Hot Desk","Organization_Name":"Amjad","Space_Id":781},{"Space_Name":"Cuztom Menu","Pricing_Category":"BLOCK_BASE - Per guest based charge","Event_Types":"1-Party,2-Wedding,6-Training","Organization_Name":"La events","Space_Id":774},{"Space_Name":"Fullerton Room","Pricing_Category":"BLOCK_BASE - Per guest based charge","Event_Types":"3-Press Conference","Organization_Name":"Amjad","Space_Id":746},{"Space_Name":"Cafe","Pricing_Category":"HOUR_BASE","Event_Types":"9-Sports","Organization_Name":"","Space_Id":726},{"Space_Name":"Great Room Singapore per hour","Pricing_Category":"HOUR_BASE","Event_Types":"8-Hot Desk","Organization_Name":"Nastar","Space_Id":710},{"Space_Name":"MS Conference Room","Pricing_Category":"HOUR_BASE","Event_Types":"3-Press Conference,4-Meeting,5-Interview,6-Training,8-Hot Desk","Organization_Name":"Organization MS","Space_Id":709},{"Space_Name":"Test MS-405","Pricing_Category":"BLOCK_BASE - Per guest based charge","Event_Types":"8-Hot Desk,10-Concert","Organization_Name":"Planet R","Space_Id":707},{"Space_Name":"RITZ CARLTON Conference Room","Pricing_Category":"BLOCK_BASE - Space only charge","Event_Types":"4-Meeting,7-Photoshoot","Organization_Name":null,"Space_Id":705},{"Space_Name":"RITZ CARLTON Boardroom Second Level","Pricing_Category":"BLOCK_BASE - Space only charge","Event_Types":"3-Press Conference,4-Meeting,5-Interview,8-Hot Desk","Organization_Name":null,"Space_Id":693},{"Space_Name":"Alphine Room","Pricing_Category":"BLOCK_BASE - Space only charge","Event_Types":"1-Party,2-Wedding","Organization_Name":"Four Seasons","Space_Id":651}])    
    expect(charactorsCount).toEqual({
        Event_Types: 63,
        Space_Id: 3,
        Pricing_Category: 50,
        Organization_Name: 24,
        Space_Name: 39
    });
});

test("Should add first and last dates", () => {
    var charactorsCount = AnalyticHelper.setStartAndEndDates('2018-11-01', '2018-11-03', [{"date":"2018-11-02","total":4,"guest":3,"user":1}]);    
    expect(charactorsCount).toEqual([{"date":"2018-11-01","total":0,"guest":0,"user":0}, {"date":"2018-11-02","total":4,"guest":3,"user":1}, {"date":"2018-11-03","total":0,"guest":0,"user":0}])
});

test("Should add first and last dates", () => {
    var charactorsCount = AnalyticHelper.setStartAndEndDates('2018-11-01', '2018-11-03', [{"date":"2018-11-01","total":4,"guest":3,"user":1},{"date":"2018-11-02","total":4,"guest":3,"user":1},{"date":"2018-11-03","total":4,"guest":3,"user":1}]); 
    expect(charactorsCount).toEqual([{"date":"2018-11-01","total":4,"guest":3,"user":1},{"date":"2018-11-02","total":4,"guest":3,"user":1},{"date":"2018-11-03","total":4,"guest":3,"user":1}])
});

test('Should calculate Cumulative Cash Inflow', () => {
    let getcummulativeCashFlowValues = ReportHelper.getCumulativeCashInflow([{Cash_Flow_After_Discount : 1},{Cash_Flow_After_Discount : 2},{Cash_Flow_After_Discount : 3}])

    expect(getcummulativeCashFlowValues).toEqual(
        [{
            Cash_Flow_After_Discount : 1,
            CumulativeCashInflow : 1
        },{
            Cash_Flow_After_Discount : 2,
            CumulativeCashInflow : 3
        },{
            Cash_Flow_After_Discount : 3,
            CumulativeCashInflow: 6
        }])
});

test('Should calculate Cumulative MS Contributed Discount', () => {
    let cumulativeMSContributedDiscount  = ReportHelper.getCumulativeMSContributedDiscount([{MS_Contributed_Discount : 100},{MS_Contributed_Discount : 200},{MS_Contributed_Discount : 300}])

    expect(cumulativeMSContributedDiscount).toEqual(
        [{
            MS_Contributed_Discount : 100,
            cumulativeMSContributedDiscount : 100
        },{
            MS_Contributed_Discount : 200,
            cumulativeMSContributedDiscount : 300
        },{
            MS_Contributed_Discount : 300,
            cumulativeMSContributedDiscount : 600
        }])
})

test('Should send same value for "MS_Contributed_Discount" null', () => {
    let cumulativeMSContributedDiscount  = ReportHelper.getCumulativeMSContributedDiscount([{MS_Contributed_Discount : 100},{MS_Contributed_Discount : 200},{MS_Contributed_Discount : 300}, {MS_Contributed_Discount : null}, {MS_Contributed_Discount : 400}])

    expect(cumulativeMSContributedDiscount).toEqual(
        [{
            MS_Contributed_Discount : 100,
            cumulativeMSContributedDiscount : 100
        },{
            MS_Contributed_Discount : 200,
            cumulativeMSContributedDiscount : 300
        },{
            MS_Contributed_Discount : 300,
            cumulativeMSContributedDiscount : 600
        },{
            MS_Contributed_Discount : null,
            cumulativeMSContributedDiscount : 600
        },{
            MS_Contributed_Discount : 400,
            cumulativeMSContributedDiscount : 1000
        }])
})



