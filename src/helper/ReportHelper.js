import moment from 'moment';

export default class ReportHelper {
    static getCharatorsCount = function(list){
       let characterCountForProperty = {};
       list.forEach(k => {
           Object.keys(k).forEach(key => {
                if((typeof k[key]) === "string"){
                    characterCountForProperty[key] = characterCountForProperty[key] ? 
                                            k[key].length > characterCountForProperty[key] ? k[key].length : characterCountForProperty[key]
                                            : k[key].length 
                }
                if((typeof k[key]) === "number"){
                    characterCountForProperty[key] = characterCountForProperty[key] ? 
                                            k[key].toString().length > characterCountForProperty[key] ? k[key].toString().length : characterCountForProperty[key]
                                            : k[key].toString().length 
                }    
           })
       });
       return characterCountForProperty;
    }

    static getFromToDateList = function(fromDate, toDate, dates){
        let startFromWeek   = moment(fromDate);
        let endWeek         = moment(toDate);
        
        let selectedWeek    = [];

        let endDay = endWeek.diff(moment()) >= 0 ? moment() : endWeek
        while(startFromWeek <= endDay){
            if(dates.filter(i => i.date === moment(startFromWeek).format('YYYY-MM-DD')).length > 0){
                selectedWeek.push({
                    date    : dates.filter(i => i.date === moment(startFromWeek).format('YYYY-MM-DD')).map(i => i.date)[0],
                    guest   : dates.filter(i => i.date === moment(startFromWeek).format('YYYY-MM-DD')).map(i => i.guest)[0],
                    total   : dates.filter(i => i.date === moment(startFromWeek).format('YYYY-MM-DD')).map(i => i.total)[0],
                    user    : dates.filter(i => i.date === moment(startFromWeek).format('YYYY-MM-DD')).map(i => i.user)[0]
                });
            }else{
                selectedWeek.push({
                    date    : moment(startFromWeek).format('YYYY-MM-DD'),
                    guest   : 0,
                    total   : 0,
                    user    : 0
                });
            }
            startFromWeek = startFromWeek.clone().add(1, 'd')
        }

        return selectedWeek;
    }

    static getCharactorsCountOfTableColumns = (reportDetails) => {
        const spaceCharactoList = ReportHelper.getCharatorsCount(reportDetails);

        return [
            {wch:'REF ID'.length > spaceCharactoList.REF_ID ? 'REF ID'.length : spaceCharactoList.REF_ID },
            {wch:'Event Date'.length > spaceCharactoList.Event_Date ? 'Event Date'.length : spaceCharactoList.Event_Date},
            {wch:'Space Name'.length > spaceCharactoList.Space_Name ? 'Space Name'.length :  spaceCharactoList.Space_Name},
            {wch:'Organization Name'.length > spaceCharactoList.Organization_Name ? 'Organization Name'.length : spaceCharactoList.Organization_Name},
            {wch:'Booking Value  before discount(LKR)'.length > spaceCharactoList.Booking_Value_Before_Discount ? 'Booking Value  before discount(LKR))'.length : spaceCharactoList.Booking_Value_Before_Discount},
            {wch:'MS-Contributed Discount (LKR)'.length > spaceCharactoList.MS_Contributed_Discount ? 'MS-Contributed Discount (LKR)'.length : spaceCharactoList.MS_Contributed_Discount},
            {wch:'Host-Contributed Discount (LKR)'.length > spaceCharactoList.HOST_Contributed_Discount ? 'Host-Contributed Discount (LKR)'.length : spaceCharactoList.HOST_Contributed_Discount},
            {wch:'Cash Inflow after Discount (LKR)'.length > spaceCharactoList.Cash_Flow_After_Discount ? 'Cash Inflow after Discount (LKR)'.length: spaceCharactoList.Cash_Flow_After_Discount},
            {wch:'Commission %'.length > spaceCharactoList.Commision ? 'Commission %'.length : spaceCharactoList.Commision},
            {wch:'Host Payment (LKR)'.length > spaceCharactoList.Host_Payment ? 'Host Payment (LKR)'.length : spaceCharactoList.Host_Payment},
            {wch:'Commission Revenue (LKR)'.length > spaceCharactoList.Commission_Revenue ? 'Commission Revenue (LKR)'.length : spaceCharactoList.Commission_Revenue},
            {wch:'Promo Code Applied'.length > spaceCharactoList.Promo_Code_Applied ? 'Promo Code Applied'.length : spaceCharactoList.Promo_Code_Applied},
            {wch:'Booking Received Date'.length > spaceCharactoList.Booking_Received_Date ? 'Booking Received Date'.length : spaceCharactoList.Booking_Received_Date},
            {wch:'Review Received?'.length > spaceCharactoList.Review_Received ? 'Review Received?'.length : spaceCharactoList.Review_Received },
            {wch:'Cumulative Cash Inflow (LKR)'.length > spaceCharactoList.CumulativeCashInflow ? 'Cumulative Cash Inflow (LKR)'.length : spaceCharactoList.CumulativeCashInflow },
            {wch:'Cumulative Commission Revenue (LKR)'.length > spaceCharactoList.cumulativeCommissionRevenue ? 'Cumulative Commission Revenue (LKR)'.length : spaceCharactoList.cumulativeCommissionRevenue },
            {wch:'Cumulative MS-Contributed Discount (LKR)'.length > spaceCharactoList.cumulativeMSContributedDiscount ? 'Cumulative MS-Contributed Discount (LKR)'.length : spaceCharactoList.cumulativeMSContributedDiscount },
            {wch:'Nett Commission Revenue (LKR)'.length > spaceCharactoList.Net_Commission_Revenue ? 'Nett Commission Revenue (LKR)'.length : spaceCharactoList.Net_Commission_Revenue },
            {wch:'Payment Mode'.length > spaceCharactoList.Payment_Method ? 'Payment Mode'.length :spaceCharactoList.Payment_Method },
            {wch:'Guest Name'.length > spaceCharactoList.Guest_Name ? 'Guest Name'.length : spaceCharactoList.Guest_Name},
            {wch:'EventType'.length > spaceCharactoList.Event_Type ? 'EventType'.length : spaceCharactoList.Event_Type},
            {wch:'Guest Contact Number'.length > spaceCharactoList.Guest_Moble ? 'Guest Contact Number'.length : spaceCharactoList.Guest_Moble},
            {wch:'Guest Email'.length > spaceCharactoList.Guest_Email ? 'Guest Email'.length : spaceCharactoList.Guest_Email},
            {wch:'Is Advance'.length > spaceCharactoList.Is_Advance ? 'Is Advance'.length : spaceCharactoList.Is_Advance},
            {wch:'Host Payment Deadline'.length > spaceCharactoList.Host_Payment_Deadline ? 'Host Payment Deadline'.length : spaceCharactoList.Host_Payment_Deadline},
          
        ];
    }

    static getCumulativeCashInflow = (reportDetails) => {
        let cumulativeCashInflow = 0;

        return reportDetails.map(report => {
            if(cumulativeCashInflow === 0){
                cumulativeCashInflow = cumulativeCashInflow + report.Cash_Flow_After_Discount 
                return Object.assign({}, report, {CumulativeCashInflow : cumulativeCashInflow})
            }else{
                cumulativeCashInflow = cumulativeCashInflow + report.Cash_Flow_After_Discount 
                return Object.assign({}, report, {CumulativeCashInflow : cumulativeCashInflow})
            }
        });
    }
    
    static getCumulativeMSContributedDiscount = (reportDetails) => {
        let commulativeMsDiscount = 0;

        return reportDetails.map(report => {
            if(commulativeMsDiscount === 0){
                commulativeMsDiscount = commulativeMsDiscount + report.MS_Contributed_Discount 
                return Object.assign({}, report, {cumulativeMSContributedDiscount : commulativeMsDiscount})
            }else{
                commulativeMsDiscount = commulativeMsDiscount + report.MS_Contributed_Discount 
                return Object.assign({}, report, {cumulativeMSContributedDiscount : commulativeMsDiscount})
            }
        });
    }

    static getCumulativeCommissionRevenue = (reportDetails) => {
        let cumulativeCommissionRevenue = 0;
        
        return reportDetails.map(report => {
            if(cumulativeCommissionRevenue === 0){
                cumulativeCommissionRevenue = cumulativeCommissionRevenue + report.Commission_Revenue 
                return Object.assign({}, report, {cumulativeCommissionRevenue : cumulativeCommissionRevenue})
            }else{
                cumulativeCommissionRevenue = cumulativeCommissionRevenue + report.Commission_Revenue 
                return Object.assign({}, report, {cumulativeCommissionRevenue : cumulativeCommissionRevenue})
            }
        });
    } 
    
}




