import moment from 'moment';
import { start } from 'pretty-error';

export default class AnalyticHelper {
    static setStartAndEndDates = function(startDate, endDate, dates){
        return setEndDefaultDate(endDate, setStartDefaultDate(startDate, dates))
    }
}

function setStartDefaultDate(startDate, dates) {
    let selectedDates = [];

    if(!dates.filter(i => moment(startDate).format('YYYY-MM-DD') === i.date).length > 0){
        selectedDates.push({"date" : startDate, "total" : 0, "guest" : 0, "user": 0});
    }else{
        return dates
    }
    dates.forEach(i => {
        selectedDates.push(i)
    });

    return selectedDates;
}

function setEndDefaultDate(endDate, dates){
    let selectedDates = [];

    if(!dates.filter(i => moment(endDate).format('YYYY-MM-DD') === i.date).length > 0){
        dates.forEach(i => {
            selectedDates.push(i)
        });
        selectedDates.push({"date" : endDate, "total" : 0, "guest" : 0, "user": 0});
        return selectedDates
    }else{
        return dates
    }
}