import moment from 'moment';
import { TIME_STRING_FORMAT } from './settings';

/**
 * Add specified number of hours to given time
 * @param {String} time Time Strings
 * @param {Number} hours Number of hours to be added
 */
export const addHours = (time, hours) => moment(time, TIME_STRING_FORMAT).add(hours, 'hours').format(TIME_STRING_FORMAT).toString();

/**
 * Compare two time strings
 * @param {String} right Time Strings
 * @param {String} left Time Strings
 */
export const compareTimeString = (right, left) => moment(left, TIME_STRING_FORMAT).isBefore(moment(right, TIME_STRING_FORMAT))  || moment(left, TIME_STRING_FORMAT).isSame(moment(right, TIME_STRING_FORMAT));


/**
 * Generate hours
 * @param {Number} from Initial value of loop.
 * @param {Number} to Control Value of loop
 * @param {String} timeStringFormat Format of time string for MomentJS
 */
export const generateHours = (from, to, timeStringFormat) => {
    
    let hours = [];
    
    for (let i=from; i<=to; i++) {

        let timeString = moment().utcOffset(0).set({hour:i,minute:0,second:0,millisecond:0}).format(TIME_STRING_FORMAT).toString();

        hours.push({
            value: timeString,
            label: timeString
        });
        
    }

    return hours;
}

export const convert24To12 = hour => moment().utcOffset(0).set({hour:hour,minute:0,second:0,millisecond:0}).format(TIME_STRING_FORMAT).toString();

/**
 * Returns days of week.
 */
export const getDaysOfWeek = () => moment.weekdays();

export const hasErrors = (fieldsError) => Object.keys(fieldsError).some(field => fieldsError[field]);

export const SUCCESS = 'success';
export const FAILED = 'failed';