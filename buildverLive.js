var fs = require('fs');
var shortid = require('shortid');
var moment = require('moment');

fs.readFile('src/metadata.live.json', function (err, content) {

    var metadata = JSON.parse(content);

    metadata.push({
        "build": shortid.generate(), 
        "timestamp": Date.now(),
        "time": moment().format('MMMM Do YYYY, h:mm:ss a')
    });

    fs.writeFile('src/metadata.live.json',JSON.stringify(metadata),function(err){
        if(err) throw err;
        console.log("Current build number: " + metadata);
    });
})

