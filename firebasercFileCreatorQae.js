var fs = require('fs');

fs.truncate('.firebaserc', 0, function() {
  fs.writeFile(
    '.firebaserc',
    JSON.stringify({
      projects: {
        qae: 'ms-admin-qae',
      },
    }),
    function(err) {
      if (err) throw err;
    },
  );
});
