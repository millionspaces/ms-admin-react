var fs = require('fs');

fs.truncate('.firebaserc', 0, function() {
  fs.writeFile(
    '.firebaserc',
    JSON.stringify({
      projects: {
        live: 'mspaces-admin',
      },
    }),
    function(err) {
      if (err) throw err;
    },
  );
});
